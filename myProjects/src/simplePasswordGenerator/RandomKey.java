package simplePasswordGenerator;

import java.util.Random;

public class RandomKey {

	public static void main(String[] args) {
		Random rand = new Random();
		
		int keyLength = 12;
		
		for (int i = 0; i < keyLength; i++) {
			int a;
			switch ( rand.nextInt(4) ) {
			case 0:
				a = rand.nextInt('z'-'a'+ 1)+'a';
				System.out.println(Character.valueOf((char) a).toString());
				break;
			case 1:
				a = rand.nextInt('Z'-'A'+ 1)+'A';
				System.out.println(Character.valueOf((char) a).toString());
				break;
			case 2:
				a = rand.nextInt(9 + 1);
				System.out.println(a);
				break;
			default:
				i--;
				break;
			} 
		}
	}
}
