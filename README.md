# OCP Java Course #

This repository contains:

* My course notes
* My code solutions to the course exercises
* My small side projects

###### Course exercises @ [https://bitbucket.org/ccjavad/ocp_192316/src/master/Aufgaben/](https://bitbucket.org/ccjavad/ocp_192316/src/master/Aufgaben/ "Hyperlink to course exercises @ bitbucket.org")


#### 2020 Feb 21

###### Exercise  'realisation of JDBC in DAO pattern'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/courseNotes/src/ub29_daoImplementation/ "Hyperlink to ub29_daoImplementation @ bitbucket.org")

- Done.

#### 2020 Feb 19

###### Exercise  'Threads - Cyclic Barrier.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub051_ThreadsCyclicBarrier/ "Hyperlink to ub051_ThreadsCyclicBarrier @ bitbucket.org")

- Done.


#### 2020 Feb 18

###### Exercise  'Threads - Fork-Join - mit List.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub050_ThreadsForkJoinList/ "Hyperlink to ub050_ThreadsForkJoinList @ bitbucket.org")

- Done.

###### Exercise  'Threads - Fork-Join - mit Array.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub049_ThreadsForkJoinArray/ "Hyperlink to ub049_ThreadsForkJoinArray @ bitbucket.org")

- Done.


#### 2020 Feb 17

###### Exercise  'Threads - ExecutorService - english words.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub048_ThreadsExecutorServiceEnglishWords/ "Hyperlink to ub048_ThreadsExecutorServiceEnglishWords @ bitbucket.org")

- Done.


#### 2020 Feb 16

###### Exercise  'Threads - Bäckerei.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub047_ThreadsBakery/ "Hyperlink to ub047_ThreadsBakery @ bitbucket.org")

- Done.


#### 2020 Feb 14

###### Exercise  'Threads - Blocking.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub046_ThreadsBlocking/ "Hyperlink to ub046_ThreadsBlocking @ bitbucket.org")

- Done.


#### 2020 Feb 12

###### Exercise  'Threads - Wetterstation.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub045_ThreadsWetterstation/ "Hyperlink to ub045_ThreadsWetterstation @ bitbucket.org")

- Done.

###### Exercise  'Threads - Logger.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub044_ThreadsLogger/ "Hyperlink to ub044_ThreadsLogger @ bitbucket.org")

- Done.


#### 2020 Feb 11

###### Exercise  'Threads - Simple API.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub043_ThreadsSimpleAPI/ "Hyperlink to ub043_ThreadsSimpleAPI @ bitbucket.org")

- Done.


#### 2020 Feb 7

###### Exercise  'IO - copy text file.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub042_IOCopyTextFile/ "Hyperlink to ub042_IOCopyTextFile @ bitbucket.org")

- Done.

###### Exercise  'Words - IO.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub041_WordsIO/ "Hyperlink to ub041_WordsIO @ bitbucket.org")

- Done.


#### 2020 Feb 5

###### Exercise  'Files - FilesCounter.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub040_FilesCounter/ "Hyperlink to ub040_FilesCounter @ bitbucket.org")

- Done.


#### 2020 Feb 4

###### Exercise  'Words - funkt. Streams.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub039_WordsfunktStreams/ "Hyperlink to ub039_WordsfunktStreams @ bitbucket.org")

- Done.


#### 2020 Jan 31

###### Exercise  'Maps - Functional.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub038_MapsFunctional/ "Hyperlink to ub038_MapsFunctional @ bitbucket.org")

- Done.

###### Exercise  'Collections - Functional.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub037_CollectionsFunctional/ "Hyperlink to ub037_CollectionsFunctional @ bitbucket.org")

- Done.

###### Small task  'Stream - Map - CharCount.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/courseNotes/src/ub19_smallTaskMerge/ "Hyperlink to ub19_smallTaskMerge @ bitbucket.org")

- Done.


#### 2020 Jan 30

###### Exercise  'Stream - collect - Collectors - Autos.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub036_StreamCollectCollectorsAutos/ "Hyperlink to ub036_StreamCollectCollectorsAutos @ bitbucket.org")

- Done.

###### Exercise  'Stream - collect - Collectors - Personen.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub035_StreamCollectCollectorsPersonen/ "Hyperlink to ub035_StreamCollectCollectorsPersonen @ bitbucket.org")

- New tasks A4 and A5 done.


#### 2020 Jan 29

###### Exercise  'Stream - collect - Collectors - Personen.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub035_StreamCollectCollectorsPersonen/ "Hyperlink to ub035_StreamCollectCollectorsPersonen @ bitbucket.org")

- Done.


#### 2020 Jan 28

###### Exercise  'Stream - collect - Numbers.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub034_StreamCollectNumbers/ "Hyperlink to ub034_StreamCollectNumbers @ bitbucket.org")

- Done.

###### Exercise  'Stream - collect - Warenkorb.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub033_StreamCollectWarenkorb/ "Hyperlink to ub033_StreamCollectWarenkorb @ bitbucket.org")

- Done.

###### Exercise  'Stream - reduce - TextStatistics.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub032_StreamReduceTextStatistics/ "Hyperlink to ub032_StreamReduceTextStatistics @ bitbucket.org")

- A2 done (with decorator pattern).


#### 2020 Jan 27

###### Exercise  'Stream - reduce - TextStatistics.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub032_StreamReduceTextStatistics/ "Hyperlink to ub032_StreamReduceTextStatistics @ bitbucket.org")

- A1 done.
- A2 work in progress...

###### Exercise  'Stream - reduce - Personen.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub031_StreamReducePersonen/ "Hyperlink to ub031_StreamReducePersonen @ bitbucket.org")

- Done.

###### Exercise  'Stream - reduce - simple.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub030_StreamReduceSimple/ "Hyperlink to ub030_StreamReduceSimple @ bitbucket.org")

- Done.


#### 2020 Jan 24

###### Exercise  'Stream - count/min/max.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub029_StreamCountMinMax/ "Hyperlink to ub029_StreamCountMinMax @ bitbucket.org")

- Done.

###### Exercise  'Stream - intermediate operations.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub028_StreamIntermediateOperations/ "Hyperlink to ub028_StreamIntermediateOperations @ bitbucket.org")

- Done.


#### 2020 Jan 23

###### Exercise  'Stream - intermediate operations.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub028_StreamIntermediateOperations/ "Hyperlink to ub028_StreamIntermediateOperations @ bitbucket.org")

- Work in progress...
- UnitTests implemented.

#### 2020 Jan 22

###### Exercise  'Stream - bilden.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub027_StreamBilden/ "Hyperlink to ub027_StreamBilden @ bitbucket.org")

- Done.


#### 2020 Jan 21

###### Exercise  'Map - Besizter Fahrzeuge.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub026_MapBesizterFahrzeuge/ "Hyperlink to ub026_MapBesizterFahrzeuge @ bitbucket.org")

- Done.

###### Exercise  'BiFunction - MethodReference.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub025_BiFunctionMethodReference/ "Hyperlink to ub025_BiFunctionMethodReference @ bitbucket.org")

- Done.

###### On method::references

https://docs.oracle.com/javase/tutorial/java/javaOO/methodreferences.html

https://docs.oracle.com/javase/specs/jls/se11/html/jls-15.html#jls-15.13.3


#### 2020 Jan 17

###### Exercise  'Collections - Autos.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub024_CollectionsAutos/ "Hyperlink to ub024_CollectionsAutos @ bitbucket.org")

- Done.

###### Exercise  'Generics - Generische Methoden - Functional.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub023_GenericsGenerischeMethodenFunctional/ "Hyperlink to ub023_GenericsGenerischeMethodenFunctional @ bitbucket.org")

- Done.


#### 2020 Jan 15

###### Exercise 'Generics - Platzhalter.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub022_GenericsPlatzhalter_withPNG/ "Hyperlink to ub022_GenericsPlatzhalter_withPNG @ bitbucket.org")

- Done.

###### Exercise 'Generics - Zoo.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub021_GenericsZoo/ "Hyperlink to ub021_GenericsZoo @ bitbucket.org")

- Done.

###### Exercise 'Generics - Platzhalter - Functional.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub020_GenericsPlatzhalterFunctional/ "Hyperlink to ub020_GenericsPlatzhalterFunctional @ bitbucket.org")

- Done.


#### 2020 Jan 13

###### Exercise 'Deque - BrowserHistory.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub019_DequeBrowserHistory/ "Hyperlink to ub019_DequeBrowserHistory @ bitbucket.org")

- Done.

###### Exercise 'Deque - TextProcessor.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub018_DequeTextProcessor/ "Hyperlink to ub018_DequeTextProcessor @ bitbucket.org")

- A1 - A3 done.
- A4 work in progress...

###### Exercise 'Map - TextStatistics.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub017_MapTextStatistics/ "Hyperlink to ub017_MapTextStatistics @ bitbucket.org")

- Done.

###### Exercise 'Map - Simple.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub016_MapSimple/ "Hyperlink to ub016_MapSimple @ bitbucket.org")

- Done.


#### 2020 Jan 10

###### Exercise 'Queue - Task.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub015_QueueTask/ "Hyperlink to ub015_QueueTask @ bitbucket.org")

- Done.


#### 2020 Jan 09

###### Exercise 'Set - TextStatistics.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub014_ListTextStatistics/ "Hyperlink to ub014_ListTextStatistics @ bitbucket.org")

- Done.


#### 2020 Jan 08

###### Exercise 'List - Benchmark.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub013_ListBenchmark/ "Hyperlink to ub013_ListBenchmark @ bitbucket.org")

- Done.

###### Exercise 'List - Iterable.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub012_ListIterable/ "Hyperlink to ub012_ListIterable @ bitbucket.org")

- Done.


#### 2020 Jan 07

###### Exercise 'Innere Klassen - Gebäude.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub011_InnerClassBuilding/ "Hyperlink to ub011_InnerClassBuilding @ bitbucket.org")

- Done.
- Comment: Building might profit from builder pattern. Floor and Room are very similar (-> composite pattern?).  

###### Exercise 'Generics - Garage.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub010_GenericsGarage/ "Hyperlink to ub010_GenericsGarage @ bitbucket.org")

- Done.

###### Exercise 'Consumer - PutIntegers.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub009_ConsumerPutIntegers/ "Hyperlink to ub009_ConsumerPutIntegers @ bitbucket.org")

- Done.

###### Exercise 'Function - StringTransform.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub008_StringTransformFunction/ "Hyperlink to ub008_StringTransformFunction @ bitbucket.org")

- Done.

###### Completion to exercise 'Interfaces - String Transform.md'

- A5 done.


#### 2019 Dec 20

###### Exercise 'Interfaces - String Transform.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub007_InterfacesStringTransform/ "Hyperlink to ub007_InterfacesStringTransform @ bitbucket.org")

- A1-A4 done.
- A5 still to do.


###### Exercise 'Enums - Kaffeeautomat.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub006_coffeeDispenser/ "Hyperlink to ub006_coffeeDispenser @ bitbucket.org")

- Done.


#### 2019 Dec 19

###### Exercise 'Exceptions - Vererbungshierarchie.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub005_exceptions/ "Hyperlink to ub005_exceptions @ bitbucket.org")

- Done.

###### Exercise 'Ueberschreiben und Exceptions'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub004_overwrite/ "Hyperlink to ub004_overwrite @ bitbucket.org")

- Done.


#### 2019 Dec 18

###### Small task

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub003_smallTask/ "Hyperlink to ub003_smallTask @ bitbucket.org")

- Done. 


#### 2019 Dec 16

###### Exercise builder pattern for Question

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub001_QuizzTypesWithBuilder/ "Hyperlink to ub001_QuizzTypesWithBuilder @ bitbucket.org")

- Work in progress... 


#### 2019 Dec 13

###### Exercise 'Project Quiz - 02 - IO - Read'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub002_QuizIORead/ "Hyperlink to ub002_QuizIORead @ bitbucket.org")

- Done.

###### Exercise 'Project Quiz - 01 - Typen.md'

- [My solution](https://bitbucket.org/UweBenary/2020_ocp/src/master/solutions2Execises/src/ub001_QuizTypes/ "Hyperlink to ub001_QuizTypes @ bitbucket.org")

- Done.
