package alignment;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import alignment.Word;

class WordTest {

	private final String expectedString = "Test";
	private final Word expectedWord = new Word(expectedString);
	private final Word expectedWordCopy = new Word(expectedString);
	private final Word expectedWord2 = new Word(expectedString + "2");
	
	@Test
	void testInstanciationFailsWithNull() {
		assertThrows(IllegalArgumentException.class, () -> new Word(null) );
	}		
	
	@Test
	void testInstanciationFailsWithEmptyOrBlanksString() {
		assertThrows(IllegalArgumentException.class, () -> new Word("") );
		assertThrows(IllegalArgumentException.class, () -> new Word("	") );
	}	

	@Test
	final void testGetValue() {
		final String expected = '\u0000' + expectedString;
		final String actual = expectedWord.getValue();
		assertEquals(expected, actual);
	}

	@Test
	final void testLength() {
		final int expected = expectedString.length() + 1;
		final int actual = expectedWord.length();
		assertEquals(expected, actual);
	}
	
	@Test
	final void testCharAt() {
		final int index = 3;		
		final char expected = expectedString.charAt(index);
		final char actual = expectedWord.charAt(index+1);
		assertEquals(expected, actual);
	}
	
	@Test
	final void testCharAt0() {	
		final char expected = '\u0000';
		final char actual = expectedWord.charAt(0);
		assertEquals(expected, actual);
		assertNotNull(String.valueOf(actual));
	}

	@Test
	final void testEqualsObject() {
		assertTrue(expectedWord.equals(expectedWordCopy));	
		assertFalse(expectedWord.equals(expectedWord2));	
	}

	@Test
	final void testToString() {
		final String expected = expectedString;
		final String actual = expectedWord.toString();
		assertEquals(expected, actual);
	}

}
