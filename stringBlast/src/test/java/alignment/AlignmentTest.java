package alignment;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import algorithm.SimilarityScoreFunction;
import scoring.Score;

class AlignmentTest {
	
	private final Word query = new Word("Anna" + '\u0000' + '\u0000');	
	private final Word target = new Word("An" + '\u0000' + '\u0000' + "ke");
	private final Word target2 = new Word("n" + '\u0000' + '\u0000' + "ke");
	private final String expectedToString = "Anna  \n||    \nAn  ke\n";
	
	
	private final Alignment al = new Alignment(query, target );
	String test = al.toString();
	
	
	@Test
	final void testAlignment() {
		assertDoesNotThrow( () -> new Alignment(query, target ) );
		assertThrows( IllegalArgumentException.class, () -> new Alignment(query, target2 ) );
	}


	
	@Test
	final void testGetquery() {
		final Word expected = query;
		final Word actual = al.getQuery();
		assertEquals(expected, actual);
	}

	@Test
	final void testGetTarget() {
		final Word expected = target;
		final Word actual = al.getTarget();
		assertEquals(expected, actual);
	}

	@Test
	final void testToString() {		
		assertEquals(expectedToString, al.toString() );		
	}
	
	
	@Test
	final void testScore() {
		final int expectedScore = 2;
		SimilarityScoreFunction ssf = (char x1, char x2) -> new Score( x1==x2 ? 1 : 0 );
		Score s = al.score( ssf );
		final int actualScore = s.getValue(); 
		assertEquals(expectedScore, actualScore );
		
	}
}
