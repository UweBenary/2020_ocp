package blast;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.Test;

import algorithm.Algorithm;
import algorithm.GapPenalty;
import algorithm.SimilarityScoreFunction;
import algorithm.SmithWaterman;
import alignment.Alignment;
import alignment.Word;
import scoring.Score;

class StringBlasterTest {
	
	private final Word query = new Word("acga"); //https://de.wikipedia.org/wiki/Smith-Waterman-Algorithmus#Beispiel
	private final Word target1 = new Word("tccg");
	private final Word target2 = new Word("acga");
	private final Word target3 = new Word("1 x- ");
	private final Word target4 = new Word("" + '\u0000');

	
	private final GapPenalty gapPenalty = x -> 1;
	private final SimilarityScoreFunction similarityScore = (char x1, char x2) -> new Score( x1 =='\u0000' || x1!=x2 ? -1 : 2 );	
	private final Algorithm algorithm = new SmithWaterman(gapPenalty, similarityScore);

	private final StringBlaster stringBlast = new StringBlaster(algorithm);

	@Test
	final void testAlign() {
		Alignment alignment = stringBlast.align(query, target1);
		
		final String expected =	"  acga\n   || \n" + "tc cg \n";
		final String actual = alignment.toString();
		
		assertEquals(expected, actual);
	}
	
	
	@Test
	final void testAlign2() {
		Alignment alignment = stringBlast.align(query, target3);
		
		final String expected =	"         ";
		final String actual = alignment.getAssociationMarkers();
		
		assertEquals(expected, actual);
	}
	
	@Test
	final void testAlign3() {
		Alignment alignment = stringBlast.align(query, target4);
		
		final String expected =	"     ";
		final String actual = alignment.getAssociationMarkers();
		
		assertEquals(expected, actual);
	}

	@Test
	final void testAlignMulti() {

		List<Word> targets = new ArrayList<Word>( Arrays.asList( new Word[] {target1, target2} )) ; 
		Collection<Alignment> result = stringBlast.alignMulti(query, targets); 
		
		final String expected =	"[  acga\n   || \ntc cg \n, acga\n||||\nacga\n]";
		final String actual = result.toString();
		
		assertEquals(expected, actual);
	}
	
}