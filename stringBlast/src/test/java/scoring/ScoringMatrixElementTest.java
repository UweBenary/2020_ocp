package scoring;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ScoringMatrixElementTest {

	private final int queryIdx = 1;
	private final int targetIdx = 2;
	private final Score score = Score.ZERO_SCORE;
	
	private final ScoringMatrixElement predecessor = new StartElement(queryIdx, targetIdx, score);

	private final int queryIdxNext = 3;
	private final int targetIdxNext = 4;
	private final Score scoreNext = new Score(1);
	private final ScoringMatrixElement element = new NextElement(queryIdxNext, targetIdxNext, scoreNext, predecessor);
	
	@Test
	final void testGetQueryIndex() {
		assertEquals(queryIdx, predecessor.getQueryIndex());
	}

	@Test
	final void testGetTargetIndex() {
		assertEquals(targetIdx, predecessor.getTargetIndex());
	}

	@Test
	final void testGetScore() {
		assertEquals(score, predecessor.getScore());
		assertFalse(score == predecessor.getScore());
	}

	@Test
	final void testHasPredecessor() {
		assertFalse(predecessor.hasPredecessor());
		assertTrue(element.hasPredecessor());
	}
	
	@Test
	final void testGetPredecessor() {
		assertEquals(predecessor, element.getPredecessor());
		assertTrue(predecessor == element.getPredecessor());
	}

	@Test
	final void testCompareTo() {
		assertTrue(predecessor.compareTo(element) != 0);

		assertTrue(predecessor.compareTo(element.getPredecessor()) == 0);
	}
//
//	@Test
//	final void testToString() {
//		System.out.println(predecessor);
//		System.out.println(element);
//	}

}
