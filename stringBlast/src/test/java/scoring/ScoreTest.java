package scoring;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import scoring.Score;

class ScoreTest {

	@Test
	final void testGetValue() {
		final int expected = 2;
		Score s = new Score(expected);
		final int actual = s.getValue();
		assertEquals(expected, actual);
	}
	
	@Test
	final void testAdd() {
		final int expected = 2;
		Score s = new Score(1);
		s = s.add(s);
		final int actual = s.getValue();
		assertEquals(expected, actual);
	}
}
