package scoring;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import alignment.Word;

class ScoringMatrixTest {

	ScoringMatrix matrix;
	ScoringMatrixElement dummyElement;
	ScoringMatrixElement dummyElement2;
	Word q = new Word("q");
	Word t = new Word("ta");
	
	@BeforeEach
	void setupMatrix() {
		matrix = new ScoringMatrix(q, t);
		dummyElement = new StartElement(0, 0, Score.ZERO_SCORE);
		dummyElement2 = new NextElement(0, 0, new Score(1), dummyElement);

	}
	
	@Test
	final void testGetElement() {
		ScoringMatrixElement expected = dummyElement;
		matrix.addElement(dummyElement);
		ScoringMatrixElement actual = matrix.getElement(0, 0);
		assertEquals(expected, actual);		
	}
	
	@Test
	final void testGetMaxScoreElement() {
		ScoringMatrixElement expected = dummyElement2;
		matrix.addElement(dummyElement2);
		ScoringMatrixElement actual = matrix.getMaxScoreElement();
		assertEquals(expected, actual);
	}
	
	@Test
	final void testGetQuery() {
		assertEquals(q, matrix.getQuery());
	}

	@Test
	final void testGetTarget() {
		assertEquals(t, matrix.getTarget());
	}

	@Test
	final void testAdd() {
		assertTrue(matrix.addElement(dummyElement));
		
		assertFalse(matrix.addElement(dummyElement2));
		
		ScoringMatrixElement dummyElementOutside = new NextElement(10, 0, new Score(1), dummyElement);
		assertFalse(matrix.addElement(dummyElementOutside));
	}


	@Test
	final void testGetScore() {
		Score expected = new Score(1);
		matrix.addElement(dummyElement2);
		Score actual = matrix.getScore(0, 0);
		assertEquals(expected, actual);
	}

//	@Test
//	final void testToStringScores() {
//		matrix.addElement(dummyElement2);
//		System.out.println("testToStringScores : \n" + matrix.toStringScores());
//	}
//
//	@Test
//	final void testToStringPredecessor() {
//		matrix.addElement(dummyElement2);
//		System.out.println("testToStringPredecessor : \n" + matrix.toStringPredecessor());
//	}
//	
//	@Test
//	final void testToString() {
//		matrix.addElement(dummyElement2);
//		System.out.println("testToString : \n" + matrix);
//	}


}
