package matrix.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collection;
import java.util.PriorityQueue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import matrix.Matrix;
import matrix.MatrixDisplayer;
import matrix.MatrixFactory;

class MatrixFactoryTest {

	private Matrix<Integer> expected;
	
	final void display(Matrix<Integer> actual) {
		MatrixDisplayer displayer = new MatrixDisplayer(expected, "*");
		System.out.println(displayer.display());
		
		displayer = new MatrixDisplayer(actual, "*");
		System.out.println(displayer.display());
	}
	
	@BeforeEach
	final private void setupMatrix() {
		expected = new Matrix<Integer>();
		expected.addIfNotPresent(1, 0, 0);
		expected.addIfNotPresent(2, 1, 0);
		expected.addIfNotPresent(3, 0, 1);
	}
	
	@Test
	final void testOf() {
		Matrix<Integer> actual = MatrixFactory.of(expected);
		assertEquals(expected, actual);
		assertFalse(expected == actual);
	}

	@Test
	final void testOf2DArray() {
		Integer[][] arr = {{1, 2}, {3}};
		Matrix<Integer> actual = MatrixFactory.of(arr);
		assertEquals(expected, actual);
	}

	@Test
	final void testOfArray() {
		Integer[] arr = {1, 2, 3};
		Matrix<Integer> actual = MatrixFactory.of(arr, 2);
		assertEquals(expected, actual);
	}

	@Test
	final void testOfCollection() {
		Collection<Integer> coll = new PriorityQueue<Integer>();
		coll.add(1);
		coll.add(2);
		coll.add(3);
		Matrix<Integer> actual = MatrixFactory.of(coll, 2);
		assertEquals(expected, actual);
	}
}
