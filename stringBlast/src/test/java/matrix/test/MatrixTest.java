package matrix.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Optional;

import org.junit.jupiter.api.Test;

import matrix.Matrix;
import matrix.MatrixDisplayer;

class MatrixTest {


	@Test
	final void testGetDimX() {
		Matrix<Integer> matrix = new Matrix<Integer>();
		final int expected = 2;
		matrix.addIfNotPresent(1, expected, 3);
		int actual = matrix.getDimX();
		assertEquals(expected, actual);
	}

	@Test
	final void testGetDimY() {
		Matrix<Integer> matrix = new Matrix<Integer>();
		final int expected = 3;
		matrix.addIfNotPresent(1, 2, expected);
		int actual = matrix.getDimY();
		assertEquals(expected, actual);
	}

	@Test
	final void testIsPresent() {
		Matrix<Integer> matrix = new Matrix<Integer>();
		matrix.addIfNotPresent(1, 2, 3);
		assertTrue(matrix.isPresent(2, 3));
		assertFalse(matrix.isPresent(0, 0));
	}


	@Test
	final void testGetElement() {
		Matrix<Integer> matrix = new Matrix<Integer>();
		final Integer expected = 1;
		matrix.addIfNotPresent(expected, 2, 3);
		Optional<Integer> element = matrix.getElement(2, 3);
		int actual = element.get();
		assertEquals(expected, actual);		
	}

	@Test
	final void testAddIfNotPresent() {
		Matrix<Integer> matrix = new Matrix<Integer>();
		assertTrue(	matrix.addIfNotPresent(1, 2, 3) );
		assertFalse( matrix.addIfNotPresent(1, 2, 3) );
	}
	
	@Test
	final void testAddIfNotPresentThrowsIAExc() {
		Matrix<Integer> matrix = new Matrix<Integer>();
		assertThrows(IllegalArgumentException.class, () -> matrix.addIfNotPresent(1, -2,  3) );
		assertThrows(IllegalArgumentException.class, () -> matrix.addIfNotPresent(1,  2, -3) );
	}

	@Test
	final void testRemove() {
		Matrix<Integer> matrix = new Matrix<Integer>();
		Optional<Integer> element = matrix.remove(0, 0);
		assertNull(element);
		
		final int expected = 1;
		matrix.addIfNotPresent(expected, 2, 3);
		element = matrix.remove(2, 3);
		int actual = element.get();
		assertEquals(expected, actual);
	}

	@Test
	final void testGetDimAfterRemove() {
		Matrix<Integer> matrix = new Matrix<Integer>();
		final int expectedX = 2;
		final int expectedY = 3;
		matrix.addIfNotPresent(1, expectedX, expectedY);
		matrix.addIfNotPresent(4, 5, 6);
		matrix.remove(5, 6);
		assertEquals(expectedX, matrix.getDimX());
		assertEquals(expectedY, matrix.getDimY());
	}
	
	@Test
	final void testReplace() {
		Matrix<String> matrix = new Matrix<String>();
		final String expected = "first";
		matrix.addIfNotPresent(expected, 2, 3);		
		Optional<String> element = matrix.replace(2, 3, "2nd");
		String actual = element.get();
		assertEquals(expected, actual);		

	}

	
	@Test
	final void testIsEmpty() {
		Matrix<String> matrix = new Matrix<String>();
		assertTrue(matrix.isEmpty());		
		matrix.addIfNotPresent(null, 1, 0);
		assertFalse(matrix.isEmpty());	
		matrix.remove(1, 0);
		assertTrue(matrix.isEmpty());	
	}
	
	@Test
	final void testGetMatrixDisplayerData() {
		Matrix<Integer> matrix = new Matrix<Integer>();
		matrix.addIfNotPresent(null, 1, 0);
		matrix.addIfNotPresent(10, 0, 1);
		
		final String expected = "+           \n  .    null \n  10   .    \n";
		
		MatrixDisplayer displayer = new MatrixDisplayer(matrix, ".")
				.setLeftIndent(0)
				.setSpacing(1);
		String actual = displayer.display();
	
		assertEquals(expected, actual);		
	}
	
	
	@Test
	final void testExtract() {
		class Element {
			int value;
			public Element(int value) {this.value = value;}
			@Override
			public String toString() {return "~" + value + "~";}
		}
		Matrix<Element> matrix = new Matrix<>();
		matrix.addIfNotPresent(null, 1, 0);
		matrix.addIfNotPresent(new Element(10), 0, 2);
		Matrix<String> extractedMatrix = matrix.extract(Element::toString);

		assertEquals(matrix.getDimX(), extractedMatrix.getDimX());	
		assertEquals(matrix.getDimY(), extractedMatrix.getDimY());	
		assertEquals(matrix.toString(), extractedMatrix.toString());		
	}
}
