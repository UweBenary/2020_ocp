package matrix.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import matrix.Matrix;
import matrix.MatrixDisplayer;
import matrix.MatrixFactory;

class MatrixDisplayerTest {

	private MatrixDisplayer displayer;
	
	@BeforeEach
	final void setupData() {
		Matrix<Integer> data = MatrixFactory.of( new Integer[][] { {1,2, 3},{4,5,6} } );
		displayer = new MatrixDisplayer(data, "X");
	}
	
	@Test
	final void testInstanciationFailsForNull() {
		assertThrows(IllegalArgumentException.class, () -> new MatrixDisplayer(new Matrix<Integer>(), null) );
	}
	
	@Test
	final void testSetColumnNamesStringArray() {
		try {
			displayer.setColumnNames( new String[] {"1","2","3"});
		} catch (Exception e) {
			fail("Unexpected exception", e);
		}
		assertThrows(IllegalArgumentException.class, () -> displayer.setColumnNames( new String[] {}) );
		assertThrows(IllegalArgumentException.class, () -> displayer.setColumnNames( new String[] {"1","2"}) );
		assertThrows(IllegalArgumentException.class, () -> displayer.setColumnNames( new String[] {"1", null, "3"}) );
	}

	@Test
	final void testSetColumnNamesCollectionOfString() {
		List<String> list = Arrays.asList( new String[] {"1","2","3"});
		try {
			displayer.setColumnNames( list );
		} catch (Exception e) {
			fail("Unexpected exception", e);
		}
	}

	@Test
	final void testSetRowNamesStringArray() {
		assertThrows(IllegalArgumentException.class, () -> displayer.setColumnNames( new String[] {}) );
		assertThrows(IllegalArgumentException.class, () -> displayer.setColumnNames( new String[] {"1", null}) );
	}

	@Test
	final void testSetRowNamesCollectionOfString() {
		List<String> list = Arrays.asList( new String[] {"1","2"});
		try {
			displayer.setRowNames( list );
		} catch (Exception e) {
			fail("Unexpected exception", e);
		}
	}

	@Test
	final void testSetSpacingFailsNegative() {
		assertThrows(IllegalArgumentException.class, () -> displayer.setSpacing(-1) );
	}

	@Test
	final void testSetLeftIndentFailsNegative() {
		assertThrows(IllegalArgumentException.class, () -> displayer.setLeftIndent(-1) );
	}

	@Test
	final void testToString() {
		final String expected = "MatrixDisplayer[columnNames=null, rowNames=null, "
				+ "data=[3 columns,2 rows], spacing=2, leftIndent=0, "
				+ "topLeftCornerSymbol=+]\n";
		String actual = displayer.toString();
		assertEquals(expected, actual);
	}
	
	@Test
	final void testTranspose() {
		final String expected = "+       \n  1  4  \n  2  5  \n  3  6  \n";
		String actual = displayer.transpose().display();
		assertEquals(expected, actual);
	}

	@Test
	final void testDisplay() {
		final String expected = "+  a  b  c  \ni  1  2  3  \nii 4  5  6  \n";
		displayer.setColumnNames("a","b","c");
		displayer.setRowNames("i","ii");
		String actual = displayer.display();
		assertEquals(expected, actual);
	}
}
