package algorithm;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import alignment.Alignment;
import alignment.Word;
import scoring.Score;

class SmithWatermanTest {

	@Test
	final void testGetAlignment1stExample() {//https://de.wikipedia.org/wiki/Smith-Waterman-Algorithmus#Beispiel

		final Word query = new Word("acga"); 
		final Word target = new Word("tccg");

		final int gapOpeningPenalty = 1;
		final int gapExtensionPenalty = 0;		
		final GapPenalty gapPenalty = GapPenalty.getLinearModel(gapOpeningPenalty, gapExtensionPenalty);

		final SimilarityScoreFunction similarityScore = (char x1, char x2) -> new Score( x1 =='\u0000' || x1!=x2 ? -1 : 2 );
		
		final Algorithm sw = new SmithWaterman(gapPenalty, similarityScore);
		Alignment al = sw.getAlignment(query, target);
		
		final String expected =	"  acga\n   || \n" + "tc cg \n";
		final String actual = al.toString();
		
		assertEquals(expected, actual);
	}
	
	@Test
	final void testGetAlignment2ndExample() {// https://en.wikipedia.org/wiki/Smith%E2%80%93Waterman_algorithm#Example
		final Word query = new Word("TGTTACGG"); 
		final Word target = new Word("GGTTGACTA");
		
		final int gapOpeningPenalty = 0;
		final int gapExtensionPenalty = 2;		
		final GapPenalty gapPenalty = x -> gapExtensionPenalty + gapOpeningPenalty;

		final SimilarityScoreFunction similarityScore = (char x1, char x2) -> new Score( x1 =='\u0000' || x1!=x2 ? -3 : 3 );
		
		final SmithWaterman sw = new SmithWaterman(gapPenalty, similarityScore);

		sw.constructScoringMatrix(query, target);
//		System.out.println(sw);	
		
		Alignment al = sw.backTrace();
//		System.out.println(al);
		
		final String expected =	" TGTT ACGG  \n  ||| ||    \n" + "G GTTGAC  TA\n";
		final String actual = al.toString();
		
		assertEquals(expected, actual);
	}
	
	@Test
	final void testGetAlignment3rdExample() {
		final Word query = new Word("GTTAC"); 
		final Word target = new Word("GTTGAC");
		
		final int gapOpeningPenalty = 0;
		final int gapExtensionPenalty = 2;		
		final GapPenalty gapPenalty = x -> gapExtensionPenalty + gapOpeningPenalty;

		final SimilarityScoreFunction similarityScore = (char x1, char x2) -> new Score( x1 =='\u0000' || x1!=x2 ? -3 : 3 );
		
		final SmithWaterman sw = new SmithWaterman(gapPenalty, similarityScore);

		sw.constructScoringMatrix(query, target);
//		System.out.println(sw);	
		
		Alignment al = sw.backTrace();
//		System.out.println(al);
		
		final String expected =	"GTT AC\n||| ||\n" + "GTTGAC\n";
		final String actual = al.toString();
		
		assertEquals(expected, actual);
	}
}
