package algorithm;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class GapPenaltyTest {

	@Test
	void testGetLinearModel() {
		final int gapOpeningPenalty = 2;
		final int gapExtensionPenalty = 1;
		final int gapLength = 1;
		
		final int expected = gapOpeningPenalty * gapLength + gapExtensionPenalty;
		
		GapPenalty gp = GapPenalty.getLinearModel(gapOpeningPenalty, gapExtensionPenalty);
		int actual = gp.ofLength(gapLength);
		
		assertEquals(expected, actual);
	}

}
