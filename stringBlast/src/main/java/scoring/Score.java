package scoring;

public class Score implements Comparable<Score> {
	
	public final static Score ZERO_SCORE = new Score(0);
	public final static Score MIN_SCORE = ZERO_SCORE;
	private int value;

	public Score(int value) {
		this.value = value;
	}	
	
	public int getValue() {
		return value;
	}

	public Score add(Score score) {
		return new Score( value + score.getValue() );
	}
	
	public Score substract(Score score) {
		return new Score( value - score.getValue() );
	}
	
	@Override
	public String toString() {
		return "" + value;
	}
	
	@Override
	public boolean equals(Object obj) {
		if ( ! (obj instanceof Score) ) 
			return false;
		
		return getValue() == ((Score) obj).getValue();
	}

	@Override
	public int compareTo(Score o) {
		return getValue() - o.getValue();
	}
}
