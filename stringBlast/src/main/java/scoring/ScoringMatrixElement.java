package scoring;


public abstract class ScoringMatrixElement implements Comparable<ScoringMatrixElement> {


	abstract public boolean hasPredecessor();
	abstract public ScoringMatrixElement getPredecessor();

	
	
	private final int queryIdx;
	private final int targetIdx;
	private Score score;
	
	public ScoringMatrixElement(int queryIdx, int targetIdx, Score score) {
		this.queryIdx = queryIdx;
		this.targetIdx = targetIdx;
		this.score = new Score( score.getValue() );
	}
	

	public int getQueryIndex() {
		return queryIdx;
	}

	public int getTargetIndex() {
		return targetIdx;
	}
	
	public Score getScore() {
		return new Score( score.getValue() );
	}
	
/**
 * compares Score values only
 */
	@Override
	public int compareTo(ScoringMatrixElement o) {
		return getScore().compareTo(o.getScore());
	}	
	
	
	@Override
	public String toString() {
		return "[" + queryIdx + ", " + targetIdx + ", " + score + "]";
	}


	/*
	 * equals and hashCode
	 * 	do not consider predecessor to prevent recursive calls
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + queryIdx;
		result = prime * result + ((score == null) ? 0 : score.hashCode());
		result = prime * result + targetIdx;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScoringMatrixElement other = (ScoringMatrixElement) obj;
		if (queryIdx != other.queryIdx)
			return false;
		if (score == null) {
			if (other.score != null)
				return false;
		} else if (!score.equals(other.score))
			return false;
		if (targetIdx != other.targetIdx)
			return false;
		return true;
	}

		
}
