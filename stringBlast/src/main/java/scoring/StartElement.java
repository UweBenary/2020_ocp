package scoring;

public class StartElement extends ScoringMatrixElement {

	public StartElement(int queryIdx, int targetIdx, Score score) {
		super(queryIdx, targetIdx, score);
	}

	@Override
	public boolean hasPredecessor() {
		return false;
	}

	@Override
	public ScoringMatrixElement getPredecessor() {
		return null;
	}

}
