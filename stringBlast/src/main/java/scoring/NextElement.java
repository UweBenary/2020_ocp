package scoring;

public class NextElement extends ScoringMatrixElement {

	
	private ScoringMatrixElement predecessor;

	public NextElement(int queryIdx, int targetIdx, Score score, ScoringMatrixElement predecessor) {
		super(queryIdx, targetIdx, score);
		this.predecessor = predecessor;
	}

	public ScoringMatrixElement getPredecessor() {
		return predecessor;
	}

	@Override
	public boolean hasPredecessor() {
		return predecessor != null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((predecessor == null) ? 0 : predecessor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		NextElement other = (NextElement) obj;
		if (predecessor == null) {
			if (other.predecessor != null)
				return false;
		} else if (!predecessor.equals(other.predecessor))
			return false;
		return true;
	}

	
}
