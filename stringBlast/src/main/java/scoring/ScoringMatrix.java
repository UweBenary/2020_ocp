package scoring;

import alignment.Word;
import matrix.Matrix;
import matrix.MatrixDisplayer;
//import matrix.MatrixFactory;

public class ScoringMatrix { 
	
	private Word query;
	private Word target;
	private ScoringMatrixElement maxScoreElement = new StartElement(0, 0, Score.MIN_SCORE);
	private Matrix<ScoringMatrixElement> matrix = new Matrix<ScoringMatrixElement>();
	
	public ScoringMatrix(Word query, Word target) {
		this.query = query;
		this.target = target;
	}

	public Word getQuery() {
		return query;
	}

	public Word getTarget() {
		return target;
	}
	
	public ScoringMatrixElement getMaxScoreElement() {
		return maxScoreElement;
	}

	public boolean isEmpty() {
		return matrix.isEmpty();
	}


	public int queryDim() {
		return matrix.getDimX();
	}
	
	public int targetDim() {
		return matrix.getDimY();
	}

	/*
	 *  add and getXXX 	
	 */
	public boolean addElement(ScoringMatrixElement element) {
		int x = element.getQueryIndex();
		int y = element.getTargetIndex();
		if ( x > query.length() || y > target.length() ) {
			return false;
		}
		
		boolean addSuccessfully = matrix.addIfNotPresent(element, x, y);
		
		if (addSuccessfully && maxScoreElement.compareTo(element) < 0) {
			maxScoreElement = element;
		}
		
		return addSuccessfully;
	}
	
	public ScoringMatrixElement getElement(int queryIdx, int targetIdx) {
		if (matrix.isPresent(queryIdx, targetIdx)) {
			return matrix.getElement(queryIdx, targetIdx).get();
		}
		return null;
	}
	
	public Score getScore(int queryIdx, int targetIdx) {
		return getElement(queryIdx, targetIdx).getScore();
	}



	
	/*
	 * toString
	 */
		
	@Override
	public String toString() {			
		return "maxScoreElement: " + maxScoreElement.toString() + "\n" 
				+ getDisplayer(matrix).display();
	}

	public String toStringScores() {
		Matrix<Score> m = matrix.extract(ScoringMatrixElement::getScore); 
		return getDisplayer(m).display();
	}
	
	public String toStringPredecessor() {
		Matrix<ScoringMatrixElement> m = matrix.extract(e -> e.getPredecessor()); 
		return getDisplayer(m).display();
	}

	
	private MatrixDisplayer getDisplayer(Matrix<?> original) {
//		MatrixDisplayer displayer = new MatrixDisplayer(fillUp(original), "") // fillUp should not run, only needed for UnitTest
		MatrixDisplayer displayer = new MatrixDisplayer(original, "")
				.setTopLeftCornerSymbol(null)
				.setLeftIndent(1)
				.setSpacing(2)
				.setColumnNames(query.toStringArray())
				.setRowNames(target.toStringArray());
		return displayer;
	}

	
//
//	private <T> Matrix<T> fillUp(Matrix<T> original) {
//		Matrix<T> copy = MatrixFactory.of(original);
//		boolean filled = copy.addIfNotPresent(null, query.length()-1, target.length()-1);
//		
//		if(filled) System.err.println("Error in ScoringMatrix::fillUp"); // addIfNotPresent should not run, only needed for UnitTest
//		
//		return copy;
//	}
}
