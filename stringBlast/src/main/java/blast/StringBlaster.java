package blast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

import algorithm.Algorithm;
import alignment.Alignment;
import alignment.Word;

public class StringBlaster {

	private Algorithm algorithm; 

	
	public StringBlaster(Algorithm algorithm) {
		this.algorithm = algorithm;
	}

	public Alignment align(Word query, Word target) {	
		return algorithm.getAlignment(query, target);
	}
	
	public Collection<Alignment> alignMulti(Word query, Collection<Word> targets) {
		
//		Collection<Alignment> alignments = new ArrayList<>();
//		
//		for(Word target : targets) {
//			alignments.add( align(query, target) );
//		}
//		
//		return alignments;
		
		return targets.stream()
				.map( target -> align(query, target) )
				.collect(Collectors.toCollection(ArrayList::new));
		

	}
}
