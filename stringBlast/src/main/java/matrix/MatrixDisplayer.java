package matrix;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class MatrixDisplayer {
	
	private static final int SPACING = 2;
	private static final int INDENT = 0;
	private static final String SYMBOL = "+";
	
	private int spacing = SPACING;		
	private int leftIndent = INDENT;
	private String topLeftCornerSymbol = SYMBOL;
	private String[] columnNames, rowNames; 

	private String[][] data;

	
	public MatrixDisplayer(Matrix<?> matrix, String missingValueString) {
		if (matrix.isEmpty()) 
			throw new IllegalArgumentException("Matrix must not be empty.");
		if (missingValueString == null) {
			missingValueString = "";
		}
		data = as2DStringArray(matrix, missingValueString);
	}
	
	private <V> String[][] as2DStringArray(Matrix<V> matrix, String missingValueString) {
		
		int lengthX = matrix.getDimX()+1;
		int lengthY = matrix.getDimY()+1;
		String[][] result = new String[lengthY][lengthX] ;
		
		for (int x = 0; x < lengthX; x++) {
			for (int y = 0; y < lengthY; y++) {
				
				if ( ! matrix.isPresent(x, y) ) {
					result[y][x] = missingValueString;
					continue;
				}
				
				Optional<V> element = matrix.getElement(x, y);
				result[y][x] = element.isEmpty() ? "null" : element.get().toString();				
			}
		}
		return result;
	}

	private int nRows() {
		return data.length;
	}
	
	private int nColumns() {
		return data[0].length;
	}

	/*
	 * Setters
	 */
	public MatrixDisplayer setSpacing(int spacingNotNegative) {
		if (spacingNotNegative < 0 ) {
			throw new IllegalArgumentException("Spacing must be non-negative");
		}
		this.spacing = spacingNotNegative;
		return this;
	}
	
	public MatrixDisplayer setLeftIndent(int leftIndentNotNegative) {
		if (leftIndentNotNegative < 0 ) {
			throw new IllegalArgumentException("Indent must be non-negative");
		}
		this.leftIndent = leftIndentNotNegative;
		return this;
	}
	
	public MatrixDisplayer setTopLeftCornerSymbol(String topLeftCornerSymbol) {
		if (topLeftCornerSymbol == null) {
			this.topLeftCornerSymbol = " "; 		
			return this;
		}
		this.topLeftCornerSymbol = topLeftCornerSymbol;
		return this;
	}
	
	public MatrixDisplayer setColumnNames(String...strings) {
		if (strings.length != nColumns()) 
			throw new IllegalArgumentException("Number of columns in columnNames and data mismatch.");
		for (String string : strings) {
			if (string == null) 
				throw new IllegalArgumentException("ColumnNames must not have elements of 'null'.");
		}
		columnNames = strings;
		return this;
	}
	
	public MatrixDisplayer setColumnNames(Collection<String> strings) {
		setColumnNames( strings.toArray(new String[] {}) );
		return this;
	}
	
	public MatrixDisplayer setRowNames(String...strings) {
		if (strings.length != nRows()) 
			throw new IllegalArgumentException("Number of rows in rowNames and data mismatch.");
		for (String string : strings) {
			if (string == null) 
				throw new IllegalArgumentException("RowNames must not have elements of 'null'.");
		}
		rowNames = strings;
		return this;
	}
	
	public MatrixDisplayer setRowNames(Collection<String> strings) {
		setRowNames( strings.toArray(new String[] {}) );
		return this;
	}
	
	/*
	 * other public instance methods
	 */
	
	@Override
	public String toString() {
		return  String.format(
				"MatrixDisplayer[columnNames%s, rowNames%s, "
				+ "data=[%d columns,%d rows], spacing=%d, leftIndent=%d, "
				+ "topLeftCornerSymbol=%s]\n", 
				
				columnNames == null ? "=null" : ".length=" + columnNames.length, 
				rowNames == null ?  "=null" : ".length=" + rowNames.length,
				nColumns(),		
				nRows(), 
				spacing, 
				leftIndent, 
				topLeftCornerSymbol);
	}
	
	public MatrixDisplayer transpose() {
		String[] dump = columnNames;
		columnNames = rowNames;
		rowNames = dump;
		
		String[][] dataTranspose = new String[nColumns()][nRows()];
		for (int i = 0; i < nRows(); i++) {
			for (int j = 0; j < nColumns(); j++) {
				dataTranspose[j][i] = data[i][j];
			}
		}
		data = dataTranspose;
		
		return this;
	}
	
	public String display() {
		return new Formatter().getString();
	}
	
	
	/*  *******************************************************
	 * 
	 * Formatter
	 * 
	 */
	private class Formatter {

		public String getString() {
			columnNamesIfNullCreateDefault();
			rowNamesIfNullCreateDefault();
			
			StringBuilder resultStringBuilder = new StringBuilder();
			String formatString = buildFormatString();	
			
			String header = buildHeaderString(formatString);
			resultStringBuilder.append(header);
		
			for (int i = 0; i < nRows(); i++) {
				List<String> input = new ArrayList<>();
				input.add(rowNames[i]);
				for (String string : data[i]) {
					input.add(string);
				}
				String row = String.format(formatString, input.toArray());
				resultStringBuilder.append(row);
			}		
					
			return resultStringBuilder.toString();
		}

		private String buildHeaderString(String formatString) {
			List<String> input = new ArrayList<>();
			input.add(topLeftCornerSymbol);
			for (String string : columnNames) {
				input.add(string);
			}
			String header = String.format(formatString, input.toArray());
			return header;
		}

		private String buildFormatString() {
			int columnSpacing = Math.max( getMaxStringLength(data), getMaxStringLength(columnNames) ) + Math.max(spacing, 1);
			int indentSpacing = Math.max( topLeftCornerSymbol.length(), getMaxStringLength(rowNames) ) + Math.max(leftIndent, 1);

			StringBuilder formatStringBuilder = new StringBuilder("%-" + indentSpacing + "s");

			for (int i = 0; i < nColumns(); i++) {
				formatStringBuilder.append("%-" + columnSpacing + "s");
			}

			return formatStringBuilder.append("\n").toString();
		}

		private void rowNamesIfNullCreateDefault() {
			if (rowNames == null) {
				List<String> list = Stream.generate(() -> "")
					.limit(nRows())
					.collect(Collectors.toList());
				rowNames = list.toArray( new String[] {});
			}
		}

		private void columnNamesIfNullCreateDefault() {
			if (columnNames == null) {
				List<String> list = Stream.generate(() -> "")
					.limit(nColumns())
					.collect(Collectors.toList());
				columnNames = list.toArray( new String[] {});
			}
		}

		private int getMaxStringLength(String[] array) {
			if (array == null) {
				return 0;
			}
			
			OptionalInt max = Stream.of(array)
					.mapToInt(String::length)
					.max();	
		
			return max.getAsInt();
		}

		private int getMaxStringLength(String[][] array2D) {
			OptionalInt max;
			
			try {
				max = Stream.of(array2D)
					.flatMap(array -> Stream.of(array))
					.mapToInt(String::length)
					.max();
				
			} catch (NullPointerException e) {
				throw new NullPointerException("MatrixDisplayerData must not have elemnts of 'null'.");
			}
			
			if (max.isEmpty()) {
				throw new IllegalArgumentException("Data is empty.");
			}
			
			return max.getAsInt();
		}

		
	}

}
