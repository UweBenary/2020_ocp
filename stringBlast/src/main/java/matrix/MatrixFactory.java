package matrix;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class MatrixFactory {
	
	
	public static <V> Matrix<V> of(Matrix<V> matrix) {
		return matrix.copy();
	}
	
	public static <V> Matrix<V> of(V[][] data) {
		Matrix<V> matrix = new Matrix<V>();
		for (int y = 0; y < data.length; y++) {
			for (int x = 0; x < data[y].length; x++) {
				matrix.addIfNotPresent(data[y][x], x, y);
			}
		}
		return matrix;
	}

	public static <V> Matrix<V> of(V[] data, int nPartitions) {
		List<V> list = Arrays.asList(data);
		return of(list, nPartitions);
	}
	
	public static <V> Matrix<V> of(Collection<V> data, int nPartitions) {
		if (nPartitions < 1) {
			throw new IllegalArgumentException("nPartions < 1");
		}
		
		Matrix<V> matrix = new Matrix<V>();
		List<V> list = new ArrayList<V>(data);
		
		for (int i = 0; i < list.size(); i++) {
			int x = i % nPartitions;	
			int y = i / nPartitions;
			matrix.addIfNotPresent(list.get(i), x, y);
		}
		
		return matrix;
	}	
	
}
