package matrix;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.function.Function;
import java.util.function.ToIntFunction;

/*
 * matrix may have no element at a certain index (x,y) -> use isPresent()
 * elements ( Optional.ofNullable(V) ) of matrix may be null ( Optional.empty() ) or non-null ( Optional.of(V) )
 */
public class Matrix<V> {
		
	public static <T> Matrix<T> of(Matrix<T> matrix) {
		Matrix<T> copy = new Matrix<T>();
		copy.data = matrix.data;
		copy.dimX = matrix.dimX;
		copy.dimY = matrix.dimY;
		return copy;
	}
	
	
	private static final String SEPARATOR = "_";
	
	private Map<String, Optional<V>> data = new HashMap<>();
	
	private int dimX;
	private int dimY;
	
	
	public Matrix() {
		data.clear();
		dimX = 0;
		dimY = 0;
	}


	
	/*
	 * Getters/Setters
	 */
	
	public boolean isEmpty() {
		return data.isEmpty();
	}	
	
	public int getDimX() {
		return dimX;
	}	
	
	public int getDimY() {
		return dimY;
	}

	public boolean isPresent(int x, int y) {
		String key = getKey(x, y);
		if (data.containsKey(key)) {
			return true;
		}
		return false;
	}

	public Optional<V> getElement(int x, int y) {
		if ( ! isPresent(x, y) ) {
			return null;
		}
		String key = getKey(x, y);
		return data.get(key);
	}
	
	
	/*
	 * copy and extract
	 */
		
	public Matrix<V> copy() {
		Matrix<V> copy = new Matrix<>();
		copy.data = data;
		copy.dimX = dimX;
		copy.dimY = dimY;
		return copy;
	}
	
	public <R> Matrix<R> extract(Function<V, R> extractor) {
		Objects.requireNonNull(extractor);
		
		Matrix<R> matrix = new Matrix<R>();
		
		if ( isEmpty() ) {
			return matrix;
		}
			
		for( String key : data.keySet() ) {
			Optional<V> original = data.get(key);			
			Optional<R> newElement = original.map(extractor);
			matrix.data.put(key, newElement);
		}

		matrix.dimX = dimX;		
		matrix.dimY = dimY;
		
		return matrix;
	}	

	
	/*
	 * hashCode/equals and toString
	 */
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + dimX;
		result = prime * result + dimY;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Matrix<?> other = (Matrix<?>) obj;
		if (dimX != other.dimX)
			return false;
		if (dimY != other.dimY)
			return false;
		if (data == null ^ other.data == null)
			return false;
		if (data != null && !data.equals(other.data))
			return false;

		return true;
	}

	@Override
	public String toString() {
		
		MatrixDisplayer displayer = new MatrixDisplayer(this, null)
				.setTopLeftCornerSymbol(null)
				.setLeftIndent(0)
				.setSpacing(1);
		return displayer.display();
	}
	
	
	/*
	 * public mutator methods
	 */

	public boolean addIfNotPresent(V element, int x, int y) {
		if (x < 0 || y < 0) {
			throw new IllegalArgumentException("Indexes must not be < 0");
		}
		
		if ( isPresent(x, y) ) {
			return false;
		}
		add(element, x, y);
		return true;
	}
	
	public Optional<V> remove(int x, int y) {
		if ( ! isPresent(x, y) ) {
			return null;
		}
		
		Optional<V> old = data.remove( getKey(x, y) );
		
		dimX = findMaxIndexInKeysBy(Idx.X);
		dimY = findMaxIndexInKeysBy(Idx.Y);
		
		return old;
	}

/**
 * 
 * @param x : matrix index
 * @param y : matrix index
 * @param element replaces (oldElement or null) at (x,y)
 * @return isPresent(x,y) ? ( oldElement==null ? Optional.empty() : Optional.of(oldElement) ) : null  
 */
	public Optional<V> replace(int x, int y, V element) {
		Optional<V> old = remove(x, y);
		add(element, x, y);
		return old;
	}


	/*
	 * private
	 */

	private String getKey(int x, int y) {
		return x + SEPARATOR + y;
	}
	
	private void add(V element, int x, int y) {
		String key = getKey(x, y);
		data.put(key, Optional.ofNullable(element));
		dimX = Math.max(dimX, x);
		dimY = Math.max(dimY, y);
	}

	private int findMaxIndexInKeysBy(Idx idx) {
		ToIntFunction<String> mapper = s -> idxFromKey(s, idx);
		
		OptionalInt max = data.keySet().parallelStream()
			.mapToInt(mapper)
			.max();
		
		return max.orElse(0);
	}

	private enum Idx { X, Y	};
	
	private int idxFromKey(String key, Idx idx) {		  
		String[] indexes = key.split(SEPARATOR); 
		int i = idx.ordinal();
		
		return (int) Integer.valueOf(indexes[i]);
	}
		
}
