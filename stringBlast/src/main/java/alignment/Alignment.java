package alignment;

import algorithm.SimilarityScoreFunction;
import scoring.Score;

public class Alignment {
	private final Word query;
	private final Word target;
	private String associationMarkers;
	
	public Alignment(Word query, Word target) {
		
		ifLengthsDifferThrowIllegalArgumentException(query, target);
		
		this.query = query;
		this.target = target;
	}

	private void ifLengthsDifferThrowIllegalArgumentException(Word query, Word target) {
		if ( query.length() != target.length() ) 
			throw new IllegalArgumentException("Length of arguments must not differ.");
	}

	/*
	 * Getters
	 */

	public Word getQuery() {
		return new Word(query.toString());
	}

	public Word getTarget() {
		return new Word(target.toString());
	}


	public Score score(SimilarityScoreFunction scoringFunction) {		
		Score score = new Score(0);
		
		for (int i = 1; i < query.length(); i++) {			
			Score nextScore = scoringFunction.score( query.charAt(i), target.charAt(i) );
			score = score.add( nextScore );			
		}
		
		return score;
	}
	
	@Override
	public String toString() {
		return  query.toString() + "\n" + 
				getAssociationMarkers() + "\n" + 
				target.toString() + "\n";
	}

	public String getAssociationMarkers() {
		if (associationMarkers == null) {
			return createAssociationMarkers();
		}
		
		return associationMarkers;
	}

	private String createAssociationMarkers() {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 1; i < query.length(); i++) {		
			sb.append( getMarkerAt(i) );			
		}		
		return sb.toString() ;
	}

	private String getMarkerAt(int index) {
		if (query.charAt(index) == '\u0000' || target.charAt(index) == '\u0000' ) 
			return " ";

		return "|";
	}

}
