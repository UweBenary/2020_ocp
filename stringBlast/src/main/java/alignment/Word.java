package alignment;

public class Word 
	implements Cloneable {
	
	private final String value;
	
	public Word(String value) {
		if (value == null || value.isBlank() ) {
			throw new IllegalArgumentException("Argument must not be null or blanks.");
		}
		
		this.value = '\u0000' + value;
	}
	
	public String getValue() {
		return value;
	}

	public char charAt(int index) {
		return value.charAt(index);
	}
	
	public int length() {
		return value.length();
	}
	
	@Override
	public boolean equals(Object obj) {
		if ( obj instanceof Word ) {
			Word other = (Word) obj;
			return toString().equals( other.toString() );
		}
		return false;
	}
	
	@Override
	public String toString() {
		return getValue().substring(1).replace('\u0000', ' ');
	}

	public String[] toStringArray() {			
		String[] result = new String[ length() ];
		for (int i = 0; i < length(); i++) {
			result[i] = "" + charAt(i);					
		}
		result[0] = "/-"; // replace('\u0000', '-')
		return result;
	}
}
