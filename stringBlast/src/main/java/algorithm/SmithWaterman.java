package algorithm;

import java.util.TreeSet;

import alignment.Alignment;
import alignment.Word;
import scoring.NextElement;
import scoring.Score;
import scoring.ScoringMatrix;
import scoring.ScoringMatrixElement;
import scoring.StartElement;

public class SmithWaterman extends Algorithm {		
	
	private Word query;
	private Word target;
	private ScoringMatrix scoringMatrix;

	
	public SmithWaterman(GapPenalty gapPenalty, SimilarityScoreFunction similarityScore) {
		super(gapPenalty, similarityScore);
	}

	@Override
	protected void constructScoringMatrix(Word query, Word target) {		
		this.query = query;
		this.target = target;	
		scoringMatrix = new ScoringMatrix(query, target);
		
		fillMatrix();	
	}
	
	@Override
	protected Alignment backTrace() {
		BackTracer backTracer = new BackTracer(scoringMatrix);  // so far only default implemented -> in future BackTracerFactory in SmithWaterman constructor?
		return backTracer.trace();
	}

	@Override
	public String toString() {
		return scoringMatrix.toString();
	}

	
	/*
	 * fill scoring matrix
	 */
	
	private void fillMatrix() {	
				
		for (int queryIdx = 0; queryIdx < query.length(); queryIdx++) {
			for (int targetIdx = 0; targetIdx < target.length(); targetIdx++) {
												
				ScoringMatrixElement newElement = createMatrixElement(queryIdx, targetIdx);		
				scoringMatrix.addElement(newElement);
			}
		}
	}

	private ScoringMatrixElement createMatrixElement(int queryIdx, int targetIdx) {
		if (queryIdx == 0 || targetIdx == 0) 
				return startNewTrace(queryIdx, targetIdx);
		
		return getNextTraceElement(queryIdx, targetIdx);
	}
	

	private ScoringMatrixElement startNewTrace(int queryIdx, int targetIdx) {
		return new StartElement(queryIdx, targetIdx, Score.ZERO_SCORE);
	}

	private ScoringMatrixElement getNextTraceElement(int queryIdx, int targetIdx) {		
		ScoringMatrixElement element1 = similarity(queryIdx, targetIdx);
		ScoringMatrixElement element2 = queryGap(queryIdx, targetIdx);
		ScoringMatrixElement element3 = targetGap(queryIdx, targetIdx);
		ScoringMatrixElement element4 = startNewTrace(queryIdx, targetIdx);
		
		return getHighestScoreElement(element1, element2, element3, element4);
	}


	/*
	 * similarity
	 */
	private ScoringMatrixElement similarity(int queryIdx, int targetIdx) {						
		char queryChar = query.charAt(queryIdx);
		char targetChar = target.charAt(targetIdx);	
		Score similarityScore = getSimilarityScoreFunction().score(queryChar, targetChar);
		
		ScoringMatrixElement predecessor = scoringMatrix.getElement(queryIdx-1, targetIdx-1);
		Score predecessorScore = predecessor.getScore();		
		Score newScore = predecessorScore.add(similarityScore);
		
		return  new NextElement(queryIdx, targetIdx, newScore, predecessor );
	}
	
	
	/*
	 * query gap penalty
	 */
	private ScoringMatrixElement queryGap(int queryIdx, int targetIdx) {		
		ScoringMatrixElement neighbor = scoringMatrix.getElement(queryIdx, targetIdx-1);
		
		Score neighborScore = neighbor.getScore();
		Score penaltyScore = queryGapPanelty(queryIdx, targetIdx);
		Score newScore = neighborScore.substract(penaltyScore);
				
		return  new NextElement(queryIdx, targetIdx, newScore, neighbor);
	}
	
	
	private Score queryGapPanelty(int queryIdx, int targetIdx) {
		int gapLength = getQueryGapLength(queryIdx, targetIdx);		
		int penalty = getGapPenalty().ofLength(gapLength);
		return new Score( penalty );
	}

	private int getQueryGapLength(int queryIdx, int targetIdx) {
		int gapLength = 0;
		ScoringMatrixElement neighbor = scoringMatrix.getElement(queryIdx, targetIdx-1-gapLength);
		
		while ( neighbor.hasPredecessor() && neighbor.getPredecessor().getQueryIndex() == queryIdx ) {
			gapLength++;
			neighbor = scoringMatrix.getElement(queryIdx, targetIdx-1-gapLength);			
		}
		return gapLength;
	}
	
	/*
	 * target gap penalty
	 */
	private ScoringMatrixElement targetGap(int queryIdx, int targetIdx) {
		ScoringMatrixElement neighbor = scoringMatrix.getElement(queryIdx-1, targetIdx);
				
		Score neighborScore = neighbor.getScore();
		Score penaltyScore = targetGapPanelty(queryIdx, targetIdx);
		Score newScore = neighborScore.substract(penaltyScore);
				
		return  new NextElement(queryIdx, targetIdx, newScore, neighbor);
	}
	

	private Score targetGapPanelty(int queryIdx, int targetIdx) {
		int gapLength = getTargetGapLength(queryIdx, targetIdx);
		int penalty = getGapPenalty().ofLength(gapLength);
		return new Score( penalty );
	}
	
	
	private int getTargetGapLength(int queryIdx, int targetIdx) {
		int gapLength = 0;
		ScoringMatrixElement neighbor = scoringMatrix.getElement(queryIdx-1-gapLength, targetIdx);
		
		while ( neighbor.hasPredecessor() && neighbor.getPredecessor().getQueryIndex() == queryIdx ) {
			gapLength++;
			neighbor = scoringMatrix.getElement(queryIdx-1-gapLength, targetIdx);			
		}
		return gapLength;
	}

	/*
	 *   getHighestScoreElement
	 */
	
	private ScoringMatrixElement getHighestScoreElement(ScoringMatrixElement... elements) {
		TreeSet<SortHelper> eList = new TreeSet<>();
		
		for (int priority = 0; priority < elements.length; priority++) {
			eList.add( new SortHelper(elements[priority], priority) );
		}

		return eList.last().getElement();	
	}
	
	private static class SortHelper implements Comparable<SortHelper> {
		
		private ScoringMatrixElement element;
		private int priority;
				
		SortHelper(ScoringMatrixElement element, int priority) {
			this.element = element;
			this.priority = priority;
		}

		@Override
		public int compareTo(SortHelper other) {
			int result = element.compareTo(other.element);
			if (result == 0) {
				result = priority - other.priority;
			}
			return result;
		}
		
		ScoringMatrixElement getElement() {
			return element;
		}
	}	


}
