package algorithm;

@FunctionalInterface
public interface GapPenalty {

	/*
	 * https://en.wikipedia.org/wiki/Gap_penalty#Types
	 */
	
	public int ofLength(int gapLength);
	
	
	public static GapPenalty getLinearModel(int gapOpeningPenalty, int gapExtensionPenalty) {
		return x -> gapExtensionPenalty * x + gapOpeningPenalty;
	}
	
}


