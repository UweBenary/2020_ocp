package algorithm;

import scoring.Score;

@FunctionalInterface
public interface SimilarityScoreFunction {

	public Score score(char querryLetter, char targetLetter) ;

}
