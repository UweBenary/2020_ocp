package algorithm;

import alignment.Alignment;
import alignment.Word;

public abstract class Algorithm {

	private GapPenalty gapPenalty;
	private SimilarityScoreFunction similarityScoreFunction;
	
	public Algorithm(GapPenalty gapPenalty, SimilarityScoreFunction similarityScoreFunction) {
		this.gapPenalty = gapPenalty;
		this.similarityScoreFunction = similarityScoreFunction;
	}

	public Alignment getAlignment(Word query, Word target) {
		constructScoringMatrix(query, target);
		return backTrace();
	}
	
	// getter
	protected GapPenalty getGapPenalty() {
		return gapPenalty;
	}
	
	protected SimilarityScoreFunction getSimilarityScoreFunction() {
		return similarityScoreFunction;
	}

	// abstract
	protected abstract void constructScoringMatrix(Word query, Word target);
	protected abstract Alignment backTrace();

}
