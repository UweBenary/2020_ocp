package algorithm;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import alignment.Alignment;
import alignment.Word;
import scoring.ScoringMatrix;
import scoring.ScoringMatrixElement;

public class BackTracer {
	
	private ScoringMatrix scoringMatrix;


	public BackTracer(ScoringMatrix scoringMatrix) {
		if ( scoringMatrix.isEmpty() ) {
			throw new IllegalStateException("No scoring matrix available to backtrace.");
		}
		this.scoringMatrix = scoringMatrix;
	}

	public Alignment trace() {								
		LinkedList<ScoringMatrixElement> trace = new LinkedList<>();
		
		ScoringMatrixElement maxScoreElement = scoringMatrix.getMaxScoreElement();
		trace.add(maxScoreElement);		
		
		ScoringMatrixElement nextElement = fillTraceByPredecessors(trace, maxScoreElement);
		fillTraceToHead(trace, nextElement);
		fillTraceToTail(trace, maxScoreElement); 
		
		return traceToAlignment(trace);		
	}
	
	private void fillTraceToHead(LinkedList<ScoringMatrixElement> trace, ScoringMatrixElement startElement) {
		for (int queryIdx = startElement.getQueryIndex()-1, targetIdx = startElement.getTargetIndex(); !(queryIdx < 0); queryIdx--) {					
			trace.add(0, scoringMatrix.getElement(queryIdx, targetIdx));
		}		
		for (int queryIdx = 0, targetIdx = startElement.getTargetIndex()-1; !(targetIdx < 0); targetIdx--) {					
			trace.add(0, scoringMatrix.getElement(queryIdx, targetIdx));
		}
	}
	
	private void fillTraceToTail(LinkedList<ScoringMatrixElement> trace, ScoringMatrixElement startElement) {
		for (int queryIdx = startElement.getQueryIndex()+1, targetIdx = startElement.getTargetIndex(); !(queryIdx > scoringMatrix.queryDim()); queryIdx++) {					
			trace.add(scoringMatrix.getElement(queryIdx, targetIdx));
		}		
		for (int queryIdx = 0, targetIdx = startElement.getTargetIndex()+1; !(targetIdx > scoringMatrix.targetDim()); targetIdx++) {					
			trace.add(scoringMatrix.getElement(queryIdx, targetIdx));
		}

	}
	
	private ScoringMatrixElement fillTraceByPredecessors(LinkedList<ScoringMatrixElement> trace, ScoringMatrixElement startElement) {
		ScoringMatrixElement previousElement = startElement;
		
		while ( previousElement.hasPredecessor() ) {						
			ScoringMatrixElement nextElement = previousElement.getPredecessor();			
			trace.add(0, nextElement);
			previousElement = nextElement;		
		} ;
		
		return previousElement;
	}
	
	private Alignment traceToAlignment(LinkedList<ScoringMatrixElement> trace) {
		Word queryWord = getQueryFrom(trace);
		Word targetWord = getTragetFrom(trace);
		return new Alignment(queryWord , targetWord);		
	}

	private Word getQueryFrom(LinkedList<ScoringMatrixElement> trace) {
		List<Integer> queryIdxList = trace.stream()
				.map(ScoringMatrixElement::getQueryIndex)
				.collect(Collectors.toList()); 
		return getAlignmentWord(queryIdxList, scoringMatrix.getQuery());
	}
	
	private Word getTragetFrom(LinkedList<ScoringMatrixElement> trace) {
		List<Integer> targetIdxList = trace.stream()
				.map(ScoringMatrixElement::getTargetIndex)
				.collect(Collectors.toList());
		return getAlignmentWord(targetIdxList, scoringMatrix.getTarget());
	}
	
	private Word getAlignmentWord(List<Integer> indexList, Word word) {
		StringBuilder builder = new StringBuilder();
		
		for (int previous = 0, next = 1; next < indexList.size(); previous = next, next++) {	
			Integer index = indexList.get(next);
			
			if ( index == indexList.get(previous) ) {
				builder.append('\u0000');
			} else {
				builder.append( word.charAt(index) );
			}
		}
		
		return new Word( builder.toString() );
	}


}
