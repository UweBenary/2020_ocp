package ub030_StreamReduceSimple;

import java.util.Arrays;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

public class Tasks {

	public static void main(String[] args) {
		
		a1();
		a1Alternative();
		a2();
	}

	private static void a1Alternative() {
		String[] items = { "aa", "bbb", "cccc", "ddddd" };
		
		
		Integer identity = 0;
		
		BinaryOperator<Integer> accumulator = Integer::sum;
		
		Integer totalLength = Arrays.stream(items)
									.map(String::length)
									.reduce(identity , accumulator);
		System.out.println(totalLength);
	}

	private static void a2() {
		String[] items = { "aa", "bbb", "cccc", "ddddd" };
		
		
		Integer identity = 0;
		BiFunction<Integer, String, Integer> accumulator = 
				(i, str) -> i + str.length();
		BinaryOperator<Integer> combiner = Integer::sum;
		
		int totalLength = Arrays.stream(items)
			  .reduce(identity, accumulator, combiner);
		System.out.println(totalLength);
	}

	private static void a1() {
		String[] items = { "aa", "bbb", "cccc", "ddddd" };
		
		String identity = "";
		BinaryOperator<String> accumulator = (s1, s2) -> s1 + s2;

		int totalLength = Arrays.stream(items)
			  .reduce(identity, accumulator)
			  .length();
		System.out.println(totalLength);
	}

}
