package ub038_MapsFunctional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Tasks {

	public static void main(String[] args) {
		
		int count = 0;
		System.out.println("*** A" + ++count);
		a1();
		System.out.println("*** A" + ++count);
		a2();
		System.out.println("*** A" + ++count);
		a3();
		System.out.println("*** A" + ++count);
		a4();
		System.out.println("*** A" + ++count);
		System.out.println(" I" );
		a5i();
		System.out.println(" II" );
		a5ii();
		System.out.println(" III" );
		a5iii();
		
	}

	private static void a5iii() {
		String expected = "1 = Mo\n2 = Di\n3 = Mi\n";
		System.out.println(" Expected: \n" + expected);
		
		Map<Integer, String> map = new HashMap<Integer, String>(Map.of(1, "Mo", 2, "Di", 3, "Mi"));
		map.forEach( (i,v) -> System.out.println( i + " = " + v ) );	
	}

	private static void a5ii() {
		Map<Integer, String> map = new HashMap<Integer, String>(Map.of(1, "Mo", 2, "Di", 3, "Mi"));
		map.merge(4, "Do", (existingValue, additionalValue) -> { 
			System.out.println("existing: " + existingValue);
			System.out.println("additiona: " + additionalValue);
			return existingValue + " (" + additionalValue + ")";
		} );
		String expected = "{1=Mo, 2=Di, 3=Mi, 4=Do}";
		System.out.println(" Expected: " + expected);
		System.out.println(" Test? -> " + expected.equals(map.toString()));		
	}

	private static void a5i() {

		Map<Integer, String> map = new HashMap<Integer, String>(Map.of(1, "Mo", 2, "Di", 3, "Mi"));
		map.merge(2, "Dienstag", (existingValue, additionalValue) -> { 
			System.out.println("existing: " + existingValue);
			System.out.println("additiona: " + additionalValue);
			return existingValue + " (" + additionalValue + ")";
		} );
		
		
		String expected = "{1=Mo, 2=Di (Dienstag), 3=Mi}";
		System.out.println(" Expected: " + expected);
		System.out.println(" Test? -> " + expected.equals(map.toString()));				
	}

	private static void a4() {
		Map<Integer, String> map = new HashMap<Integer, String>(Map.of(1, "Mo", 2, "Di", 3, "Mi"));
		map.computeIfPresent(2, (k,v) -> "Dienstag");
		
		String expected = "{1=Mo, 2=Dienstag, 3=Mi}";
		System.out.println(" Expected: " + expected);
		System.out.println(" Actual: " + map);		
		System.out.println(" Test? -> " + expected.equals(map.toString()));			
	}

	private static void a3() {
		Map<Integer, String> map = new HashMap<Integer, String>(Map.of(1, "Mo", 2, "Di", 3, "Mi"));
		map.computeIfAbsent(2, k -> "Dienstag");
		
		String expected = "{1=Mo, 2=Di, 3=Mi}";
		System.out.println(" Expected: " + expected);	
		System.out.println(" Actual: " + map);		
		System.out.println(" Test? -> " + expected.equals(map.toString()));		
	}

	private static void a2() {
		Map<Integer, String> map = new HashMap<Integer, String>(Map.of(1, "Mo", 2, "Di", 3, "Mi"));
		map.compute(2, (x,y)->"Dienstag");
		System.out.println(map);
	}

	private static void a1() {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("one", 1);
		map.put("two", 2);
		map.put("three", 3);
		
		map.forEach( (key, value) -> System.out.println(key + " = " + value) );
	}

}
