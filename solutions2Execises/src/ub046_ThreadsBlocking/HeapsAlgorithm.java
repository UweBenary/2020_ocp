package ub046_ThreadsBlocking;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;


public class HeapsAlgorithm {
	
	private ArrayBlockingQueue<ArrayList<Character>> bQueue = new ArrayBlockingQueue<>(1);

	/*
	 * https://en.wikipedia.org/wiki/Heap%27s_algorithm
	 */
	public void permutate(char[] arr, int pointer) throws InterruptedException {
		
	    if(pointer==1) {
//	        System.out.printf("%s %n", Arrays.toString(arr));
	    	output(arr);
	        return;
	    }
	    
		for (int i = 0; i < pointer-1; i++) {
		   permutate(arr, pointer-1);
		    
			if(pointer%2==0) {
			    char tmp = arr[pointer-1];
			    arr[pointer-1] = arr[i];
			    arr[i] = tmp;
			} else {
			    char tmp = arr[pointer-1];
			    arr[pointer-1] = arr[0];
			    arr[0] = tmp;
			}
			
		}
		
		permutate(arr, pointer-1);
	}

	private void output(char[] arr) throws InterruptedException {      
		ArrayList<Character> list = new ArrayList<Character>();
		for (char c : arr) {
			list.add(c);
		}
		bQueue.put(list);
//		System.out.printf("%s %n", Arrays.toString(arr));		
	}	
	
	public ArrayList<Character> getPermutation() throws InterruptedException {
		return bQueue.take();
	}
}
