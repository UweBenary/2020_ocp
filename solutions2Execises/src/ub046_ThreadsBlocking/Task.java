package ub046_ThreadsBlocking;


public class Task {

	public static void main(String[] args) {

		char[] arr = { 'a', 'b', 'c', 'd', 'e', 'f'};
		
		HeapsAlgorithm heaps = new HeapsAlgorithm();

		Thread a1 = new Thread( ()-> {
			try {
				heaps.permutate(arr, arr.length);
				System.out.println("Thread a1 done.");
				
			} catch (InterruptedException e) {
				System.out.println("Thread a1 terminated");
			}
		} );
		
		a1.start();
		
		Runnable displayer = () -> {
			while (true) {
				try {						
					System.out.println( heaps.getPermutation() );
				} catch (InterruptedException e) {
					System.out.println("Thread a2 terminated");
					break;
				}
			}

		};
		
		Thread a2 = new Thread( displayer );
		a2.start();
		
		Thread a3 = new Thread( ()-> {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Thread a3 terminates all:");
			a1.interrupt();
			a2.interrupt();
		});
		
		a3.start();
	}

}
