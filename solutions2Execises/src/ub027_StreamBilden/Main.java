package ub027_StreamBilden;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Main {

	public static void main(String[] args) {

		/*
		 * A1
		 */
		List<Integer> list1 = Arrays.asList( 1, 2, 3 );
		List<Integer> list2 = Arrays.asList( 55, 77 );
		
		List<List<Integer>> list3 = Arrays.asList(list1, list2);
		for(List<Integer> e : list3) {
		    System.out.println("size = " + e.size() + ". elements = " + e);
		}
		
		Stream.of(list1, list2)
				.forEach( e->System.out.println("size = " + e.size() + ". elements = " + e) );
		
		/*
		 * A2
		 */
		Test.main(null);
		Stream.generate(Test::nextInt)
			.limit(100)
			.forEach( System.out::println );
		
		/*
		 * A3
		 */
		for (int i = 100; i >= 1; i--) {
		    System.out.println( i );
		}
		
		Stream.iterate(100, i->i-1)
			.limit(100)
			.forEach(System.out::println);
		
		/*
		 * A4
		 */
        String[] a1 = { "a", "b" };
        String[] a2 = { "c", "d" };

        String[][] a3 = { a1, a2 };
        for (String[] arr : a3) {
            for (String s : arr) {
                System.out.println(s);
            }
        }
        
       Stream.concat(Stream.of(a1), Stream.of(a2) )
       	.forEach(System.out::println);
       
       /*
        * A5
        */
       Collection<Path> coll = new ArrayList<>();
		coll.add(Paths.get("/a"));
		coll.add(Paths.get("/a/b"));
		coll.add(Paths.get("/a/b/c"));
		coll.add(Paths.get("/a/b/c/d"));
		for(Path p : coll) {
			System.out.println(p);
		}
		coll.stream()
			.forEach(System.out::println);
		 
	}

}
