package ub017_MapTextStatistics;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;


public class TextStatistics extends ub014_ListTextStatistics.TextStatistics {

	private TextStatistics(String text) { 
		super(text);
	};
	
	public Map<Character, Integer> getCharCounts() {
		Map<Character, Integer> resultMap = new TreeMap<>(); 
		
		for (int i = 0; i < getText().length(); i++) {
			
			Character key = getText().charAt(i);
			
			Integer count = 0; 	
			if ( resultMap.containsKey(key) ) {
				count = resultMap.get(key);
			}
			
			resultMap.put(key, ++count);
		}
		
		return resultMap;		
	}	
	
	public static TextStatistics of(String string) {
		return new TextStatistics(string);
	}
	
	public static void main(String[] args) {
		
		String string = "Heute ist Montag!";
		TextStatistics stats = TextStatistics.of(string);
		
		
		Collection<Character> uniqueChars = stats.getUniqueChars();
		System.out.println("Unique characters in '" + string + "' are " + uniqueChars);
		
		Map<Character, Integer> uniqueCharCounts = stats.getCharCounts();
		System.out.println("The frequencies of the unique characters are " + uniqueCharCounts);
	}
}
