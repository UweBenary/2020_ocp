package ub017_MapTextStatistics.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ub017_MapTextStatistics.TextStatistics;

class TextStatisticsTest {

	@Test
	final void testGetCharCounts() {
		final TextStatistics stats = (TextStatistics) TextStatistics.of("aab");
		final String expected = "{a=2, b=1}"; 
		final String actual = stats.getCharCounts().toString();
		assertEquals(expected, actual);
	}

}
