package ub015_QueueTask;

import java.util.PriorityQueue;
import java.util.Queue;

public class TaskQueue {

	public static void main(String[] args) {

		Task task1 = new Task("Autowäsche", Priority.LOW);
		Task task2 = new Task("Einkaufen", Priority.NORMAL);
		Task task3 = new Task("Rechnung bezahlen", Priority.HIGH);
		
		Queue<Task> taskQueue = new PriorityQueue<>();
		taskQueue.offer(task1);
		taskQueue.offer(task2);
		taskQueue.offer(task3);
		
		while ( ! taskQueue.isEmpty() ) {
			System.out.println( taskQueue.poll() );			
		}
	}

}
