package ub015_QueueTask;

import java.time.LocalDateTime;

public class Task 
	implements Comparable<Task> {

	private String taskText;
	private Priority priority;
	private LocalDateTime deadline;
	
	public Task(String taskText, Priority priority) {
		this(taskText, priority, LocalDateTime.MAX);
	}
		
	public Task(String taskText, Priority priority, LocalDateTime deadline) {
		this.taskText = taskText;
		this.priority = priority;
		this.deadline = deadline;
	}

	@Override
	public String toString() {
		return "'" + taskText + "', Priorität: " + priority;
	}

	@Override
	public int compareTo(Task otherTask) {
		int result = deadline.compareTo(otherTask.deadline) ; 
		if( result == 0) {
			result = otherTask.priority.ordinal() - priority.ordinal();
		}
		if( result == 0) {
			result = taskText.compareTo(otherTask.taskText);
		}
		return result ;
	}
}
