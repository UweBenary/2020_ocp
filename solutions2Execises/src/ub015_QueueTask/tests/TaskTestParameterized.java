package ub015_QueueTask.tests;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import ub015_QueueTask.Priority;
import ub015_QueueTask.Task;

@RunWith(Parameterized.class)
class TaskTestParameterized {
	

	// creates the test data
    @Parameterized.Parameters(name = "{index}: Test with m1={0}, result is:{1}. ")
    public static Collection<Object[]> data() {
    	
    	final LocalDateTime deadline = LocalDateTime.now();
    	
    	Task t2 = new Task( "Autowäsche", Priority.HIGH );
    	Task t3 = new Task( "Autowäsche", Priority.HIGH, deadline );
    	Task t4 = new Task( "Autowäsche", Priority.LOW,  deadline );
    	
    	List<Object[]> data = new ArrayList<>();
    	data.add( new Object[] {2, 1} );
    	data.add( new Object[] {3, 2} );
    	data.add( new Object[] {4, 3} );
    	
    	System.out.println(data);
    	return data;
    }

    // fields used together with @Parameter must be public
    @Parameterized.Parameter(0)
    public int task;
    @Parameterized.Parameter(1)
    public int result;
    

    public TaskTestParameterized(int task, int result) {	
		this.task = task;
		this.result = result;
	}
    
    
	@Test
	public final void testCompareTo() {
		System.out.println(task + " " + result);
		final Task t = new Task( "Autowäsche", Priority.LOW );
        assertEquals("Result", result, task < 4);
//      assertEquals("Result", result, t.compareTo(task));
	}

}
