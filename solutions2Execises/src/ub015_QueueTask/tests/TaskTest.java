package ub015_QueueTask.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.provider.CsvSource;

import ub015_QueueTask.Priority;
import ub015_QueueTask.Task;

class TaskTest {

	private final Task t = new Task( "Autowäsche", Priority.LOW );
		
	@Test
	final void testToString() {
		final String expected = "'Autowäsche', Priorität: LOW";
		String actual = t.toString();
		assertEquals(expected, actual);
	}

	
	
	DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
	
	final String argument1 = "Autowäsche, HIGH, , true";
	final String argument2 = "Autowäsche, LOW, 2020-01-10T16:03:41.1452616, true";
	
	@ParameterizedTest
	@CsvSource({ argument1, argument2 })
	final void testParameterizedCompareTo(ArgumentsAccessor argumentsAccessor) {
		
		String arg1 = argumentsAccessor.getString(0);
		Priority arg2 = argumentsAccessor.get(1, Priority.class);
		
		Task task;
		try {
			if ( argumentsAccessor.getString(2) == null ) {
				task = new Task(arg1, arg2 );
			} else {
				LocalDateTime arg3 = LocalDateTime.parse( argumentsAccessor.getString(2) , formatter );
				task = new Task(arg1, arg2, arg3 );
			}
		} catch (Exception e) {			
			System.err.println("argumentsAccessor.getString(2): " + argumentsAccessor.getString(2) + "Exception caught: " + e.getMessage() );
			throw new RuntimeException();
		}
				
		boolean actual = t.compareTo(task) > 0;
		assertEquals(Boolean.parseBoolean(argumentsAccessor.getString(3)), actual);
	}
	
}
