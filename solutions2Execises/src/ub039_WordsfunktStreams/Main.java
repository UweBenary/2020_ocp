package ub039_WordsfunktStreams;

import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

import words.ResourceLoader;

public class Main {

	public static void main(String[] args) {

		List<String> words = ResourceLoader.loadPasswords();
		
		System.out.println("A1: " + 
				words.stream()
					.filter(x->x.length()>5)
					.count() );
		
		System.out.println("A2: " + 
				words.stream()
					.filter(x->x.contains("m"))
					.sorted()
					.findFirst()
					.get());
		
		System.out.println("A3: " + 
				words.stream()
					.skip(20)
					.limit(10)
					.collect(Collectors.toList())
				);
		
		System.out.println("A4: " + 
				words.stream()
					.filter(x->x.contains("ooo"))
					.findAny()
					.get()
				);
		
		System.out.println("A5: " + 
				words.stream()
					.filter(x->x.contains("aba"))
					.collect(Collectors.toCollection(LinkedList::new))
				);
		
		System.out.println("A6: " + 
				words.stream()
					.filter(x->x.contains("password"))
					.findAny()
					.get()
				);
		
		System.out.println("A7: " + 
				words.stream()
					.collect(Collectors.groupingBy(s->s.length()))
				);
		
		System.out.println("A8: " + 
				words.stream()
					.collect(Collectors.groupingBy(s->s.length(), TreeMap::new, Collectors.counting()))
				);
	}

}
