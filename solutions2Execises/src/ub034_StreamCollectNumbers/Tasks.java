package ub034_StreamCollectNumbers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Tasks {

	public static void main(String[] args) {
		
		
		String[] arr = {
				"1,2,3,4,5",
				"7,6,5,4,3",
				"123,456",
		};
		
		// A
		List<Integer> list = originalCode(arr);
		// B
		
		System.out.println(list); // [1, 2, 3, 4, 5, 7, 6, 5, 4, 3, 123, 456]

		/*
		 * A1
		 */
		list = originalCodeReplacedWithStream(arr);
		System.out.println("A1: " + list);
		
		/*
		 * A2
		 */
		list = selectEven(arr);
		System.out.println("A2: " + list);
		
	}

	public static Stream<Integer> getStreamOfIntegers(String[] arr) {
		return Stream.of(arr)
//					.flatMap(s -> Stream.of( s.split(",") ))
					.map(s -> s.split(","))
					.flatMap(Arrays::stream)
					.map(Integer::parseInt);
	}
	
	public static List<Integer> originalCodeReplacedWithStream(String[] arr) {
		return getStreamOfIntegers(arr).collect( Collectors.toList());
	}

	public static List<Integer> selectEven(String[] arr) { 		
		return getStreamOfIntegers(arr)
				.filter( x -> x%2 == 0)
				.collect( Collectors.toList() );
	}
	
	public static List<Integer> originalCode(String[] arr) {
		
		List<Integer> list = new ArrayList<>();
		for (String s : arr) {
			String[] stringNumbers = s.split(",");
			
			for (String sNum : stringNumbers) {
				Integer num = Integer.valueOf(sNum);
				list.add(num);
			}
		}
		return list;
	}
}
