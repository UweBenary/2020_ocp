package ub034_StreamCollectNumbers.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;

import ub034_StreamCollectNumbers.Tasks;

class TasksTest {

	@Test
	final void testOriginalCode() {

		final String[] input = {
				"1,2,3,4,5",
				"7,6,5,4,3",
				"123,456",
			};
		final String expectedOutput = "[1, 2, 3, 4, 5, 7, 6, 5, 4, 3, 123, 456]";
		
		List<Integer> list = Tasks.originalCode(input);
		final String actualOutput = list.toString();

		assertEquals(expectedOutput, actualOutput);
	}

	@Test
	final void testOriginalCodeReplacedWithStream() {

		final String[] input = {
				"1,2,3,4,5",
				"7,6,5,4,3",
				"123,456",
			};
		final String expectedOutput = "[1, 2, 3, 4, 5, 7, 6, 5, 4, 3, 123, 456]";
		
		List<Integer> list = Tasks.originalCodeReplacedWithStream(input);
		final String actualOutput = list.toString();

		assertEquals(expectedOutput, actualOutput);
	}
	
	@Test
	final void testSelectEven() {

		final String[] input = {
				"1,2,3,4,5",
				"7,6,5,4,3",
				"123,456",
			};
		final String expectedOutput = "[2, 4, 6, 4, 456]";
		
		List<Integer> list = Tasks.selectEven(input);
		final String actualOutput = list.toString();

		assertEquals(expectedOutput, actualOutput);
	}
	
}
