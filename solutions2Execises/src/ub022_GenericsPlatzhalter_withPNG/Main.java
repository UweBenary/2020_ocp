package ub022_GenericsPlatzhalter_withPNG;

import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		List list = null;
		List<?> listUnknown = null;//? = ? extends Object
		List<Object> listObject = null;
		List<? super Object> listSuperObject = null;
		List<? extends Object> listExtendsObject = null;
		List<Inter> listInter = null;
		List<? super Inter> listSuperInter = null;
		List<? extends Inter> listExtendsInter = null;
		List<Base> listBase = null;
		List<? super Base> listSuperBase = null;
		List<? extends Base> listExtendsBase = null;
		List<Derived> listDerived = null;
		List<? super Derived> listSuperDerived = null;
		List<? extends Derived> listExtendsDerived = null;
		List<A> listA = null;
		List<? super A> listSuperA = null;
		List<? extends A> listExtendsA = null;
		List<C> listC = null;
		List<? super C> listSuperC = null;
		List<? extends C> listExtendsC = null;
		List<AD> listAD = null;
		List<? super AD> listSuperAD = null;
		List<? extends AD> listExtendsAD = null;
		List<ADD> listADD = null;
		List<? super ADD> listSuperADD = null;
		List<? extends ADD> listExtendsADD = null;

// Welche der folgenden Zuweisungen werden kompilieren?

//		1.  kompiliert	
		listSuperA  = listSuperInter;
//		2.  kompiliert	nicht
		listDerived = listSuperC;
//		3.  kompiliert	
		listExtendsADD  = listADD;
//		4.  kompiliert	
		listExtendsBase = listExtendsADD;
//		5.  kompiliert	nicht
		listAD  = listBase;
//		6.  kompiliert	nicht
		listADD = listSuperA;
//		7.  kompiliert	
		listExtendsBase = listExtendsBase;
//		8.  kompiliert	nicht
		listBase    = listExtendsADD;
//		9.  kompiliert	nicht
		listObject  = listExtendsDerived;
//		10. kompiliert	nicht
		listSuperA  = listExtendsInter;
//		11. kompiliert	nicht
		listSuperObject = listAD;
//		12. kompiliert	nicht
		listSuperC = listExtendsAD;
//		13. kompiliert	
		listExtendsAD = listExtendsADD;
//		14. kompiliert	nicht
		listDerived = listADD;
//		15. kompiliert	nicht
		listADD = listA;
//		16. kompiliert	nicht
		listBase = listA;
//		17. kompiliert	
		listSuperBase = listInter;
//		18. kompiliert	nicht
		listSuperBase = listUnknown;
//		19. kompiliert	
		listExtendsObject = listA;
//		20. kompiliert	nicht
		listExtendsC = listSuperC;
//		21. kompiliert	
		listUnknown = listExtendsAD;
//		22. kompiliert	nicht
		listSuperDerived = listA;
//		23. kompiliert	
		listSuperDerived = listInter;
//		24. kompiliert	nicht
		listDerived = listAD;
//		25. kompiliert	nicht
		listExtendsInter = listSuperBase;
//		26. kompiliert	
		listExtendsBase = list;
//		27. kompiliert	
		listExtendsObject = listExtendsC;
//		28. kompiliert	nicht
		listAD = listObject;
//		29. kompiliert	nicht
		listExtendsC = listUnknown;
//		30. kompiliert	nicht
		listSuperC = listA;
//		31. kompiliert	
		listExtendsInter = listExtendsBase;
//		32. kompiliert	
		listExtendsObject = listSuperDerived;
//		33. kompiliert	
		listExtendsObject = listExtendsADD;
//		34. kompiliert	nicht
		listSuperDerived = listExtendsC;
//		35. kompiliert	nicht
		listAD = listSuperObject;
//		36. kompiliert	
		listSuperAD = listSuperDerived;
//		37. kompiliert	nicht
		listExtendsAD = listA;
//		38. kompiliert	
		listUnknown = listSuperC;
//		39. kompiliert	nicht
		listExtendsADD = listC;
//		40. kompiliert	nicht
		listAD = listSuperAD;
//		41. kompiliert	nicht
		listSuperBase = listExtendsAD;
//		42. kompiliert	nicht
		listC = listSuperA;
//		43. kompiliert	nicht
		listA = listInter;
//		44. kompiliert	
		list = listSuperAD;
//		45. kompiliert	nicht
		listObject = listExtendsInter;
//		46. kompiliert	
		listSuperA = listSuperA;
//		47. kompiliert	nicht
		listExtendsAD = listExtendsC;
//		48. kompiliert	
		list = listADD;
//		49. kompiliert	nicht
		listA = listDerived;
//		50. kompiliert	
		listUnknown = listSuperA;

	}

}
