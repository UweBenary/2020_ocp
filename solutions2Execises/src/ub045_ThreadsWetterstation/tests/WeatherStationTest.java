package ub045_ThreadsWetterstation.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import ub045_ThreadsWetterstation.WeatherStation;

class WeatherStationTest {

	@Test
	void testGetMeasurements() {

		WeatherStation station = new WeatherStation();
		
		final int expected = 10;		
		for (int i = 1; i < expected; i++) {
			station.measureTemperature();
		}
		
		int actual = station.getMeasurements().size();
		
		assertEquals(expected, actual);
		
	}

}
