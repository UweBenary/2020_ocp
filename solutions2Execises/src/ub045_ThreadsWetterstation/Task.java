package ub045_ThreadsWetterstation;

public class Task {

	public static void main(String[] args) {
		
		WeatherStation station = new WeatherStation();

		Thread monitorer = startMonitorer(station);
		
		Thread analyser = startAnalizer(station);
		
		Thread inspector = startInspector(station);

		
		Thread terminator = new Thread() {
			@Override
			public void run() {
				try {
					Thread.sleep(20_000);
					
					monitorer.interrupt();
					analyser.interrupt();
					inspector.interrupt();
					
				} catch (InterruptedException e) {
					e.getStackTrace();
				}
			}
		};
		terminator.start();


	}

	private static Thread startInspector(WeatherStation station) {
		Runnable inspectTemperature = () -> {
			while (true) {
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					break;
				}
				station.check4Warnings();
			}			
		};
		
		Thread inpector = new Thread(inspectTemperature);		
		inpector.start();
		
		return inpector;
	}

	private static Thread startMonitorer(WeatherStation station) {
		Runnable monitorTemperature = () -> {
			while (true) {
				station.measureTemperature();
				
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					break;
				}
			}			
		};
		
		Thread monitorer = new Thread(monitorTemperature);		
		monitorer.start();
		
		return monitorer;
	}

	private static Thread startAnalizer(WeatherStation station) {
		Runnable analyseTemperature = () -> {
			while (true) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					break;
				}
				station.displayTemperatureTrend();
			}			
		};
		
		Thread analyser = new Thread(analyseTemperature);		
		analyser.start();
		
		return analyser;
	}

}
