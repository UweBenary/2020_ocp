package ub045_ThreadsWetterstation;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

	
public class WeatherStation {
	
	private static final int temperatureVariance = 5;
	private List<Double> measurements = new ArrayList<Double>();
	private static final Double upperLimit = 35.;
	private static final Double lowerLimit = -35.;
	
	public WeatherStation() {
		measurements.add(0.);
	}
	
	public List<Double> getMeasurements() {
		return new ArrayList<Double>(measurements);
	}
	
//	public List<Double> getLast3Measurements() {
//		return new ArrayList<Double>(measurements.subList(measurements.size()-3, measurements.size()));
////		return measurements.stream()
////							.skip( measurements.size()-3 )
////							.collect(Collectors.toList());
//	}
	
	public void measureTemperature() {
		Double newTemperature = getNewMeasurement();
		measurements.add( newTemperature );
	}
	
	private double getNewMeasurement() {
		Double oldTemperature = getLatestTemperature();
		Double temperatureIncrement = getRandomIncrement();
		return oldTemperature + temperatureIncrement;
	}

	private double getRandomIncrement() {
		return new Random().nextDouble() * 2 * temperatureVariance - temperatureVariance;
	}

	public void displayTemperatureTrend() {
		double lastTemperature = getLatestTemperature();
		double lastBut1Temperature = measurements.get(measurements.size()-2);
		double lastBut2Temperature = measurements.get(measurements.size()-3);
		
		if (lastBut2Temperature < lastBut1Temperature && lastBut1Temperature < lastTemperature) 
			System.out.println("^");
		
		if (lastBut2Temperature > lastBut1Temperature && lastBut1Temperature > lastTemperature) 
			System.out.println("v");		
	}

	public void check4Warnings() {
		if (getLatestTemperature() > upperLimit) 
			System.out.println("Warning: it's too hot! " + getLatestTemperature());
		if (getLatestTemperature() < lowerLimit) 
			System.out.println("Warning: it's too cold! " + getLatestTemperature());
	}

	private Double getLatestTemperature() {
		return measurements.get(measurements.size()-1);
	}
	
	
}
