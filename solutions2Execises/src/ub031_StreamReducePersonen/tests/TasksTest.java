package ub031_StreamReducePersonen.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import ub031_StreamReducePersonen.Person;
import ub031_StreamReducePersonen.Tasks;

class TasksTest {
	
	private final List<Person> inputPersonList = Arrays.asList( new Person[] {
			new Person("Tom", "Katze"),
			new Person("Jerry", "Maus"),
			new Person("Alexander", "Poe"),
		}) ;
	
	private final String expectedPersonString = "Tom Poe";

	@Test
	void testReduce3() {
		String actual = Tasks.reduce3(inputPersonList).toString();
		
		assertEquals(expectedPersonString, actual);
	}

	@Test
	void testReduce2() {
		String actual = Tasks.reduce2(inputPersonList).toString();
		
		assertEquals(expectedPersonString, actual);
	}

	@Test
	void testReduce1() {		
		String actual = Tasks.reduce1(inputPersonList).toString();
	
		assertEquals(expectedPersonString, actual);
	}

}
