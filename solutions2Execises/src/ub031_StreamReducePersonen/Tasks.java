package ub031_StreamReducePersonen;


import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

public class Tasks {

	public static void main(String[] args) {
		
		List<Person> persons = Arrays.asList( new Person[] {
				new Person("Tom", "Katze"),
				new Person("Jerry", "Maus"),
				new Person("Alexander", "Poe"),
			}) ;
	
		Person greatest = reduce1(persons);
		System.out.println(greatest);
		
		greatest = reduce2(persons);
		System.out.println(greatest);
		
		greatest = reduce3(persons);
		System.out.println(greatest);
	}
	
	private static String getMaxString(String s1, String s2) {
		return s1.compareTo(s2) > 0 ? s1 : s2;
	}
	
	private static String getMaxFirstName(Person p1, Person p2) {
		return getMaxString(p1.getFirstName(), p2.getFirstName());
	}
	
	private static String getMaxLastName(Person p1, Person p2) {
		return getMaxString(p1.getLastName(), p2.getLastName());
	}
		
	private static Person combinePersons(Person p1, Person p2) {
		String firstName = getMaxFirstName(p1, p2);
		String lastName = getMaxLastName(p1, p2);
		return new Person(firstName, lastName);
	}
	
	
	
	
	public static Person reduce3(List<Person> persons) {
//		String identity = "";
//		BinaryOperator<String> combiner = (s1, s2) -> s1.compareTo(s2) > 0 ? s1 : s2;
//		
//		BiFunction<String, Person, String> accumulator = (s1,p2) -> s1.compareTo(p2.getFirstName()) > 0 ? s1 : p2.getFirstName();
//		String firstName = persons.stream()
//			   .reduce(identity, accumulator, combiner);
//		
//		BiFunction<String, Person, String> accumulator2 = (s1,p2) -> s1.compareTo(p2.getLastName()) > 0 ? s1 : p2.getLastName();
//		String lastName = persons.stream()
//			   .reduce(identity, accumulator2, combiner);
//		
//		return new Person(firstName, lastName);
		
		Person identity = persons.get(0);
		BiFunction<Person, Person, Person> accumulator = Tasks::combinePersons;
		BinaryOperator<Person> combiner = Tasks::combinePersons;
		return persons.stream()
			   	  .reduce(identity, accumulator, combiner);
	}

	public static Person reduce2(List<Person> persons) {
//		Person identity = persons.get(0);
//		BinaryOperator<Person> accumulator = (p1,p2) -> {
//			Person p = p1;
//			if (p.getFirstName().compareTo(p2.getFirstName()) < 0) {
//				p = new Person(p2.getFirstName(), p.getLastName());
//			}
//			if (p.getLastName().compareTo(p2.getLastName()) < 0) {
//				p = new Person(p.getFirstName(), p2.getLastName());
//			}
//			return p;
//		};
//		return persons.stream()
//			   	  .reduce(identity, accumulator);
		
		Person identity = persons.get(0);
		BinaryOperator<Person> accumulator = Tasks::combinePersons;
		return persons.stream()
			   	  .reduce(identity, accumulator);
	}

	public static Person reduce1(List<Person> persons) {
		
//		BinaryOperator<Person> accumulator = (p1, p2) -> {
//			Person[] p = {p1, p2};
//			
//			Arrays.sort(p, Comparator.comparing( Person::getFirstName ));	
//			String firstName = p[1].getFirstName();
//
//			Arrays.sort(p, Comparator.comparing( Person::getLastName ));	
//			String lastName = p[1].getLastName();
//			
//			return new Person(firstName, lastName);
//		};
//
//		return persons.stream()
//				   	  .reduce(accumulator).get();
		
		BinaryOperator<Person> accumulator = Tasks::combinePersons;
		return persons.stream()
			   	  .reduce(accumulator).get();
		
	}

}
