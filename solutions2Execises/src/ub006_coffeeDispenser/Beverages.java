package ub006_coffeeDispenser;

public enum Beverages {
	COFFEE (1 ),
	CAPPUCCINO (1.20 ),
	ESPRESSO (1.13 );
	
	private double price;
	
	private Beverages(double price) {
		this.price = price;
	}
	
	public double getPrice() {
		return price;
	}
	
	@Override
	public String toString() {
		return name().toLowerCase();
	}
}
