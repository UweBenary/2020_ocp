package ub006_coffeeDispenser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class CoffeeDispenser {

	public static Display display = new Display();
	
	public static String serve(Beverages type) {
		String string = "Serving your hot and tasty " + type + "!";
		display.print(string);
		return string;
	}

	public static String priceList() {
		String result = "";
		for (Beverages type : Beverages.values()) {
			result += String.format(" %-10s (%1.2f Euro)%n",
					type.name(),
					type.getPrice() );
		}
		return result;
	}

	public static double getPayment(Beverages type) {
		final double priceToPay = type.getPrice();
		printPrizeToPay(priceToPay);
		
		Scanner scanner = new Scanner( System.in );
		double payment = 0;
		while ( payment < priceToPay ) {
			
			double coin = scanner.nextDouble();
			if ( ! isValidCoin(coin) ) {
			
				printCounterfeitCoinMessage(priceToPay, payment);
				continue;
			}
			
			payment += coin;
			if (payment < priceToPay) {
				printRemainingPrize(priceToPay, payment);
			}			
		}				
//		scanner.close();
		
		return payment;
	}

	private static boolean isValidCoin(double coin) {
		for ( Coins realCoin : Coins.values() ) {
			if ( realCoin.getValue() == coin ) {
				return true;
			}
		}
		return false;
	}
	
	private static void printRemainingPrize(final double priceToPay, double payment) {
		String outputString = String.format("  %1.2f Euro still needed...", 
				(priceToPay - payment) );
		display.print( outputString );
	}

	private static void printPrizeToPay(final double priceToPay) {
		String outputString = String.format("Please insert %1.2f Euro !", 
				priceToPay);
		display.print( outputString );
	}

	private static void printCounterfeitCoinMessage(final double priceToPay, double payment) {
		System.out.println(" !!! Counterfeit Coin detected !!! ");
		printRemainingPrize(priceToPay, payment);
	}

	public static Coins[] returnCash(Beverages type, double payment) {
		
		double returnAmount = payment - type.getPrice();
		List<Coins> returnCash = new ArrayList<>();
		
		Coins[] coinsInReverseOder = Coins.values();
		Arrays.sort(coinsInReverseOder, Collections.reverseOrder() );
		
		for (Coins coin : coinsInReverseOder) {
			while ( returnAmount > 0 && coin.getValue() <= returnAmount ) {
				returnCash.add(coin);
				returnAmount = returnAmount - coin.getValue();
			} ;
		}
		return returnCash.toArray( new Coins[] {} );
	}
}
