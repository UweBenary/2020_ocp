package ub006_coffeeDispenser;

public enum Coins {
	ONE_CENT(" 1 Cent", 0.01),
	TWO_CENT(" 2 Cent", 0.02),
	FIVE_CENT(" 5 Cent", 0.05),
	TEN_CENT("10 Cent", 0.10),
	TWENTY_CENT("20 Cent", 0.20),
	FIFTY_CENT("50 Cent", 0.50),
	ONE_EURO(" 1 Euro", 1),
	TWO_EURO(" 2 Euro", 2);
	
	private String name;
	private double value;
	
	private Coins(String name, double value) {
		this.name = name;
		this.value = value;
	}
	
	@Override
	public String toString() {
		return name;
	}

	public double getValue() {
		return value;
	}
}
