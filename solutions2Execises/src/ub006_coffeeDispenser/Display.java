package ub006_coffeeDispenser;

import java.util.Scanner;

public class Display {
	
	public void print(String s) {
		System.out.println(s);
	}

	public Beverages getChoice() {
		print( getListOfChoices() );
		int input = getUserInput();
		return Beverages.values()[input-1];
	}

	private int getUserInput() {		
		Scanner scanner = new Scanner( System.in );
		int input = -1;
		boolean isValidInput = false;
		do {
			input = scanner.nextInt();
			isValidInput = checkValidity(input);
		} while ( ! isValidInput );

		//		scanner.close();
		return input;
	}

	private boolean checkValidity(int input) {
		if (input < 1 || input > Beverages.values().length ) {
			print("Your choise " + input + " is not an option! Please choose again...");
			return false;
		} 
		return true;
	}

	private String getListOfChoices() {
		String result = "";
		int i = 0;
		for (Beverages type : Beverages.values()) {
			result += String.format(" -%d-  %-1s %n",
					++i,
					type.name() );
		}
		result += "Please make your choice: ";
		return result;		
	}


}
