package ub006_coffeeDispenser;

import java.util.Arrays;

public class _Main {
	
	/*
	 * calculations of differences is not exact. returned cash sometime misses a cent!
	 */

	public static void main(String[] args) {
		
		Display dispenserDisplay = CoffeeDispenser.display;
		
		String menue = CoffeeDispenser.priceList();
		dispenserDisplay.print(menue);
		
		Beverages coffeeType = dispenserDisplay.getChoice();
		
		double payment = CoffeeDispenser.getPayment(coffeeType);
		
		CoffeeDispenser.serve(coffeeType);
		
		if ( payment > coffeeType.getPrice() ) {
			Coins[] cash = CoffeeDispenser.returnCash(coffeeType, payment);
			dispenserDisplay.print("Cash return:");
			dispenserDisplay.print( Arrays.toString(cash) );
		}
		
		
	}

}
