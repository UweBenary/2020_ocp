package ub006_coffeeDispenser.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ub006_coffeeDispenser.Beverages;
import ub006_coffeeDispenser.CoffeeDispenser;
import ub006_coffeeDispenser.Display;

class DisplayTest {

	@Test
	final void testPrint() {
		String s = CoffeeDispenser.priceList();
		CoffeeDispenser.display.print(s);
	}
	
	@Test
	final void testGetChoice() {
		Beverages expected = Beverages.COFFEE;
		System.err.println("***Type 1 for testing***");
		Display d = new Display();
		Beverages actual = d.getChoice();
		assertEquals(expected, actual);
	}


}
