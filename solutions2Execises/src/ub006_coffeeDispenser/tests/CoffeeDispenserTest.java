package ub006_coffeeDispenser.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ub006_coffeeDispenser.Beverages;
import ub006_coffeeDispenser.CoffeeDispenser;
import ub006_coffeeDispenser.Coins;

class CoffeeDispenserTest {

	@Test
	final void testServe() {
		String expected = "Serving your hot and tasty coffee!";
		String actual = CoffeeDispenser.serve(Beverages.COFFEE);
		assertEquals(expected, actual);
	}

	@Test
	final void testDisplayPriceList() {
		String expected = " COFFEE     (1,00 Euro)\r\n" + 
						  " CAPPUCCINO (1,20 Euro)\r\n" + 
						  " ESPRESSO   (1,13 Euro)\r\n";
		String actual = CoffeeDispenser.priceList();
		assertEquals(expected, actual);
	}	
	
	@Test
	final void testGetPayment() {
		Beverages type = Beverages.ESPRESSO;
		double actual = CoffeeDispenser.getPayment(type);
		assertTrue(actual >= type.getPrice() );
	}
	
	@Test
	final void testReturnCash() {
		Coins[] expected = {Coins.ONE_EURO};
		Beverages type = Beverages.COFFEE;
		Coins[] actual = CoffeeDispenser.returnCash(type, 2);
		assertArrayEquals(expected, actual);
	}
	
}
