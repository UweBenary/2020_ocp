package ub006_coffeeDispenser.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import ub006_coffeeDispenser.Beverages;

class BeveragesTest {

	@Test
	final void testConstants() {
		String expected = "[coffee, cappuccino, espresso]";
		String actual = Arrays.toString( Beverages.values() );
		assertEquals(expected, actual);
	}
	
	@Test
	final void testGetPrice() {
		double expected = 1.13;
		double actual = Beverages.ESPRESSO.getPrice();
		assertEquals(expected, actual);
	}
	
	@Test
	final void testToString() {
		String expected = "espresso";
		String actual = Beverages.ESPRESSO.toString();
		assertEquals(expected, actual);
	}

}
