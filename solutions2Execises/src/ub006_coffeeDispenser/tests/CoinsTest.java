package ub006_coffeeDispenser.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import ub006_coffeeDispenser.Coins;


class CoinsTest {

	@Test
	final void testConstants() {
		String expected = "[ 1 Cent,  2 Cent,  5 Cent," 
				+ " 10 Cent, 20 Cent, 50 Cent," 
				+ "  1 Euro,  2 Euro]";
		String actual = Arrays.toString( Coins.values() );
		assertEquals(expected, actual);
	}
	
	@Test
	final void testGetValue() {
		double expected = .5;
		double actual = Coins.FIFTY_CENT.getValue();
		assertEquals(expected, actual);
	}
	
}
