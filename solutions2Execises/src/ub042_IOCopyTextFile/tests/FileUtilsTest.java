package ub042_IOCopyTextFile.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ub042_IOCopyTextFile.CopySourceNotFound;
import ub042_IOCopyTextFile.FileUtils;

class FileUtilsTest {

	private static final Path dir = Paths.get("src", "ub042_IOCopyTextFile", "tests");
	
	private static final String fromFileString = "source.txt";
	private static final Path fromFile = dir.resolve(fromFileString);
	
	private static final String toFileString = "copy.txt";
	private static final Path toFile = dir.resolve(toFileString);
	
	@BeforeEach
	void setUpBeforeClass() throws Exception {
			Files.createDirectories(fromFile);
			System.out.println("Setup works: " + Files.exists(fromFile));
	}

	@AfterEach
	void tearDownAfterClass() throws Exception {
		Files.deleteIfExists(fromFile);
		System.out.println("Clean-up 1 done: " + !Files.exists(fromFile));
		
		Files.deleteIfExists(toFile);
		System.out.println("Clean-up 2 done: " + !Files.exists(toFile));
	}

	/*
	 * A1
	 */
	@Test
	void testCopyTextFile() {		
		try {
			FileUtils.copyTextFile(fromFile.toString(), toFile.toString());
			assertTrue(Files.exists(toFile));
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	/* 
	 * A2
	 */
	@Test
	void testCopyTextFileFailsIfNoSource() {	
		try {
			Files.deleteIfExists(fromFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("A2: fromFile deleted: " + !Files.exists(fromFile));
		
		assertThrows(CopySourceNotFound.class, () -> FileUtils.copyTextFile(fromFile.toString(), toFile.toString()) );
	}
	
	@Test
	void testCopyTextFileFailsIfTargetAlreadyExists() {	
		try {
			FileUtils.copyTextFile(fromFile.toString(), toFile.toString());
			System.out.println("A2: toFile exists already: " + Files.exists(toFile));
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
		
		assertThrows(FileAlreadyExistsException.class, 
				() -> FileUtils.copyTextFile(fromFile.toString(), toFile.toString()) );
	}
	
}
