package ub042_IOCopyTextFile;

import java.io.IOException;
import java.io.UncheckedIOException;

public class CopySourceNotFound extends UncheckedIOException {

	public CopySourceNotFound(IOException e) {
		super(e);
	}
}
