package ub042_IOCopyTextFile;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class FileUtils {

	public static void copyTextFile(String fromFile, String toFile) throws IOException {

		Path source = Paths.get(fromFile);
		Path target = Paths.get(toFile);
		
		try {
			Files.copy(source, target);
			
		} catch (NoSuchFileException e) {
			throw new CopySourceNotFound(e);
		} catch (FileAlreadyExistsException e) {
			throw new FileAlreadyExistsException(target.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void copyTextFile(String fromFile, String toFile, boolean append) throws IOException {
		
		Path source = Paths.get(fromFile);
		Path target = Paths.get(toFile);
		
		try {
			if (append) {
				append2File(fromFile, toFile);
				
			} else {
				Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
			}
			
		} catch (NoSuchFileException e) {
			throw new CopySourceNotFound(e);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void append2File(String fromFile, String toFile) { // new FileWriter(fromFile, true) 
		try(FileReader fromFileReader = new FileReader(fromFile);
			FileReader toFileReader = new FileReader(toFile);
			FileWriter fileWriter = new FileWriter(fromFile) 
			){
			
			int data;
			while ( (data = fromFileReader.read()) != -1) {
				fileWriter.write(data);
			}
			
			fileWriter.write("\n");
			
			while ( (data = toFileReader.read()) != -1) {
				fileWriter.write(data);
			}
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
		
	}
	

}
