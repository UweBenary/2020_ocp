package ub041_WordsIO;


import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import words.ResourceLoader;


public class Task {

	static final Path folder = Paths.get("src/ub041_WordsIO/mydir");
	
	static {
		try {
			if ( ! Files.exists(folder) ) {
				Files.createDirectory(folder);
			} 
	
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws IOException {

		List<String> words = ResourceLoader.loadEnglishWords();

		words.stream()
				.collect(Collectors.groupingBy(s->s.charAt(0)))
				.forEach( (ch, list) -> write2File(ch + ".txt", list) );
		
		deleteAll();
		
	}
	
	private static void deleteAll() throws IOException {
		
		Files.list(folder) // Files.newDirectoryStream(folder)
			.forEach(path -> {
				try {
					Files.deleteIfExists(path);
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
		
		Files.delete(folder);
	}

	private static void write2File(String fileName, List<String> words) {
		String file = folder.resolve(fileName).toString();
		
		try( FileWriter writer = new FileWriter(file) ) {

			for (String string : words) {
				writer.write(string + "\n");
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
