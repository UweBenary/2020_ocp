package ub029_StreamCountMinMax;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		/*
		 * A1.
		 */
		System.out.println("*** A1");
		Locale[] locales = Locale.getAvailableLocales();
//		Optional<String> result = Stream.<Locale>of(locales)
//										.map( Locale::getDisplayCountry )
//										.max( Comparator.naturalOrder() );
//		System.out.println(result.orElse("Lexicographic max value not found."));

//		Comparator<Locale> comparator = (l1, l2) -> l1.getDisplayCountry().compareTo(l2.getDisplayCountry());
		Comparator<Locale> comparator = Comparator.comparing(Locale::getDisplayCountry);
		Optional<Locale> result = Stream.<Locale>of(locales)
										.max( comparator );
		System.out.println(result.get());	
		
		/*
		 * A2.
		 */
		System.out.println("*** A2");
		long count = Stream.<Locale>of(locales)
										.filter( l -> l.getLanguage().equals(new Locale("de").getLanguage()))
										.count();
		System.out.println(count);
		
		/*
		 * A3
		 */
		System.out.println("*** A3");
		// A    
	    Locale expectedLocale = getExpectedLocale(locales);
	    // B
	    
	    Optional<Locale> actualLocal = getLexicographicMinMaybeLocaleContainingTFromStream();

	    boolean testResult = testEqual(expectedLocale, actualLocal);
	    
	    displayTestResult(expectedLocale, actualLocal, testResult);
	    
	    if ( testResult ) {
	        System.out.println( actualLocal.get().getDisplayCountry() );
	        System.out.println( actualLocal.get().getDisplayLanguage() );
		}
	    	
	     

	}


	private static void displayTestResult(Locale expectedLocale, Optional<Locale> actualLocal, boolean testResult) {
		System.out.println("---- Test ----");
		String testResultString = testResult ? "Test passed." : 
	    			"Test failed! Expected: " + expectedLocale + " Actual:" + actualLocal;
	    
	    System.out.println( testResultString );
		System.out.println("--------------");
	}	
	

	private static Optional<Locale> getLexicographicMinMaybeLocaleContainingTFromStream() {
		Locale[] locales = Locale.getAvailableLocales();
		
//		Comparator<Locale> cmp = (loc1, loc2) -> 
//				loc1.getDisplayLanguage().compareTo(loc2.getDisplayLanguage());
		Comparator<Locale> cmp = Comparator.comparing(Locale::getDisplayLanguage);
		
		return Stream.<Locale>of(locales)
				.filter( l -> l.getDisplayCountry().contains("t") )
				.min( cmp );
	}


	private static boolean testEqual(Locale expectedLocale, Optional<Locale> actualLocal) {
		if (actualLocal.isEmpty()) {
			System.err.println("Optional<Locale> is empty!");
			return false;
		}
		return expectedLocale.equals(actualLocal.get());
	}

	private static Locale getExpectedLocale(Locale[] locales) {
		
		List<Locale> filtered = new ArrayList<>();
	    for (Locale locale : locales) {
	        if(locale.getDisplayCountry().contains("t")) {
	            filtered.add(locale);
	        }
	    }
	    
	    Comparator<Locale> cmp = (loc1, loc2) -> 
	            loc1.getDisplayLanguage().compareTo(loc2.getDisplayLanguage());
	    
	    filtered.sort( cmp );
	    
	    Locale min = null;
	    if(filtered.size() > 0) {
	        min = filtered.get( 0 );
	        System.out.println( min.getDisplayCountry() );
	        System.out.println( min.getDisplayLanguage() );
	    }
	    
	    return min;
	}

}
