package ub048_ThreadsExecutorServiceEnglishWords;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import words.ResourceLoader;

public class Tasks {

	static List<String> words;
	
	public static void main(String[] args) {

		/*
		 * A1
		 */
		words = ResourceLoader.loadEnglishWords();
		
		/*
		 * A2
		 */
		AtomicInteger result = new AtomicInteger();
		Runnable tWords = ()-> result.set( countWordsContaining("t") );
		
		/*
		 * A3
		 */
		Callable<Integer> ooWords = () -> countWordsContaining("oo");
		
		/*
		 * A4
		 */
		ExecutorService service = Executors.newCachedThreadPool();
		
				
		Future<AtomicInteger> tFuture = service.submit(tWords, result);
		
		Future<Integer> ooFuture = service.submit(ooWords);
		
		service.shutdown();
		
		try {			
			AtomicInteger tResult = tFuture.get();
			System.out.println("words containing 't': " + tResult);
			
			Integer ooResult = ooFuture.get();
			System.out.println("words containing 'oo': " + ooResult);
			
		} catch (ExecutionException | InterruptedException e) {
			e.printStackTrace();
		}
		
		/*
		 * A5
		 */
		Callable<Integer> wordsLength5Count = () -> countWordsOfLength5();
		
		service = Executors.newSingleThreadExecutor();
		Future<Integer> futureA5 = service.submit(wordsLength5Count);
		service.shutdown();
		try {
			System.out.println("words of length 5: " + futureA5.get() );
		} catch (ExecutionException | InterruptedException e) {
			e.printStackTrace();
		}
		
		/*
		 * A6
		 */
		double nTasks = 50.;
		int sublistsize = (int) Math.ceil(words.size()/nTasks);
		
		List<Callable<Integer>> callables = new ArrayList<Callable<Integer>>();
		for (int j = 0; j < nTasks; j++) {
			int from = j*sublistsize;
			int to = (j+1)*sublistsize - 1;
			callables.add( () -> countWordsOfLength5(from, to));
		}

		/*
		 * A7
		 */

		try {
			service = Executors.newCachedThreadPool();
			List<Future<Integer>> futureA6 = service.invokeAll(callables);
			service.shutdown();			
			
			System.out.println("words of length 5: " + totalSumOf(futureA6));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	} // end of main
	
	

	private static int totalSumOf(List<Future<Integer>> futureA6) {
		return futureA6.stream()
				.collect( Collectors.summingInt( f -> {
					try {
						return f.get().intValue();
					} catch (InterruptedException|ExecutionException e) {
						e.printStackTrace();
					}
					return 0;
				} ) );
	}

	private static Integer countWordsOfLength5(int from, int to) {
		return (int) words.stream()
				.skip(from)
				.limit(to - from + 1)
				.filter( s -> s.length() == 5 )
				.count(); 
	}
	private static Integer countWordsOfLength5() {
		return (int)  words.stream()
				.filter( s -> s.length() == 5 )
				.count();
	}

	private static Integer countWordsContaining(String letter) {
		return (int)  words.stream()
							.filter( s -> s.contains(letter))
							.count();
	}

}
