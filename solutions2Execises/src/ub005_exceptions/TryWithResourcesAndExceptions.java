package ub005_exceptions;

class MyClass implements AutoCloseable {

	@Override
	public void close() throws RuntimeException {
		throw new RuntimeException("close");		
	}
	
}

public class TryWithResourcesAndExceptions {

	public static void main(String[] args) throws Exception {

//		m1();
//		m2();
//		m3();
		m4();
	}

	private static void m4() throws Exception {
		try(MyClass mc = new MyClass() ) {
			throw new Exception("try");
		} catch (RuntimeException e) {
			throw e;
		}
	}
	
	private static void m3() throws Exception {
		try(MyClass mc = new MyClass() ) {
			throw new ArithmeticException("try");
		} catch (ArithmeticException e) {
			throw e;
		}
	}
	
	private static void m2() throws Exception {
		try(MyClass mc = new MyClass() ) {
			throw new ArithmeticException("try");
		} catch (Exception e) {
			throw e;
		}
	}

	private static void m1() throws Exception {
		try(MyClass mc = new MyClass() ) {
			throw new RuntimeException("try");
		} catch (Exception e) {
			throw e;
		}
	}

}
