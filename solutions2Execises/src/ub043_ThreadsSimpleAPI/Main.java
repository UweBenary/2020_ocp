package ub043_ThreadsSimpleAPI;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {

		/*
		 * A1
		 */
//		a1();
		
		/*
		 * A2
		 */
//		a2();
		
		/*
		 * A3
		 */
//		a3();
		
		/*
		 * A4
		 */
		a4();
		
		/*
		 * A5
		 */
//		a5();
	}
	private static void a1() {		
		Thread threadA1 = new Thread() { 
			@Override
			public void run() {
				while (true) {					
					Main.sleep(1000);
					System.out.println( Thread.currentThread().getId() + " : " + Thread.currentThread().getName() );
				}
			}
		};
		threadA1.start();
	}
	
	private static void a2() {		
		Runnable runnable = () -> {
			while (true) {					
				sleep(1000);
				System.out.println( Thread.currentThread().getId() + " : " + Thread.currentThread().getName() );
			}
		};
		
		Thread threadA2 = new Thread( runnable );
		threadA2.start();
	}

	private static void a3() {		
		
		for (int i = 0; i < 37; i++) {
			Thread thread = new Thread();
			System.out.println((i+1) + ": " + thread.getId());			
		}
//		Stream.iterate(1, x->x+1)
//			  .limit(37)
//			  .map( x -> new Thread() )
//			  .forEach( th->System.out.println(th.getId()) );
	}
	
	private static void a4() {		
//		Thread th = new Thread();
//		th.start();
//		final long startIdx = th.getId();
//		
//		Runnable runnable = () -> System.out.println((char)('A' + Thread.currentThread().getId() - startIdx - 1) );
//		
//		for (int i = 0; i < 26; i++) {
//			Thread thread = new Thread( runnable );
//			thread.start();		
//		}
//		
		for (char i = 'A'; i < 'A'+26; i++) {
			Thread thread = new Thread( ()->System.out.println(Thread.currentThread().getName()), String.valueOf(i) );
			thread.start();		
		}
	}
	
	private static void a5() {	
		
		Thread tom = new Thread( () -> bigJob(), "Tom");
		Thread jerry = new Thread( () -> bigJob(), "Jerry");
		
		tom.setPriority(Thread.MIN_PRIORITY);
		jerry.setPriority(Thread.MAX_PRIORITY);
		
		tom.start();
		jerry.start();
	}
	
	private static void sleep(long millis) {
		try {
			Thread.sleep(millis);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void bigJob() {
		Thread th = Thread.currentThread();
		System.out.println("Starte " + th.getName() 
			+ ". Priorität: " + th.getPriority());
		
		long start = System.currentTimeMillis();

		int exists = 0;
		for (int i = 0; i < 100_000; i++) {
			Path path = Paths.get("./"+i);
			if(Files.exists(path)) {
				exists++;
			}
		}
		
		long ende = System.currentTimeMillis();
		
		System.out.println("Thread: " + th.getName() 
				+ ", Zeit: " + (ende-start)/1000. + " Sek. / " + exists);
	}
}
