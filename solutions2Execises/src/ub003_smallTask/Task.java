package ub003_smallTask;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

public class Task {

	public static void main(String[] args) {

/*
 *  create OS object. constructor has arguments of type String and int
 */
		OS os1 = new OS("Mac", 9);
		System.out.println(os1); // Mac 9
		
/*
 * create array of following OS objects
 */
		OS[] osArray = {
				new OS("Linux", 3), 
				new OS("Windows", 95), 
				os1, 
				new OS("Linux", 1)};
		
/*
 * display method for array as table
 */
		print(osArray);

/*
 * sort for osNames
 */
		
		Arrays.sort(osArray);
		print(osArray);
		
/*
 * randomly mix array
 */
		Comparator<OS> cmp = (o1, o2) -> new Random().nextInt(2) -1; //50:50 chance of interchanging positions
		Arrays.sort(osArray, cmp);
		print(osArray);
		
/*
 * find position of os1 in array
 */
		Arrays.sort(osArray);
		int position = Arrays.binarySearch(osArray, os1);
		System.out.println("position of " + os1 + " in array: " + position);
	}

	private static void print(OS[] osArray) {
		System.out.println("***");
		for (int i = 0; i < osArray.length; i++) {
			System.out.format("| %2d. | %-8s | %3d | %n", 
					i+1, 
					osArray[i].getOsName(), 
					osArray[i].getVersion());
		}
		
	}
}
