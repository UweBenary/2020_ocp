package ub003_smallTask;

public class OS 
	implements Comparable<OS> {
	
	private String osName;
	private int version;

	public OS(String os, int version) {
		this.osName = os;
		this.version = version;
	}
	
	public String getOsName() {
		return osName;
	}

	public int getVersion() {
		return version;
	}



	@Override
	public String toString() {
		return osName + " " + version;
	}

	@Override
	public int compareTo(OS o) {
		int result = this.getOsName().compareTo( o.getOsName() );
		if (result == 0) {
			result = this.getVersion() - o.getVersion();
		}
		return result;
	}

}
