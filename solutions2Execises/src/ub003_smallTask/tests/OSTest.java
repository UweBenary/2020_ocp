package ub003_smallTask.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import ub003_smallTask.OS;


class OSTest {

	@Test
	final void testToString() {
		OS os = new OS("Mac", 9);

		String expected = "Mac 9";
		String actual = os.toString();
		assertEquals(expected, actual);	
	}
	
	@Test
	final void testGetOsName() {
		String expected = "Mac";
		OS os = new OS(expected, 9);
		String actual = os.getOsName();
		assertEquals(expected, actual);	
	}

	@Test
	final void testGetVersion() {
		int expected = 9;
		OS os = new OS("Mac", expected);
		int actual = os.getVersion();
		assertEquals(expected, actual);	
	}


	@Test
	final void testCompareTo() {
		OS os1 = new OS("Linux", 3);
		OS os2 = new OS("Linux", 1);
		OS os3 = new OS("Alpha", 3);
		
		int expected = 3-1;
		int actual = os1.compareTo(os2);				
		assertEquals(expected, actual);
		
		expected = 'L'-'A';
		actual = os2.compareTo(os3);				
		assertEquals(expected, actual);
	}
}
