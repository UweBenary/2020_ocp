package ub024_CollectionsAutos;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.TreeSet;

public class Main {

	public static void main(String[] args) {
		
		/*
		 * A2
		 */
		System.out.println("*** A2");
		
		VW vw = new VW("Golf", 1990);
		System.out.println(vw);
		
		BMW bmw = new BMW("Z4", 2000);
		System.out.println(bmw);

		/*
		 * A3
		 */
		VW vw1 = new VW("Golf", 1990);
		VW vw2 = new VW("Käfer", 2000);
		VW vw3 = new VW("Käfer", 1960);

		/*
		 * A4
		 */
		LinkedList<VW> linkedList = new LinkedList<VW>();
		linkedList.add(vw1);
		linkedList.add(vw2);
		linkedList.add(vw3);
		
//		Collections.sort(linkedList);
		
		HashSet<VW> hashSet = new HashSet<VW>();
		hashSet.add(vw1);
		hashSet.add(vw2);
		hashSet.add(vw3);
		
		TreeSet<VW> treeSet = new TreeSet<VW>();
		treeSet.add(vw1);
		treeSet.add(vw2);
		treeSet.add(vw3);
		
		Comparator<VW> cmp =  (VW x, VW y) -> x.compareTo(y);
		PriorityQueue<VW> priorityQueue = new PriorityQueue<VW>(cmp);
		priorityQueue.add(vw1);
		priorityQueue.add(vw2);
		priorityQueue.add(vw3);
		
		/*
		 * A5
		 */
		System.out.println("*** A5");
		
		System.out.println("LinkedList:");
		for (VW vW : linkedList) {
			System.out.println(vW);
		}
		
		System.out.println("HashSet:");
		for (VW vW : hashSet) {
			System.out.println(vW);
		}
		
		System.out.println("TreeSet:");
		for (VW vW : treeSet) {
			System.out.println(vW);
		}
		
		System.out.println("priorityQueue:");
		for (VW vW : priorityQueue) {
			System.out.println(vW);
		}
		
		/*
		 * A6
		 */
		System.out.println("*** A6");
		
		BMW bmw1 = new BMW("Z4", 2000);
		BMW bmw2 = new BMW("X6", 2008);
		
		
		System.out.println("ArrayList:");
		
		ArrayList<BMW> arrayList = new ArrayList<BMW>();
		arrayList.add(bmw1);
		arrayList.add(bmw2);

//		Collections.sort(arrayList);
		
		for (BMW bMW : arrayList) {
			System.out.println(bMW);
		}
		
		
		System.out.println("HashSet:");

		HashSet<BMW> hashSet2 = new HashSet<BMW>();
		hashSet2.add(bmw1);
		hashSet2.add(bmw2);
		
		for (BMW bMW : hashSet2) {
			System.out.println(bMW);
		}
		
		
		System.out.println("TreeSet:");

		TreeSet<BMW> treeSet2 = new TreeSet<BMW>();
		treeSet2.add(bmw1);
		treeSet2.add(bmw2);
		
		for (BMW bMW : treeSet2) {
			System.out.println(bMW);
		}
		
		
		System.out.println("ArrayDeque:");

		ArrayDeque<BMW> arrayDeque = new ArrayDeque<BMW>();
		arrayDeque.add(bmw1);
		arrayDeque.add(bmw2);
		
		for (BMW bMW : arrayDeque) {
			System.out.println(bMW);
		}
		
		
		/*
		 * A7
		 */
		System.out.println("*** A7");
		
		System.out.println( hashSet2.contains(bmw1) );
		
		/*
		 * A8
		 */
		System.out.println("*** A8");
		
		bmw1.setBaujahr(2014);
		System.out.println( hashSet2.contains(bmw1) );
		// false because new value of hashcode()
		
		
		/*
		 * A9
		 */
		VW vw4 = new VW("Polo", 2200);
		linkedList.add(vw4);
		
		/*
		 * A10
		 */
		System.out.println("*** A10");
		
		VW vw5 = new VW("Polo", 2200);
		int result =  Collections.binarySearch(linkedList, vw5);
		System.out.println(result);
		
		/*
		 * A11
		 */
		System.out.println("*** A11");
		
		Collections.sort(linkedList);
		System.out.println(linkedList);
		
		/*
		 * A12
		 */
		System.out.println("*** A12");
		
		Collections.sort(linkedList, Collections.reverseOrder());
		System.out.println(linkedList);
		
		/*
		 * A13
		 */
		System.out.println("*** A13");
		
		Collections.sort(linkedList);
		result = Collections.binarySearch(linkedList, vw5);
		System.out.println(result);
		
		/*
		 * A14
		 */
		System.out.println("*** A14");
		
		VW vw6 = new VW("Polo", 3300);
		result = Collections.binarySearch(linkedList, vw6);
		System.out.println(result);
	}

}
