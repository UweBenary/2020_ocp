package ub024_CollectionsAutos;

public class VW extends Auto {

	public VW(String modell, int baujahr) {
		super(modell, baujahr);
	}

	
	@Override
	public String toString() {
		return getClass().getSimpleName() + ". " + super.toString();
	}
}
