package ub024_CollectionsAutos.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.TreeSet;

import org.junit.jupiter.api.Test;

import ub024_CollectionsAutos.VW;

class VWTest {

	VW vw1 = new VW("Golf", 1990);
	VW vw2 = new VW("Käfer", 2000);
	VW vw3 = new VW("Käfer", 1960);

	VW[] expectedOrder = {vw1, vw3, vw2 };
	
	
	@Test
	final void testToString() {
		
		final String expected = "VW. Modell: Golf, Baujahr 1990";
		final String actual = vw1.toString();
		
		assertEquals(expected, actual);
	}
	
	@Test
	final void testLinkListSorting() {
		
		LinkedList<VW> list = new LinkedList<VW>();
		list.add(vw1);
		list.add(vw2);
		list.add(vw3);
		
		Collections.sort(list);
		
		Iterator<VW> iter = list.iterator();
		
		VW[] resultingOrder = new VW[list.size()];
		int i = 0;
		while (iter.hasNext()) {
			resultingOrder[i] = iter.next();
			i++;			
		}
		
		assertArrayEquals(expectedOrder, resultingOrder);
	}
	
	@Test
	final void testTreeSetSorting() {
		
		TreeSet<VW> set = new TreeSet<VW>();
		set.add(vw1);
		set.add(vw2);
		set.add(vw3);
		Iterator<VW> iter = set.iterator();
		
		VW[] resultingOrder = new VW[set.size()];
		int i = 0;
		while (iter.hasNext()) {
			resultingOrder[i] = iter.next();
			i++;			
		}

		assertArrayEquals(expectedOrder, resultingOrder);
	}
	
	
	@Test
	final void testPriorityQueueSorting() {
		
		Comparator<VW> cmp =  (VW x, VW y) -> x.compareTo(y);
		PriorityQueue<VW> queue = new PriorityQueue<VW>(cmp);
		queue.add(vw1);
		queue.add(vw2);
		queue.add(vw3);
//		Iterator<VW> iter = queue.iterator();
		
		VW[] resultingOrder = new VW[queue.size()];
//		int i = 0;
//		while (iter.hasNext()) {
//			resultingOrder[i] = iter.next();
//			i++;			
//		}

		for (int j = 0; j < resultingOrder.length; j++) {
			resultingOrder[j] = queue.remove();
		}

		assertArrayEquals(expectedOrder, resultingOrder);
	}
	
}
