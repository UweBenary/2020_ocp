package ub024_CollectionsAutos.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ub024_CollectionsAutos.BMW;

class BMWTest {

	@Test
	final void testToString() {
		
		BMW bmw = new BMW("Z4", 2000);
		
		final String expected = "BMW. Modell: Z4, Baujahr 2000";
		final String actual = bmw.toString();
		
		assertEquals(expected, actual);
	}

}
