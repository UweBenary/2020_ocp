package ub024_CollectionsAutos;

public class BMW extends Auto {

	public BMW(String modell, int baujahr) {
		super(modell, baujahr);
	}
	
	public void setBaujahr(int baujahr) {
		super.setBaujahr(baujahr);
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + ". " + super.toString();
	}




}
