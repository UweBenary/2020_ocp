package ub024_CollectionsAutos;

abstract class Auto implements Comparable<Auto> {

	private int baujahr;
	private String modell;
	
	
	public Auto(String modell, int baujahr) {
		this.baujahr = baujahr;
		this.modell = modell;
	}

	protected void setBaujahr(int baujahr) {
		this.baujahr = baujahr;
	};	
	
	@Override
	public String toString() {
		return "Modell: " + modell + ", Baujahr " + baujahr;
	}
		
	@Override
	public int compareTo(Auto o) {
		int result = modell.compareTo(o.modell);
		
		if (result == 0) {
			result = baujahr - o.baujahr;
		}
		
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if ( obj instanceof Auto) {
			Auto other = (Auto) obj;
			return baujahr == other.baujahr && modell.equals( other.modell );
		}
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		return modell.hashCode() + 31*Integer.valueOf(baujahr).hashCode();
	}

}
