package ub002_QuizIORead.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.nio.file.Paths;

import org.junit.jupiter.api.Test;

import ub002_QuizIORead.QuizIO;

class QuizIOTest {
	
    String pathToTextFile = Paths.get("src","ub002_QuizIORead","tests","KontrollfragenTest_abbrev.md").toString();
	String expectedString = "	# Haupt-Thema\n" + "	\n" + "	## Thema 1\n";
	

	@Test
	final void testLoadText() {
		String actual = QuizIO.loadText(pathToTextFile);
		assertEquals(expectedString, actual);
	}

}
