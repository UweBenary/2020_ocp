package ub002_QuizIORead;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class QuizIO {

	public static void main(String[] args) {
		
		Path path = Paths.get("src","ub002_QuizIORead","Kontrollfragen.md");

	    String textFromFile = loadText(path.toString());
	    
	    System.out.println(textFromFile);
	    
	}
	
	public static String loadText(String pathToFile) {
		StringBuilder sb = new StringBuilder();
		
		try(BufferedReader in = new BufferedReader(new FileReader(pathToFile))) {
			String line;
			while( (line = in.readLine()) != null ) {
				sb.append(line).append("\n");
			}
			
		} catch (IOException e) {
			throw new UncheckedIOException("Cannot read file " + pathToFile, e);
		}
		
		return sb.toString();
	}
	
}
