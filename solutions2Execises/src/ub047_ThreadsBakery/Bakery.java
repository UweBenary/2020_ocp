package ub047_ThreadsBakery;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedDeque;

public class Bakery {
	
	private String name;	
	private static int STORAGE_CAPACITY;
	private ConcurrentLinkedDeque<Bun> storage = new ConcurrentLinkedDeque<Bun>();

	public Bakery(String name) {
		this(name, 100);
	}
	
	public Bakery(String name, int storageCapacity) {
		this.name = name;
		STORAGE_CAPACITY = storageCapacity;
	}
	
	public void produceBuns() throws InterruptedException {
		if (storage.size() < STORAGE_CAPACITY - 10) {
			storeBuns( bake10Buns() );
			System.out.printf("Bakery %s has %d buns in storage.\n", name, storage.size() );	
		}
	}
	
	private void storeBuns(List<Bun> buns) {
		for (Bun bun : buns) {
			if (storage.size() < STORAGE_CAPACITY ) {
				storage.add(bun);
			}
		}
	}
	
	private List<Bun> bake10Buns() throws InterruptedException {
		List<Bun> buns = new ArrayList<Bun>();
		for (int i = 0; i < 10; i++) {
			buns.add(new Bun());
		}		
		int variance = new Random().nextInt(11) - 5;
		Thread.sleep(100 + variance );
		
		return buns;
	}
	
	public List<Bun> deliverBun(int numberOfBuns) {
		List<Bun> buns = new ArrayList<Bun>();
		for (int i = 0; i < numberOfBuns; i++) {
			Bun bun = storage.pollFirst();
			if (bun == null) {
				break;
			}
			buns.add( bun );
		}	
		return buns;
	}
}
