package ub047_ThreadsBakery;

public class Main {
	
	public static void main(String[] args) {
		
		Bakery bakery = new Bakery("Agethen");
		
		Thread bakeryThread = new Thread( ()-> {			
			while (true) {
				try {
					synchronized (bakery) {
						bakery.produceBuns();
						bakery.notifyAll();
					}

				} catch (Exception e) {
					break;
				}	
			}
		});

		Person peter = new Person("Peter", bakery);
		Person paul = new Person("Paul", bakery);
		Person mary = new Person("Mary", bakery);
		

		Thread peterThread = new Thread( ()-> {			
			while (true) {
				try {
					synchronized (bakery) {
						bakery.wait();	
						peter.buyBuns();
					}	
				} catch (InterruptedException e) {
					break;
				}
			}
		});
		
		Thread paulThread = new Thread( ()-> {			
			while (true) {
				try {
					synchronized (bakery) {
						bakery.wait();	
						paul.buyBuns();
					}	
				} catch (InterruptedException e) {
					break;
				}
			}
		});
		
		Thread maryThread = new Thread( ()-> {			
			while (true) {
				try {
					synchronized (bakery) {
						bakery.wait();	
						mary.buyBuns();
					}					
				} catch (InterruptedException e) {
					break;
				}
			}
		});
		

		bakeryThread.start();
		peterThread.start();
		paulThread.start();
		maryThread.start();
	}

}
