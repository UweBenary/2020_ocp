package ub047_ThreadsBakery;

import java.util.List;
import java.util.Random;

public class Person {
	
	private String name;
	private Bakery favouriteBakery;
	
	public Person(String name, Bakery favouriteBakery) {
		this.name = name;
		this.favouriteBakery = favouriteBakery;
	}
	
	public void buyBuns() {
		int numberOfBuns = new Random().nextInt(13) + 3;
		List<Bun> buns = favouriteBakery.deliverBun(numberOfBuns);
		System.out.printf("%s bought %d of %d buns \n", name, buns.size(), numberOfBuns);
	}

}
