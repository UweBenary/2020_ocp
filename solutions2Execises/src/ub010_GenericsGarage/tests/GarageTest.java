package ub010_GenericsGarage.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ub010_GenericsGarage.Garage;
import ub010_GenericsGarage.LKW;
import ub010_GenericsGarage.PKW;

class GarageTest {

	Garage<PKW> garagePKW = new Garage<>();	
	Garage<LKW> garageLKW = new Garage<>();
	
	@Test
	void testReinfahrenPKW() {
		
//		assertTrue(garagePKW.reinfahren( new LKW() )); //Compiler error as expected
		assertTrue(garagePKW.reinfahren( new PKW() ));
		
		assertFalse(garagePKW.reinfahren( new PKW() ));

	}
	
	@Test
	void testReinfahrenLKW() {
		
//		assertTrue(garageLKW.reinfahren( new PKW() )); //Compiler error as expected
		assertTrue(garageLKW.reinfahren( new LKW() ));
		
		assertFalse(garageLKW.reinfahren( new LKW() ));

	}

}
