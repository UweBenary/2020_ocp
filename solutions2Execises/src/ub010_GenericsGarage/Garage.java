package ub010_GenericsGarage;

public class Garage <T extends Fahrzeug> {

	private T vehicle;
	
	public boolean reinfahren(T vehicle) {
		if (this.vehicle == null) {
			this.vehicle = vehicle;
			return true;
		}
		return false;
	}
}
