package ub050_ThreadsForkJoinList;

import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

import javax.sound.midi.Soundbank;

public class A2 {

	public static void main(String[] args) {

		List<String> words = A1.getWordList();
		System.out.println(words.get(0));
		System.out.println(words.size());
		
		ForkJoinPool pool = ForkJoinPool.commonPool();
		RecursiveAction task = new MakeWordsGreatAgain(words);	
		pool.invoke(task);
		
		System.out.println(words.get(0));
		System.out.println(
				words.stream()
					.filter( s -> s.equals(s.toUpperCase()))
					.count());
		
	}

}
