package ub050_ThreadsForkJoinList;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class A3 {

	public static void main(String[] args) {
		List<String> words = A1.getWordList();
		System.out.println(words.size());

		RecursiveTask<String> task = new GetLongest(words);
		ForkJoinPool pool = new ForkJoinPool();
		
		String longestString = pool.invoke(task);
		System.out.println(longestString);

	}

}
