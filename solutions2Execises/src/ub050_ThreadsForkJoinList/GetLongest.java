package ub050_ThreadsForkJoinList;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.RecursiveTask;

public class GetLongest extends RecursiveTask<String> {
	
	private List<String> list;
	private static final int maxSize = 10;
	
	public GetLongest(List<String> list) {
		this.list = list;
	}
	
	@Override
	protected String compute() {
		
		if (list.size() < maxSize) {
			
			return getLongestString(list);
			
		} else {
			GetLongest task1 = new GetLongest(list.subList(0, list.size()/2));
			GetLongest task2 = new GetLongest(list.subList(list.size()/2, list.size())); 
			
			task1.fork();
			String[] result = {
					task2.compute(),
					task1.join()
					};
			
			return getLongestString(Arrays.asList(result));
		}
	}

	private String getLongestString(List<String> strings) {
		String longestString = "";
		
		for (String string : strings) {
			longestString = getLongest(longestString, string);
		}
		
		return longestString;
	}

	private String getLongest(String s1, String s2) {
		return s1.length() > s2.length() ? s1 : s2;
	}

}
