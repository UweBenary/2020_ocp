package ub050_ThreadsForkJoinList;

import java.util.List;
import java.util.concurrent.RecursiveAction;

public class MakeWordsGreatAgain extends RecursiveAction {

	private List<String> list;
	private static final int maxSize = 100;
	
	public MakeWordsGreatAgain(List<String> list) {
		this.list = list;
	}

	@Override
	protected void compute() {
		if (list.size() < maxSize) {
			list.replaceAll(String::toUpperCase);
			
		} else {
			int center = list.size()/2;
			MakeWordsGreatAgain task1 = new MakeWordsGreatAgain(list.subList(0, center));
			MakeWordsGreatAgain task2 = new MakeWordsGreatAgain(list.subList(center, list.size())); 
			
			invokeAll(task1, task2);
		}

	}

}
