package ub019_DequeBrowserHistory;

public class UrlMementoOriginator {

	private String url;

	public void setUrl(String url) {
		this.url = url;
	} 
	
	public UrlMemento createMemento() {
		return new UrlMemento(url);
	}
	
	public String restoreFromMemento(UrlMemento memento) {
		url = memento.getUrl();
		return url;
	}
	
}
