package ub019_DequeBrowserHistory;

import java.util.ArrayDeque;
import java.util.NoSuchElementException;

public class BrowserHistory {
	
	private static final int BROWSER_HISTORY_SIZE = 4;
	
	private String currentUrl;
	private ArrayDeque<UrlMemento> previousUrls =  new ArrayDeque<UrlMemento>();
	private ArrayDeque<UrlMemento> nextUrls =  new ArrayDeque<UrlMemento>();
	private UrlMementoOriginator originator = new UrlMementoOriginator();
	
	public void open(String url) {
		if (currentUrl != null) {
			addUrlMemento(previousUrls);	
		}	
		
		currentUrl = url;

		trimBrowserHistory();

		deleteNextUrls();		
	}
	
	private void trimBrowserHistory() {
		if (previousUrls.size() > BROWSER_HISTORY_SIZE) {
			previousUrls.removeFirst();
		}	
	}

	private void addUrlMemento( ArrayDeque<UrlMemento> mementos ) {
		originator.setUrl(currentUrl);
		UrlMemento memento = originator.createMemento();
		mementos.addLast( memento );
	}

	private void deleteNextUrls() {
		nextUrls.clear();
	}

	/*
	 * openPrevious and openNext are very similar
	 * refractoring?
	 */
	public String openPrevious() { 
		try {
			UrlMemento memento = previousUrls.removeLast();
			addUrlMemento(nextUrls);	
			currentUrl = originator.restoreFromMemento(memento);	
			
		} catch (NoSuchElementException e) {
			throw new IllegalStateException("Previous history is empty.");
		}
		return currentUrl;
	}
	
	public String openNext() {
		try {
			UrlMemento memento = nextUrls.removeLast();
			addUrlMemento(previousUrls);	
			currentUrl = originator.restoreFromMemento(memento);
		} catch (NoSuchElementException e) {
			throw new IllegalStateException("Next history is empty.");
		}
		return currentUrl;
	}
	
	public String getCurrent() {		
		return currentUrl;
	}

}
