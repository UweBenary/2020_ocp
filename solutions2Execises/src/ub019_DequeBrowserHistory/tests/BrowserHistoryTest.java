package ub019_DequeBrowserHistory.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ub019_DequeBrowserHistory.BrowserHistory;

class BrowserHistoryTest {

	BrowserHistory browserHistory;
	
	@BeforeEach
	final void init() {
		browserHistory = new BrowserHistory();
	}
	
	@Test
	final void testGetCurrent() {
		assertNull( browserHistory.getCurrent() );		
	}
	
	@Test
	final void testOpen() {
		final String expected = "test.com";
		browserHistory.open(expected);
		final String actual = browserHistory.getCurrent();
		assertEquals(expected, actual);
	}
	
	@Test
	final void testOpenGreater5() {
		for (int i = 0; i < 6; i++) {
			browserHistory.open("test" + i + ".com");
		}
		assertEquals("test5.com", browserHistory.getCurrent() );
	}
	
	@Test
	final void testOpenGreater5DeletesFirstEntry() {
		for (int i = 1; i < 7; i++) {
			browserHistory.open("test" + (i) + ".com");
		}		
		for (int i = 1; i < 5; i++) {
			browserHistory.openPrevious();
		}
		assertEquals("test2.com", browserHistory.getCurrent() );
	}
	
	@Test
	final void testOpenPrevious() {
		final String expected = "test1.com";
		browserHistory.open(expected);
		browserHistory.open("test2.com");
		
		browserHistory.openPrevious();
		final String actual = browserHistory.getCurrent();
		assertEquals(expected, actual);
	}
	
	@Test
	final void testOpenPreviousThrowsExc() {
		browserHistory.open("test.com");
		assertThrows(IllegalStateException.class, 
				() -> browserHistory.openPrevious());
	}
	
	@Test
	final void testOpenPreviousKeepsLastEntryUponThrowsExc() {
		browserHistory.open("test.com");
		try {
			browserHistory.openPrevious();
		} catch (IllegalStateException e) {
			System.out.println("IllegalStateException caught...");
		}
		assertEquals("test.com", browserHistory.getCurrent());
	}

	@Test
	final void testOpenNext() {
		browserHistory.open("test1.com");
		
		final String expected = "test2.com";
		browserHistory.open(expected);
		
		browserHistory.openPrevious();
		browserHistory.openNext();
		
		final String actual = browserHistory.getCurrent();
		assertEquals(expected, actual);
	}

	@Test
	final void testOpenNextThrowsExc() {
		browserHistory.open("test1.com");
		browserHistory.open("test2.com");
		
		browserHistory.openPrevious();
		browserHistory.openNext();

		assertThrows(IllegalStateException.class, 
				() -> browserHistory.openNext());
	}
	
	@Test
	final void testOpenNextKeepsLastEntryUponThrowsExc() {
		browserHistory.open("test1.com");		
		
		final String expected = "test2.com";
		browserHistory.open(expected);
		
		browserHistory.openPrevious();
		browserHistory.openNext();
		try {
			browserHistory.openNext();
		} catch (IllegalStateException e) {
			System.out.println("IllegalStateException caught...");
		}
		assertEquals(expected, browserHistory.getCurrent());
	}
	
	
	@Test
	final void testOpenDeletesNextBrowserHistory() {
		for (int i = 1; i < 7; i++) {
			browserHistory.open("test" + (i) + ".com");
		}		
		for (int i = 1; i < 3; i++) {
			browserHistory.openPrevious();
		}
		browserHistory.open("test100.com");
		
		assertThrows(IllegalStateException.class, 
				() -> browserHistory.openNext());
	}
}
