package ub019_DequeBrowserHistory.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ub019_DequeBrowserHistory.UrlMemento;

class UrlMementoTest {

	@Test
	void testGetUrl() {
		final String expectedString = "test.com";
		
		UrlMemento memento = new UrlMemento(expectedString);
		
		assertEquals(expectedString, memento.getUrl() );
	}

}
