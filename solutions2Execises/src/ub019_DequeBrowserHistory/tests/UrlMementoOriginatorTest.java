package ub019_DequeBrowserHistory.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ub019_DequeBrowserHistory.UrlMemento;
import ub019_DequeBrowserHistory.UrlMementoOriginator;

class UrlMementoOriginatorTest {

	
	@Test
	void testCreateMemento() {
		final String expectedString = "test.com";
		
		UrlMementoOriginator orginator = new UrlMementoOriginator();
		orginator.setUrl(expectedString);
		UrlMemento memento = orginator.createMemento();
		
		assertEquals(expectedString, memento.getUrl());
	}

	@Test
	void testRestoreFromMomento() {
		final String expectedString = "test.com";
		
		UrlMementoOriginator orginator = new UrlMementoOriginator();
		orginator.setUrl(expectedString);
		UrlMemento memento = orginator.createMemento();
		
		assertEquals(expectedString, orginator.restoreFromMemento(memento));		
	}

}
