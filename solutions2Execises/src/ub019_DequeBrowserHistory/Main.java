package ub019_DequeBrowserHistory;

public class Main {
	
	public static void main(String[] args) {
		
		/*
		 * A1
		 */
		BrowserHistory h = new BrowserHistory();
		
		/*
		 * A2
		 */		
		System.out.println("*** A2");
		h.open("u1.com"); // [u1.com]
		h.open("u2.com"); // u1.com < [u2.com]
		h.open("u3.com"); // u1.com < u2.com < [u3.com]
		h.open("u4.com"); // u1.com < u2.com < u3.com < [u4.com]
		h.open("u5.com"); // u1.com < u2.com < u3.com < u4.com < [u5.com]

		System.out.println( h.getCurrent() ); 	// u5.com
		
		/*
		 * A3
		 */
		System.out.println("*** A3");
		h.open("u6.com");
		System.out.println( h.getCurrent() );	// u6.com
		
		// u2.com < u3.com < u4.com < u5.com < u6.com

		/*
		 * A4
		 */
		System.out.println("*** A4");
		h.openPrevious();
		h.openPrevious(); 
		h.openPrevious(); 
		h.openPrevious(); 
		System.out.println( h.getCurrent() );	// u2.com
		
		/*
		 * A5
		 */
		System.out.println("*** A5");
		try {
			h.openPrevious();
		} catch (IllegalStateException e) {
			System.out.println( e.getMessage() ); // previous history is empty
		}
		System.out.println( h.getCurrent() );	// u2.com
		
		/*
		 * A6
		 */
		System.out.println("*** A6");
		h.openNext();
		h.openNext();
		h.openNext();
		h.openNext();
		System.out.println( h.getCurrent() );	// u6.com
		
		/*
		 * A7
		 */
		System.out.println("*** A7");
		try {
			System.out.println( h.openNext() );
		} catch (IllegalStateException e) {
			System.out.println( e.getMessage() ); // next history is empty
		}
		
		System.out.println( h.getCurrent() );	// u6.com
		
		/*
		 * A8
		 */
		System.out.println("*** A8");
		h.openPrevious();
		h.openPrevious(); 
		System.out.println( h.getCurrent() );	// u4.com
		

		h.open("u7.com");
		
		try {
			System.out.println( h.openNext() );
		} catch (IllegalStateException e) {
			System.out.println( e.getMessage() ); // next history is empty
		}
		
	}

}
