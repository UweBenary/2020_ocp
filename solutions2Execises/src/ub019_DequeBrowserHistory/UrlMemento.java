package ub019_DequeBrowserHistory;

public class UrlMemento {
	
	private String url;

	public UrlMemento(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}
}
