package ub040_FilesCounter;

import java.nio.file.NotDirectoryException;

public class Test {

	public static void main(String[] args) {
		
		/*
		 * A1
		 */
		System.out.println("*** A1");
		 FilesCounter fc = null;
		try {
		 fc = new FilesCounter("C:\\Windows");
		 System.out.println(fc);
		} catch (NotDirectoryException e) {
			e.getMessage();
		}
		
		/*
		 * A2
		 */
		System.out.println("*** A2");
		
		int anzahl = fc.count("txt");
		System.out.println(anzahl);
		
		/*
		 * A3
		 */
		System.out.println("*** A3");
		
		anzahl = fc.countDeep("txt");
		System.out.println(anzahl);
		

	}

}
