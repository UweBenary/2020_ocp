package ub040_FilesCounter.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.nio.file.NotDirectoryException;

import org.junit.jupiter.api.Test;

import ub040_FilesCounter.FilesCounter;

class FilesCounterTest {

	@Test
	final void testFilesCounter() {
		try {
			FilesCounter fc = new FilesCounter("C:\\Windows");
			final String expected = "FilesCounter @ C:\\Windows";
			String actual = fc.toString();
			assertEquals(expected, actual);
			
		} catch (NotDirectoryException e) {
			e.getMessage();
		}
		
	}

	@Test
	final void testCount() throws NotDirectoryException {
		FilesCounter fc = new FilesCounter("src/ub040_FilesCounter/tests/dummyFolder");
		int expectedFileCount = 1;
		int actualFileCount = fc.count("txt");
		assertEquals(expectedFileCount, actualFileCount);
	}

	@Test
	final void testCountDeep() throws NotDirectoryException {
		FilesCounter fc = new FilesCounter("src/ub040_FilesCounter/tests/dummyFolder");
		int expectedFileCount = 3;
		int actualFileCount = fc.countDeep("txt");
		assertEquals(expectedFileCount, actualFileCount);
	}

}
