package ub040_FilesCounter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NotDirectoryException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class FilesCounter {
	
	private final String dirString;

	public FilesCounter(String dirString) throws NotDirectoryException {
		if (! Files.isDirectory( Paths.get(dirString) )) {
			throw new NotDirectoryException(dirString + " is not a directory.");
		}
		this.dirString = dirString;		
	}

	public int count(String fileFormatString) {
		Stream<Path> ds = getStream(1);
		
		return filterAndCount(ds, fileFormatString) ;
	}

	public int countDeep(String fileFormatString) {
		Stream<Path> ds = getStream(Integer.MAX_VALUE);
		
		return filterAndCount(ds, fileFormatString) ;
	}

	@Override
	public String toString() {
		return "FilesCounter @ " + dirString;
	}
	

	private Stream<Path> getStream(int maxDepth) {	
		Stream<Path> ds = Stream.empty();
		
		try {
			ds = Files.walk( Paths.get(dirString), maxDepth);  //walkFileTree(), SimpleFileVisitor::visitFileFailed
			return ds;
		} catch (IOException e) {
			e.printStackTrace();
			ds.close();
		}
		return ds;
	}

	private int filterAndCount(Stream<Path> ds, String fileFormatString) {
		long result = ds.filter(p -> getFileExtension(p).equals(fileFormatString) )
						.count();
		
		if (result > Integer.MAX_VALUE) {
			String warningMessage =  String.format("Warning: number of files (%d) is larger than Integer.MAX_VALUE", result);
			throw new IllegalStateException(warningMessage);
		}
		ds.close();
		return (int) result;
	}
	
	//  https://stackoverflow.com/a/21974043
	private String getFileExtension(Path path) {
	    String name = path.getFileName().toString();
	    int lastIndexOf = name.lastIndexOf(".");
	    if (lastIndexOf == -1 					// txt 	(missing .)
	    	|| lastIndexOf == 0 				// .txt (folder)
	    	|| lastIndexOf == name.length()-1 	// txt. (missing extension)
	    	) {
	    		return ""; // empty extension
	    }
	    return name.substring(lastIndexOf+1);
	}
}
