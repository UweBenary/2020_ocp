package ub035_StreamCollectCollectorsPersonen;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) {

		Person[] personen = {
				new Person("Tom", "Bauarbeiter(in)"),	
				new Person("Jerry", "Lehrer(in)"),	
				new Person("Peter", "Metzger(in)"),	
				new Person("Paul", "Bauarbeiter(in)"),	
				new Person("Mary", "Lehrer(in)"),	
			};
		
		/*
		 * A1
		 */
		System.out.println("A1: " + a1(personen) );
		
		
		/*
		 * A2
		 */
		System.out.println("A2: " + a2(personen) );
		
		
		/*
		 * A3
		 */
		System.out.println("A3: " + a3(personen) );
		
		/*
		 * A4
		 */
		System.out.println("A4: " + a4(personen) );
		
		/*
		 * A5
		 */
		System.out.println("A5: " + a5(personen) );
		
		
		/*
		 * A
		 */
		System.out.println("A i: " + ai(personen) );
		System.out.println("A ii: " + aii(personen) );
		System.out.println("A iii: " + aiii(personen) );
	}

	private static Map<Boolean, List<Person>> a5(Person[] personen) {
		return Arrays.stream(personen)
				 .collect( Collectors.partitioningBy(p->p.getBeruf()=="Bauarbeiter(in)"));
	}

	private static Map<String, List<Person>> a4(Person[] personen) {
		Function<Person, String> classifier = Person::getBeruf;
		return Arrays.stream(personen)
				 .collect( Collectors.groupingBy(classifier));
	}

	private static Map<Person, String> aiii(Person[] personen) {
		UnaryOperator<Person> keyMapper = Person::self;
		Function<Person, String> valueMapper = Person::getBeruf;
		return Arrays.stream(personen)
				 .collect( Collectors.toMap(keyMapper, valueMapper));
	}
	
	private static Map<String, Person> aii(Person[] personen) {
		Function<Person, String> keyMapper = p -> p.getName() + "_" + p.getBeruf();
		UnaryOperator<Person> valueMapper = p -> p;
		return Arrays.stream(personen)
				 .collect( Collectors.toMap(keyMapper, valueMapper));
	}
	
	private static List<String> ai(Person[] personen) {
		Function<Person, String> mapper = p -> {
			String s = p.getBeruf();
			return s.replace("(in)", "(m/w/d)");
		};
		Collector<String, ?, List<String>> downstream = Collectors.toList();
		return Arrays.stream(personen)
				 .collect( Collectors.mapping(mapper, downstream));
	}

	private static Collection<String> a3(Person[] personen) {
		Function<Person, String> mapper = Person::getBeruf;
		Collector<String, ?, Set<String>> downstream = Collectors.toSet();
		return Arrays.stream(personen)
				 .collect( Collectors.mapping(mapper, downstream));
	}

	private static Map<String, Person> a2(Person[] personen) {
		Function<Person, String> keyMapper = Person::getName;
		UnaryOperator<Person> valueMapper = p -> p;
		return Arrays.stream(personen)
					 .collect( Collectors.toMap(keyMapper, valueMapper));
	}

	private static TreeSet<Person> a1(Person[] personen) {
		return Arrays.stream(personen)
					 .collect( Collectors.toCollection(TreeSet::new) );
	}

}
