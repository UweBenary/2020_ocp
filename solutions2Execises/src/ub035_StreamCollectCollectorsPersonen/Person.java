package ub035_StreamCollectCollectorsPersonen;

public class Person implements Comparable<Person> {
	
	private String name;
	private String beruf;
	
	public Person(String name, String beruf) {
		this.name = name;
		this.beruf = beruf;
	}
	
	// more methods here...
	
	public Person self() {
		return this;
	}
	
	public String toString() {
		return name;
	}

	public String getName() {
		return name;
	}

	public String getBeruf() {
		return beruf;
	}

	@Override
	public int compareTo(Person other) {
		int result = name.compareTo(other.name);
		if (result == 0) {
			result = beruf.compareTo(other.beruf);
		}
		return result;
	}
	
	
}