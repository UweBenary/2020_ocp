package ub033_StreamCollectWarenkorb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

public class Main {
	
	public static final Map<String, Integer> priceList = new TreeMap<String, Integer>( Comparator.naturalOrder() ); 
	
	static {
		priceList.put("Brot", 129);
		priceList.put("Wurst", 230);
		priceList.put("Milch", 99);
	}
	public static void main(String[] args) {
		
		/*
		 * A1
		 */
		List<Produkt> warenkorb = new ArrayList<>();
		warenkorb.add(new Produkt("Brot", 129));
		warenkorb.add(new Produkt("Wurst", 230));
		warenkorb.add(new Produkt("Milch", 99));
		warenkorb.add(new Produkt("Milch", 99));
		
		int total = totalPrize(warenkorb);
		System.out.println("A1: " + total);

		/*
		 * A2
		 */
		List<Bestellung> bestellungen = new ArrayList<>();
		bestellungen.add(new Bestellung("Brot", 3));
		bestellungen.add(new Bestellung("Wurst", 1));
		bestellungen.add(new Bestellung("Milch", 2));
		
		warenkorb = buildWarenkorb(bestellungen);
		System.out.println(warenkorb);
	}


	public static List<Produkt> buildWarenkorb(List<Bestellung> bestellungen) {
		
		BiConsumer<List<Produkt>, Bestellung> accumulator = (prodList, bestellung) -> {
			String name = bestellung.getProduktName();
			int preis = priceList.get(name);
			for (int i = 0; i < bestellung.getAnzahl(); i++) {
				prodList.add( new Produkt(name, preis));
			}
		};
		return bestellungen.parallelStream()
							.collect(ArrayList::new, accumulator, List::addAll);
	}

	public static int totalPrize(List<Produkt> warenkorb) {
		return warenkorb.stream()
						.collect( Collectors.summingInt(Produkt::getPreis) ) ;
	}

}
