package ub033_StreamCollectWarenkorb.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import ub033_StreamCollectWarenkorb.Bestellung;
import ub033_StreamCollectWarenkorb.Main;
import ub033_StreamCollectWarenkorb.Produkt;

class MainTest {

	@Test
	final void testTotalPrize() {
		final int expected = 1;
		
		List<Produkt> warenkorb = new ArrayList<>();
		warenkorb.add(new Produkt("Test", expected));
		
		final int actual = Main.totalPrize(warenkorb);

		assertEquals(expected, actual);
	}

	@Test
	final void testBuildWarenkorb() {
		final String name = "Brot";
		final List<Produkt> expected = new ArrayList<>( );
		expected.add(new Produkt(name, Main.priceList.get(name)));
		
		List<Bestellung> bestellungen = new ArrayList<>();
		bestellungen.add(new Bestellung(name, 1));
		
		final List<Produkt> actual = Main.buildWarenkorb(bestellungen);

		assertEquals(expected.toString(), actual.toString());
	}

}
