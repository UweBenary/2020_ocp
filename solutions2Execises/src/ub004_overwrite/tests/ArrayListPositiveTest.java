package ub004_overwrite.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import ub004_overwrite.ArrayListPositive;
import ub004_overwrite.NotPostivieArgumentException;
import ub004_overwrite.NullArgumentException;

class ArrayListPositiveTest {

//	@Test
//	final void testAddFailsUponNonIntegerArgument() {
//		try {
//			ArrayListPositive array = new ArrayListPositive();
//			array.add("Test");
//			fail("Expected: IllegalArgumentException");
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
//		
//		try {
//			ArrayListPositive array = new ArrayListPositive();
//			array.add(true);
//			fail("Expected: IllegalArgumentException");
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
//		
//		try {
//			ArrayListPositive array = new ArrayListPositive();
//			array.add(2.5);
//			fail("Expected: IllegalArgumentException");
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
//	}
	
	@Test
	final void testAddFailsUponNullArgument() {
		ArrayListPositive array = new ArrayListPositive();
		assertThrows(NullArgumentException.class, () -> array.add(null) );
	}
	
	@ParameterizedTest
	@CsvSource	( {
		"0",
		"-1",
	} )
	final void testAddFailsUponNonPositiveIntegerArgument(int intValue) {
		ArrayListPositive array = new ArrayListPositive();
		assertThrows(NotPostivieArgumentException.class, () -> array.add( Integer.valueOf(intValue) ) );
	}
	
	@Test
	final void testAddAtPositionFailsUponNullArgument() {
			ArrayListPositive array = new ArrayListPositive();
			assertThrows(NullArgumentException.class, () -> array.add(0, null) );
	}
	
	@ParameterizedTest
	@CsvSource	( {
		"0",
		"-1",
	} )
	final void testAddAtPositionFailsUponNonPositiveIntegerArgument(int intValue) {
		ArrayListPositive array = new ArrayListPositive();
		assertThrows(NotPostivieArgumentException.class, () -> array.add(0, Integer.valueOf(intValue) ) );
	}
	
	@Test
	final void testAddAllFailsUponNullArgument() {
		ArrayListPositive array = new ArrayListPositive();
		assertThrows(NullArgumentException.class, () -> array.addAll(null) );
	}
	
	@Test
	final void testAddAllFailsUponNullReferencesCollectionArgument() {
		ArrayListPositive array = new ArrayListPositive();
		Collection<Integer> coll = Arrays.asList(null, new Integer(1));
		assertThrows(NullArgumentException.class, () -> array.addAll(coll) );
	}
	
	@ParameterizedTest
	@CsvSource	( {
		"0",
		"-1",
	} )
	final void testAddAllFailsUponNotPositiveIntegerInCollectionArgument(int intValue) {
		ArrayListPositive array = new ArrayListPositive();
		Collection<Integer> coll = Arrays.asList(new Integer(1), Integer.valueOf(intValue));
		assertThrows(NotPostivieArgumentException.class, () -> array.addAll(coll) );
	}
	
	@Test
	final void testAddAllAtPositionFailsUponNullArgument() {
		ArrayListPositive array = new ArrayListPositive();
		assertThrows(NullArgumentException.class, () -> array.addAll(0, null) );
	}
	
	@Test
	final void testAddAllAtPositionFailsUponNullReferencesCollectionArgument() {
		ArrayListPositive array = new ArrayListPositive();
		Collection<Integer> coll = Arrays.asList(null, new Integer(1));
		assertThrows(NullArgumentException.class, () -> array.addAll(0, coll) );
	}
	
	@ParameterizedTest
	@CsvSource	( {
		"0",
		"-1",
	} )
	final void testAddAllAtPositionFailsUponNotPositiveIntegerInCollectionArgument(int intValue) {
		ArrayListPositive array = new ArrayListPositive();
		Collection<Integer> coll = Arrays.asList(new Integer(1), Integer.valueOf(intValue));
		assertThrows(NotPostivieArgumentException.class, () -> array.addAll(0, coll) );
	}
	
	@Test
	final void testReplaceAllAlwaysFailsAsUnsupportedOperation() {
		ArrayListPositive array = new ArrayListPositive();
		assertThrows(UnsupportedOperationException.class, () -> array.replaceAll(null) );
	}

	@Test
	final void testSetAtPositionFailsUponNullArgument() {
		ArrayListPositive array = new ArrayListPositive();
		assertThrows(NullArgumentException.class, () -> array.set(0, null) );
	}
	
	@ParameterizedTest
	@CsvSource	( {
		"0",
		"-1",
	} )
	final void testSetAtPositionFailsUponNonPositiveIntegerArgument(int intValue) {
		ArrayListPositive array = new ArrayListPositive();
		assertThrows(NotPostivieArgumentException.class, () -> array.set(0, Integer.valueOf(intValue) ) );
	}
}
