package ub004_overwrite;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.UnaryOperator;

public class ArrayListPositive extends ArrayList<Integer>{

	@Override
	public boolean add(Integer e) {
		checkElement(e);
		return super.add(e);
	}

	private void checkElement(Integer e) {
		if (e == null) {
			throw new NullArgumentException();
		}
		if (e < 1) {
			throw new NotPostivieArgumentException();
		}
	}
	
	@Override
	public void add(int index, Integer element) {
		checkElement(element);
		super.add(index, element);
	}
	
	@Override
	public boolean addAll(Collection<? extends Integer> c) {
		if (c == null) {
			throw new NullArgumentException();
		}
		for (Integer integer : c) {
			checkElement(integer);
		}
		return super.addAll(c);
	}
	
	@Override
	public boolean addAll(int index, Collection<? extends Integer> c) {
		if (c == null) {
			throw new NullArgumentException();
		}
		for (Integer integer : c) {
			checkElement(integer);
		}
		return super.addAll(index, c);
	}
	
	/*
	 * Will be implemented ...
	 */
	@Override
	public void replaceAll(UnaryOperator<Integer> operator) {
		throw new UnsupportedOperationException("Not yet implemented");
	}
	
	@Override
	public Integer set(int index, Integer element) {
		checkElement(element);
		return super.set(index, element);
	}
}
