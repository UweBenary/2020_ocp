package ub018_DequeTextProcessor.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ub018_DequeTextProcessor.TextProcessor;

class TextProcessorTest {

	
	@Test
	final void testToString() {
		final TextProcessor p = new TextProcessor();
		
		final String expected = ""; // START_STRING
		final String actual = p.toString();	
		assertEquals(expected, actual);
	}
	
	@Test
	final void testAdd() {
		final TextProcessor p = new TextProcessor();
		p.add("a");
		p.add("b");
		p.add("c");
		
		final String expected = "abc";
		final String actual = "abc";	
		assertEquals(expected, actual);
	}
	
	@Test
	final void testUndo() {
		final TextProcessor p = new TextProcessor();
		p.add("a");

		assertTrue( p.undo() );

		assertFalse( p.undo() );		
	}
	
	
	@Test
	final void testRedo() { //this test is awful
		final TextProcessor p = new TextProcessor();
		p.add("a");

		assertTrue( p.undo() );

		assertTrue( p.redo() );	
		assertFalse( p.redo() );	
		
		assertTrue( p.undo() );

		assertTrue( p.redo() );		
				
		assertTrue( p.undo() );
		assertFalse( p.undo() );

		assertTrue( p.redo() );		
	}

}
