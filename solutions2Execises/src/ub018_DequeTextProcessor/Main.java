package ub018_DequeTextProcessor;

public class Main {

	public static void main(String[] args) {
		
		/*
		 *  A1.
		 */
		System.out.println("*** A1");
		TextProcessor p = new TextProcessor();
		p.add("a");
		p.add("b");
		p.add("c");
		
		System.out.println(p); // abc

		/*
		 *  A2.
		 */
		System.out.println("*** A2");

		System.out.println( p.undo() ); // true
		System.out.println( p ); // ab
		
		System.out.println(" --- ");
		
		System.out.println( p.undo() ); // true
		System.out.println( p ); // a

		System.out.println(" --- ");
		
		System.out.println( p.undo() ); // true
		System.out.println( p ); //
		
		System.out.println(" --- ");
		
		System.out.println( p.undo() ); // false
		System.out.println( p ); // 
		
		/*
		 *  A3.
		 */
		System.out.println("*** A3");
		
		System.out.println( p.redo() ); // true
		System.out.println( p ); // a
		
		System.out.println(" --- ");
		
		System.out.println( p.redo() ); // true
		System.out.println( p ); // ab
		
		System.out.println(" --- ");
				
		System.out.println( p.redo() ); // true
		System.out.println( p ); // abc
		
		System.out.println(" --- ");
		
		System.out.println( p.redo() ); // false
		System.out.println( p ); // abc
		
		
	}

}
