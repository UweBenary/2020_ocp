package ub018_DequeTextProcessor;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.NoSuchElementException;

public class TextProcessor {
	
	private static final String START_STRING = "";
	
	private String text = START_STRING;
	
	private Deque<String> undoTexts = new ArrayDeque<>();
	private Deque<String> redoTexts = new ArrayDeque<>();
	
	@Override
	public String toString() {
		return text;
	}

	public void add(String string) {
		undoTexts.offerLast(text);
		text += string;		
	}
	
	public boolean undo() {
		if (undoTexts.isEmpty()) {
			return false;
		}

		redoTexts.offerLast(text);
		text = undoTexts.removeLast();

		return true;
	}
	
	public boolean redo() {
		if (redoTexts.isEmpty()) {
			return false;
		}

		undoTexts.offerLast(text);
		text = redoTexts.removeLast();

		return true;
	}

}
