package ub032_StreamReduceTextStatistics;


import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

import junit.extensions.TestSetup;

public class TextStatistics {


	private final String[] words;
	
	public TextStatistics(String text) {
		words = text.trim().split(" ");
	}
	
	public int getCountChars() {
		return (int) Arrays.stream(words)
					 .reduce(0, (i, s) -> i + s.length() , Integer::sum);
	}
	
	public int getCountSpecialChars() {
		return getCountChars() - getCountLetters();
	}
	
	public int getCountLetters() {
		return (int) Arrays.stream(words)
				 .<Character>flatMap( s -> s.chars().mapToObj( c -> (char) c) )
				 .filter( TextStatistics::isLetter )
				 .count();
	}

	private static boolean isLetter(Character c) {
//		return 		( c >= 'A' && c <= 'Z') 
//				 || ( c >= 'a' && c <= 'z') 
//				 || c == 'Ä' 
//				 || c == 'ä'
//				 || c == 'Ö'
//				 || c == 'ö'
//				 || c == 'Ü'
//				 || c == 'ü'
//				 || c == 'ß';
		return Character.isLetter(c);
	}
	
	public Optional<String> getLongestWord() {
		return Arrays.stream(words)
					 .max( Comparator.comparing(String::length) );
	}
	
	public void printStats() {
		System.out.println("Count chars: " + getCountChars() );
		System.out.println("Count special chars: " + getCountSpecialChars() );
		System.out.println("Count letters: " + getCountLetters() );
		System.out.println("Longest word: " + getLongestWord().orElseGet( ()->"No word found.") );
	}
	
}
