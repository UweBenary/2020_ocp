package ub032_StreamReduceTextStatistics.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ub032_StreamReduceTextStatistics.TextStatistics;

class TextStatisticsTest {

	private final TextStatistics stats = new TextStatistics("test - äh - this!");
	private final int expectedSpecialCharacterCount = 3;
	private final int expectedCharacterCount = 13;
	private final int expectedLetterCount = 10;
	private final String expectedLongestWord = "this!";
	
	@Test
	void testGetCountChars() {
		assertEquals(expectedCharacterCount, stats.getCountChars() );
	}

	@Test
	void testGetCountSpecialChars() {
		assertEquals(expectedSpecialCharacterCount, stats.getCountSpecialChars());
	}

	@Test
	void testGetCountLetters() {
		assertEquals(expectedLetterCount, stats.getCountLetters());
	}

	@Test
	void testGetLongestWord() {
		assertEquals(expectedLongestWord, stats.getLongestWord().get());
	}

}
