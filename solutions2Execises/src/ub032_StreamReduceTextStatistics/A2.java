package ub032_StreamReduceTextStatistics;

import java.util.Arrays;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

public class A2 {

	public static void main(String[] args) {

		String[] input = {
				"Hallo Welt!",
				"1 2 3",
				"Ene mene muh..."
		};
		
		
		TextStatistics identity = new TextStatistics("");
		
		BiFunction<TextStatistics, String, TextStatistics> accumulator = 
					(stats, str) -> {
						TextStatistics newStats = new TextStatistics(str);
						return new TextStatisticsDecorator(stats, newStats);
					};
				
		BinaryOperator<TextStatistics> combiner = TextStatisticsDecorator::new;
		
		TextStatistics stats = Arrays.stream(input)
									 .reduce(identity , accumulator, combiner );
		
		stats.printStats();
	}

}
