package ub032_StreamReduceTextStatistics;

import java.util.Optional;

public class TextStatisticsDecorator extends TextStatistics {

	private final TextStatistics stats1;
	private final TextStatistics stats2;

	public TextStatisticsDecorator(TextStatistics stats1, TextStatistics stats2) {
		super("Bad design. Should have been an interface");
		this.stats1 = stats1;
		this.stats2 = stats2;
	}

	@Override
	public int getCountChars() {
		return stats1.getCountChars() + stats2.getCountChars();
	}

	@Override
	public int getCountSpecialChars() {
		return stats1.getCountSpecialChars() + stats2.getCountSpecialChars();
	}

	@Override
	public int getCountLetters() {
		return stats1.getCountLetters() + stats2.getCountLetters();
	}

	@Override
	public Optional<String> getLongestWord() {
		String str1 = stats1.getLongestWord().orElse("");
		String str2 = stats2.getLongestWord().orElse(""); 
		
		if ( str1.length() > str2.length() ) {
			return stats1.getLongestWord();
		}
		
		return stats2.getLongestWord();
	}

	@Override
	public void printStats() {
		super.printStats();
	}
	
	
	
	
}
