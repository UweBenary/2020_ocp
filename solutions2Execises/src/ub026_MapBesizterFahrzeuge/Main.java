package ub026_MapBesizterFahrzeuge;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Main {

	public static void main(String[] args) {

		/*
		 * A1
		 */
		Person p1 = new Person("Uhn", "Owen");
		Person p2 = new Person("Al", "Coholic");
		Person p3 = new Person("Anita", "Bath");
		
		Vehicle v1 = new Vehicle("Golf", "VW");
		Vehicle v2 = new Vehicle("Z4", "BMW");
		Vehicle v3 = new Vehicle("A6", "Audi");
		Vehicle v4 = new Vehicle("Käfer", "VW");
		
		/*
		 * A2
		 */
		Map<Vehicle, Person> map = new HashMap<Vehicle, Person>();
		map.put(v1, p1);
		map.put(v2, p2);
		map.put(v3, p3);
		map.put(v4, p1);
		
		/*
		 * A3
		 */
		Person p = map.get(v1);
		System.out.println(p);
		
		/*
		 * A4
		 */
		Map<Person, Set<Vehicle>> map2 = new HashMap<Person, Set<Vehicle>>();
		
		for (Map.Entry<Vehicle, Person> entry : map.entrySet()) {
			
			Person person = entry.getValue();
			
			Set<Vehicle> vehicles = new HashSet<Vehicle>();
			vehicles.add(entry.getKey());
			
			map2.merge(person, vehicles, (list1, list2) -> { 
				
				Set<Vehicle> s = new HashSet<Vehicle>();
				s.addAll(list1);
				s.addAll(list2);
				return s;
			});
		}
		
		/*
		 * A5
		 */
		Set<Vehicle> v = map2.get(p1);
		System.out.println(v);
		
		/*
		 * A6
		 */
		print(map);
		print(map2);
	}

	private static <T,U> void print(Map<T, U> map) {
		System.out.println("-------------------");
		map.forEach((t,u)-> System.out.println( String.format("%-12s : %-10s", t.toString(), u.toString())) );
		
	}

}
