package ub026_MapBesizterFahrzeuge;

public class Vehicle {
	
	private String model;
	private String manufacturer;
	
	public Vehicle(String model, String manufacturer) {
		this.model = model;
		this.manufacturer = manufacturer;
	}	
	
	@Override
	public String toString() {
		return manufacturer + " " + model;
	}

	@Override
	public int hashCode() {
		int result = 0;
		result = 31 * result + model.hashCode();
		result = 31 * result + manufacturer.hashCode();
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Vehicle)) {
			return false;
		}
		Vehicle other = (Vehicle) obj;
		return model.equals(other.model) && manufacturer.equals(other.manufacturer);
	}
}
