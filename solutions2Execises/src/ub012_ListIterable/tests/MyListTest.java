package ub012_ListIterable.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ub012_ListIterable.MyList;

class MyListTest {

	final String expectedString = "test";
	
	@Test
	void testGet() {
		MyList list = new MyList(expectedString);
		String actual = list.get(0);
		assertEquals(expectedString, actual);
	}
	
	@Test
	void testGetThrowsIllegalArgumentExceptionUponEmptyIndex() {
		MyList list = new MyList(expectedString);
		assertThrows(IllegalArgumentException.class, () -> list.get(1) );
	}
	
	
	@Test
	void testGetThrowsIndexOutOfBoundsException() {
		MyList list = new MyList(expectedString);
		assertThrows(IndexOutOfBoundsException.class, () -> list.get(-1) );

		assertThrows(IndexOutOfBoundsException.class, () -> list.get(10) );

	}
	
	@Test
	void testAdd() {
		MyList list = new MyList("start");
		list.add(expectedString);
		String actual = list.get(1);
		assertEquals(expectedString, actual);		
	}

	@Test
	void testAddThorwsUnsupportedOperationExceptionUpon11Element() {
		MyList list = new MyList(expectedString);
		for (int i = 1; i < 10; i++) {
			list.add(expectedString);
		}
		assertThrows(UnsupportedOperationException.class, () -> list.add(expectedString) );
	}
	
	@Test
	void testSize() {
		MyList list = new MyList(expectedString);
		int expectedSize = 1;
		int actual = list.size();
		assertEquals(expectedSize, actual);
	}
	
	@Test
	void testForeach() {
		MyList list = new MyList("Start");
		for (int i = 1; i < 4; i++) {
			list.add(expectedString);
		}
		list.add("End");

		assertDoesNotThrow( () -> {
				for (String string : list) {
					System.out.println( string );
				}
			});		
	}
	
}
