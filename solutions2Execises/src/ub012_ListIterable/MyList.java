package ub012_ListIterable;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyList 
	implements Iterable<String> {

	
	private class Itr implements Iterator<String> {
	    int cursor;       // index of next element to return
	
	    // prevent creating a synthetic constructor
	    Itr() {}
		@Override
		public boolean hasNext() {
			return cursor != size();
		}
		
		@Override
		public String next() {
			int i = cursor;
		    if (i >= size() )
		        throw new NoSuchElementException();
		    cursor = i + 1;
		    return stringContainer[i];
		}
	}
	
	private final String[] stringContainer = new String[10];
	
	public MyList(String string) {
		stringContainer[0] = string;
	}
	
	public void add(String element) {
		for (int i = 0; i < stringContainer.length; i++) {
			if (stringContainer[i] == null) {
				stringContainer[i] = element;
				return;
			}
		}
		throw new UnsupportedOperationException("MyList filled. Cannot add '" + element + "'.");
	}
	
	public String get(int index) {
		if (stringContainer[index] == null) {
			throw new IllegalArgumentException("No element at index " + index);
		}
		return stringContainer[index];
	}
	
	public int size() {
		int count = 0;
		for (String string : stringContainer) {
			if (string != null) {
				count++;
			}
		}
		return count;
	}
	
	@Override
	public Iterator<String> iterator() {
		return new Itr();
	}

}
