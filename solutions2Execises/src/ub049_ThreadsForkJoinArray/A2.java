package ub049_ThreadsForkJoinArray;

import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
import java.util.stream.IntStream;

public class A2 {
	
	static class TaskCountNegatives extends RecursiveTask<Integer> {

		private final int[] array;
		private final int thisArrayLength;
		private static final int maxLength = 5;
		
		public TaskCountNegatives(int[] array) {
			this.array = array.clone();
			thisArrayLength = array.length;
		}

		@Override
		protected Integer compute() {

			if (thisArrayLength < maxLength) {
				return (int) IntStream.of(array)
							.filter( i -> i <0)
							.count();
			} else {
				TaskCountNegatives task1 = new TaskCountNegatives(subArray(0,thisArrayLength/2));
				TaskCountNegatives task2 = new TaskCountNegatives(subArray(thisArrayLength/2, thisArrayLength));
				
				task1.fork();
				
				return task2.compute() + task1.join();
			}

		}

		private int[] subArray(int from, int to) {
			return IntStream.of(array)
					.skip(from)
					.limit(to)
					.toArray();
		}
		
	}

	public static void main(String[] args) {

		int[] array = A1.getRandomArray(10);
		System.out.println(Arrays.toString(array));

		RecursiveTask<Integer> task = new TaskCountNegatives(array);
		ForkJoinPool pool = new ForkJoinPool();
		Integer result = pool.invoke(task);
		System.out.println(result);
		
	}

}
