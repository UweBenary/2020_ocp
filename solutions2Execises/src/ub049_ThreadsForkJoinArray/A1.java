package ub049_ThreadsForkJoinArray;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.stream.IntStream;

class TaskReplaceNegatives extends RecursiveAction {
			
	private int[] array;
	private int fromIdx, toIdx;
	private static final int maxLength = 5;
	
	public TaskReplaceNegatives(int[] array) {
		this(array, 0, array.length);		
	}
	
	private TaskReplaceNegatives(int[] array, int fromIdx, int toIdx) {
		this.fromIdx = fromIdx;
		this.toIdx = toIdx;		
		this.array = array;
	}


	@Override
	protected void compute() {
		
		if (toIdx - fromIdx < maxLength) {
			for (int i = fromIdx; i < toIdx; i++) {
				if (array[i] < 0) 
					array[i] = 0;
			}
			
		} else {			
			int center = (fromIdx + toIdx)/2;
			RecursiveAction task1 = new TaskReplaceNegatives(array, fromIdx, center);
			RecursiveAction task2 = new TaskReplaceNegatives(array, center, toIdx);
			invokeAll(task1, task2);
		}
						
	}
};

public class A1 {

	public static void main(String[] args) {

		int[] array = getRandomArray(10);
		System.out.println(Arrays.toString(array));
		
		RecursiveAction task = new TaskReplaceNegatives(array);
		
		ForkJoinPool pool = new ForkJoinPool();

		pool.invoke(task);
		
		System.out.println(Arrays.toString(array));
	}

	public static int[] getRandomArray(int arrayLength) {
		Random random = new Random();
		return  IntStream.generate( () -> random.nextInt(101) - 50 )
				.limit(arrayLength)
				.toArray();
	}

}
