package ub011_InnerClassBuilding.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ub011_InnerClassBuilding.Building;
import ub011_InnerClassBuilding.Building.Room;

class BuildingTest {

	@Test
	void testConstructorFailsWithLessthan1Floor() {
		int numberOfFloors = 0;
		assertThrows(IllegalArgumentException.class, 
				() -> new Building("street", 1, numberOfFloors, 1) );
	}
	
	@Test
	void testConstructorFailsWithLessthan1Room() {
		int numberOfRooms = 0;
		assertThrows(IllegalArgumentException.class, 
				() -> new Building("street", 1, 3, numberOfRooms) );
	}

	@Test
	void testGetFloor() {
		Building b = new Building("street", 1, 3, 1);
		assertTrue(b.getFloor(2) instanceof Building.Floor );
	}
	
	@Test
	void testGetRoom() {
		Building b = new Building("street", 1, 3, 2);
		assertTrue(b.getRoom(2,0) instanceof Building.Room );
	}
	
	@Test
	void testRoomToString() {
		Building gebaeude = new Building("Hauptstr.", 45, 3, 10);
		Room room02 = gebaeude.getRoom(0, 2);
		String expected = "Raum 0.2 / Hauptstr. 45";
		String actual = room02.toString();
		assertEquals(expected, actual);
	}
}
