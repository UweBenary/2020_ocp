package ub011_InnerClassBuilding;

import ub011_InnerClassBuilding.Building.Room;

public class Main {

	
	public static void main(String[] args) {
		
		Building gebaeude = new Building("Hauptstr.", 45, 3, 10);
		
		Room room02 = gebaeude.getRoom(0, 2);
		
		System.out.println(room02);
	}
	
}
