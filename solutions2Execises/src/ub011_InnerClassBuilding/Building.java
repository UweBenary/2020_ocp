package ub011_InnerClassBuilding;

import java.util.ArrayList;
import java.util.List;

public class Building {

	public class Floor {
		private Room[] rooms;
		
		Floor(int floorIndex, int numberOfRooms) {
			if (numberOfRooms < 1) {
				throw new IllegalArgumentException("Floor must have 1 or more rooms.");
			}			
			rooms = new Room[numberOfRooms];
			for (int roomIndex = 0; roomIndex < numberOfRooms; roomIndex++) {
				rooms[roomIndex] = new Room(floorIndex + "." + roomIndex);
			}
		}

		public Room getRoom(int roomIndex) {
			return rooms[roomIndex];
		}
	}
	
	public class Room {
		private String roomNumber;
		
		Room(String roomNumber) {
			this.roomNumber = roomNumber;
		}
		@Override
		public String toString() {
			return "Raum " + roomNumber + " / " + street + " " + number;
		}
	}
	
	private String street;
	private int number;
	private Floor[] floors;

	public Building(String street, int number, int numberOfFloors, int numberOfRoomsPerFloor) {
		if (numberOfFloors < 1) {
			throw new IllegalArgumentException("Building must have 1 or more floors.");
		}
		this.street = street;
		this.number = number;
		this.floors = new Floor[numberOfFloors];
		for (int floorIndex = 0; floorIndex < floors.length; floorIndex++) {
			floors[floorIndex] = new Floor(floorIndex, numberOfRoomsPerFloor);
		}
	}

	public Floor getFloor(int floorIndex) {		
		return floors[floorIndex];
	}

	public Room getRoom(int floorIndex, int roomIndex) {
		return floors[floorIndex].getRoom(roomIndex);
	}
}
