package ub014_ListTextStatistics;

import java.util.Collection;
import java.util.HashSet;

public class TextStatistics {

	private String text;
	
	protected TextStatistics(String text) { 
		this.text = text;
	};
	
	public String getText() {
		return text;
	}
	
	public static void main(String[] args) {
		
		TextStatistics stat = TextStatistics.of("Heute ist Montag!");
		
		Collection<Character> chars = stat.getUniqueChars();
		
		System.out.println(chars);
	}

	public Collection<Character> getUniqueChars() {
		Collection<Character> uniqueChars = new HashSet<Character>();
		for (int i = 0; i < text.length(); i++) {
			uniqueChars.add( text.charAt(i) );
		}
		return uniqueChars;
	}
	
	public static TextStatistics of(String string) {
		return new TextStatistics(string);
	}
	
}
