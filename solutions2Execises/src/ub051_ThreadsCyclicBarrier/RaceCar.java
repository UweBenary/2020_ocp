package ub051_ThreadsCyclicBarrier;

import java.util.List;
import java.util.Random;

public class RaceCar implements Runnable {
	private String name;
	private List<RaceCar> finish;
	
	public RaceCar(String name, List<RaceCar> finish) {
		this.name = name;
		this.finish = finish;
	}

	public void run() {
		System.out.println(name + " started.");
		
		try {
			Thread.sleep( (long) new Random().nextInt(100) );
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		synchronized (finish) {
			finish.add(this);
			System.out.println(name + " finished.");
		}
	}
	
	public String toString() {
		return name;
	}
}