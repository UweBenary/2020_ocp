package ub051_ThreadsCyclicBarrier;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class A2_CarRace {

	public static void main(String[] args) {

		List<RaceCar> finish = new ArrayList<RaceCar>();
		Runnable barrierAction = ()-> {
			System.out.println("Race Over");
			System.out.println(finish);
		};
			
		CyclicBarrier finishLine = new CyclicBarrier(4, barrierAction);
		
		Runnable race = () -> {
			RaceCar car = new RaceCar(Thread.currentThread().getName(), finish);
			car.run();
			try {
				finishLine.await();
			} catch (InterruptedException | BrokenBarrierException e) {
				e.printStackTrace();
			}
		};
		
		Thread car1 = new Thread(race, "car1");
		Thread car2 = new Thread(race, "car2");
		Thread car3 = new Thread(race, "car3");
		Thread car4 = new Thread(race, "car4");
		
		car1.start();
		car2.start();
		car3.start();
		car4.start();

	}

}
