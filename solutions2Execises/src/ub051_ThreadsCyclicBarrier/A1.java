package ub051_ThreadsCyclicBarrier;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicInteger;

public class A1 {

	public static void main(String[] args) {

		CyclicBarrier barrier = new CyclicBarrier(2, () -> System.out.println("passed") );
		
		AtomicInteger count = new AtomicInteger(0);
		
		Runnable target = () -> {
			for (int i = 0; i < 1_000_000; i++) {
				count.getAndIncrement();
			}
			
			try {
				barrier.await();
			} catch (InterruptedException | BrokenBarrierException e) {
				e.printStackTrace();
			}
			
			System.out.println(Thread.currentThread().getName() + " is done.");
			
		};
		
		Thread thread1 = new Thread(target, "Thread1"); 
		Thread thread2 = new Thread(target, "Thread2"); 
		
		thread1.start();
		thread2.start();
	}

}
