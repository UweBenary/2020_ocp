package ub008_StringTransformFunction;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.UnaryOperator;

public class StringTransform {

	private List<UnaryOperator<String>> transformations = new ArrayList<>();
	
	public StringTransform() {
		transformations.clear();
	}

	public String process(String string) {
		for (UnaryOperator<String> trafo : transformations) {
			string = (String) trafo.apply(string);
		}
		return string;
	}

	public StringTransform addTransformation(UnaryOperator<String> lambda) {
		transformations.add(lambda);
		return this;
	}
	
	@Override
	public String toString() {
		return transformations.toString();
	}


}
