package ub008_StringTransformFunction.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.function.UnaryOperator;

import org.junit.jupiter.api.Test;

import ub008_StringTransformFunction.StringTransform;

class StringTransformTest {

	
	final UnaryOperator<String> trafo1 =  s -> s.toUpperCase() ;
	final UnaryOperator<String> trafo2 =  s -> s + "!"  ;

	final StringTransform t1 = new StringTransform()
            .addTransformation( trafo1 )
            .addTransformation( trafo2 );
	
	@Test
	final void testAddTransformation() {

		
		String expected = "[" + trafo1.toString() + ", " + trafo2.toString() + "]";
		
        String actual = t1.toString();
		assertEquals(expected, actual);       
		
	}

	
	@Test
	final void testProcess() {
		
		String expected = "HALLO!";
        String actual = t1.process("Hallo");
		assertEquals(expected, actual);    
        
	}

}
