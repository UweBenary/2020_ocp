package ub008_StringTransformFunction;

public class Main {

	public static void main(String[] args) {
        
        // Transformationen vordefinieren:
        StringTransform t1 = new StringTransform()
                .addTransformation( s -> s.toUpperCase() )
                .addTransformation( s -> s + "!" );
        
        // Transformationen durchführen:
        String s = t1.process("Hallo");
        System.out.println(s); // HALLO!
        
        s = t1.process("Java ist toll");
        System.out.println(s); // JAVA IST TOLL!
        
        
        t1.addTransformation( x -> x.concat(" ~ ") + x.toLowerCase() );
        s = t1.process("Test");
        System.out.println(s); 
  
    }

}
