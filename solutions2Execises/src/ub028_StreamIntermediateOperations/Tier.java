package ub028_StreamIntermediateOperations;

public class Tier implements Comparable<Tier> {
	
	private String name;

	public Tier(String name) {
		this.name = name;
	}
	public String toString() {
		return "Tier " + name;
	}

	@Override
	public int compareTo(Tier o) {
		return name.compareTo(o.name);
	}
	
	@Override
	public boolean equals(Object obj) {
		if ( ! (obj instanceof Tier) ) {
			return false;
		}
		return name.equals( ((Tier) obj).name );
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}
}
