package ub028_StreamIntermediateOperations;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Exercises {

	public static void main(String[] args) {

	/*
	 * A1.
	 */
		System.out.println("*** A1");
		
		List<Integer> list = Arrays.asList(13, 15, 17, 19, 21);
		
		String result = exerciseA1(list) ;
		System.out.println(result);
		
	/*
	 * A2.
	 */
		System.out.println("*** A2");
		
		Integer[] array = { 1, 4, 7, 3, -8 };
		
		result = exerciseA2(array) ;
		System.out.println(result);
		
	/*
	 * A3.
	 */
		System.out.println("*** A3");
		
		List<String> listTier = Arrays.asList("Tom", "Jerry", "Rex");
		
		result = exerciseA3(listTier) ;
		System.out.println(result);
		
	/*
	 * A4.
	 */
		System.out.println("*** A4");
		
		result = exerciseA4() ;
		System.out.println(result);
		
	/*
	 * A5.
	 */
		System.out.println("*** A5");
		
		Tier[] tierArray = {
				new Tier("Rex"),
				new Tier("Tom"),
				new Tier("Jerry"),
				new Tier("Tom"),
				new Tier("Jerry"),
			};
		
		result = exerciseA5(tierArray) ;
		System.out.println(result);
	
	/*
	 * A6.
	 */
		System.out.println("*** A6");
		
		List<String> mailsErsthelfer = Arrays.asList(
				"tom@mycompany.com",
				"jerry@mycompany.com"
				);
		List<String> mailsIT = Arrays.asList(
				"tom@mycompany.com",
				"mary@mycompany.com"
				);
		List<String> mailsQM = Arrays.asList(
				"peter@mycompany.com",
				"jerry@mycompany.com"
				);
		
		Stream<List<String>> lists = Stream.of(mailsErsthelfer, mailsIT, mailsQM);
		result = exerciseA6(lists) ;
		System.out.println(result);
	}
	
	

	public static String exerciseA1(List<Integer> list) {
		List<String> result = list.stream()
			.filter( x -> (x==15 || x==19))
			.map(x->String.format("Treffer: %d", x))
			.collect(Collectors.toList());
		return result.toString();
	}
	
	public static String exerciseA2(Integer[] array) {
		return Arrays.stream(array)
			.map( x -> x%2==0 ? "gerade" : "ungerade")
			.collect(Collectors.toList())
			.toString();
	}
	
	public static String exerciseA3(List<String> listTier) {
		return listTier.stream()
				.map( x -> new Tier(x))
				.collect(Collectors.toList())
				.toString();
	}
	
	public static String exerciseA4() {
		Stream<Integer> input = Stream.generate( ()-> new Random().nextInt(41)-20 );
		Stream<Double> result = exerciseA4_DelNegReturnDouble(input);
		result = exerciseA4_Limit30(result);
		return result.collect(Collectors.toList())
				.toString();
	}
	
	public static String exerciseA5(Tier[] array) {
		return Arrays.stream(array)
				.distinct()
				.sorted()
				.collect(Collectors.toList())
				.toString();
	}
	
	public static String exerciseA6(Stream<List<String>> listStream) {
		List<String> streamResult = listStream.flatMap( (List<String> l) -> Stream.of(l.toArray(new String[] {}) ) )
				.map(s->s.substring(0, s.indexOf("@")) )
				.distinct()
				.collect(Collectors.toList());
		StringBuilder result = new StringBuilder();
		 for (String string : streamResult) {
			result.append(string + "\n");
		}
		return result.toString();
	}

	public static Stream<Double> exerciseA4_DelNegReturnDouble(Stream<Integer> input) {
		return input.filter(x -> !(x <= -10 & x >= -15) )
			.map( x -> (double) x);
	}

	public static Stream<Double> exerciseA4_Limit30(Stream<Double> input) {
		return input.limit(30);
	}
}
