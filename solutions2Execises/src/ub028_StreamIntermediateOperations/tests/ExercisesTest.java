package ub028_StreamIntermediateOperations.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import ub028_StreamIntermediateOperations.Exercises;
import ub028_StreamIntermediateOperations.Tier;

class ExercisesTest {

	@Test
	final void testExerciseA1() {
		final List<String> expectedList = new ArrayList<String>();
		
		List<Integer> list = Arrays.asList(13, 15, 17, 19, 21);
		// A
		for(Integer x : list) {
			if(x==15 || x==19) {
//				System.out.println("Treffer: " + x);
				expectedList.add("Treffer: " + x);
			}
		}
		// B
		
		final String expectedString = expectedList.toString();
		String actual = Exercises.exerciseA1(list);
		
		assertEquals(expectedString, actual);		
	}

	@Test
	final void testExerciseA2() {
		final List<String> expectedList = new ArrayList<String>();
		
		Integer[] array = { 1, 4, 7, 3, -8 };
		//A
		for (Integer x : array) {
//			System.out.println( x%2==0 ? "gerade" : "ungerade" );
			expectedList.add( x%2==0 ? "gerade" : "ungerade");
		}
		//B
		
		final String expectedString = expectedList.toString();
		String actual = Exercises.exerciseA2(array);
		
		assertEquals(expectedString, actual);		
	}

	@Test
	final void testExerciseA3() {
		final List<Tier> expectedList = new ArrayList<Tier>();
		
		List<String> list = Arrays.asList("Tom", "Jerry", "Rex");
		
		//A
		for(String name : list) {
			Tier t = new Tier(name);
//			System.out.println(t);
			expectedList.add( t );
		}
		//B
		
		final String expectedString = expectedList.toString();
		String actual = Exercises.exerciseA3(list);
		
		assertEquals(expectedString, actual);	
	}

	@Test
	final void testExerciseA4_DelNegReturnDouble() {
		final String expectedString = Arrays.asList(10.0, 1.0,-3.0, 0.0).toString();
		
		Stream<Integer> input = Stream.of(10, 1, -3, 0, -13);
		Stream<Double> result = Exercises.exerciseA4_DelNegReturnDouble(input);
		
		Collector<Double, ?, List<Double>> collector = Collectors.toList();
		String actual = result.collect(collector).toString();

		assertEquals(expectedString, actual);			
	}
	
	@Test
	final void testExerciseA4_Size30() {
		final long expected = 30;
		
		Stream<Double> input = Stream.generate( ()->1.);
		Stream<Double> result = Exercises.exerciseA4_Limit30(input);
		
		long actual = result.count();
		
		assertEquals(expected, actual);			
	}

	@Test
	final void testExerciseA5() {
		
		Tier[] array = {
				new Tier("Rex"),
				new Tier("Tom"),
				new Tier("Jerry"),
				new Tier("Tom"),
				new Tier("Jerry"),
			};
		
		final Set<Tier> expectedSet = new TreeSet<>(Arrays.asList(array));
		final String expected = expectedSet.toString();
		
		String actual = Exercises.exerciseA5(array);
		
		assertEquals(expected, actual);
	}

	@Test
	final void testExerciseA6() {
		
		final String expectedString = "tom\njerry\nmary\npeter\n";
		
		List<String> mailsErsthelfer = Arrays.asList(
				"tom@mycompany.com",
				"jerry@mycompany.com"
				);
		List<String> mailsIT = Arrays.asList(
				"tom@mycompany.com",
				"mary@mycompany.com"
				);
		List<String> mailsQM = Arrays.asList(
				"peter@mycompany.com",
				"jerry@mycompany.com"
				);
		
		Stream<List<String>> lists = Stream.of(mailsErsthelfer, mailsIT, mailsQM);
		
		String actual = Exercises.exerciseA6(lists);
		
		assertEquals(expectedString, actual);
	}

}
