package ub007_InterfacesStringTransform.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ub007_InterfacesStringTransform.A1;

class A1Test {

	@Test
	void testTransform() {
		String expected = "[MO, DI, MI]";
		String actual = A1.transform(new String[] {"mo", "di", "mi"}).toString();
		assertEquals(expected, actual);
	}

	@Test
	void testTransformFailsWithNull() {
		assertThrows(NullPointerException.class, () -> A1.transform(null));
	}
}
