package ub007_InterfacesStringTransform.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ub007_InterfacesStringTransform.A3;
import ub007_InterfacesStringTransform.A4;

class A4Test {

	@Test
	void testTransform() {
		String expected = "[mo(2), di(2), mi(2)]";
		A4 a4 = new A4();
		String actual = a4.transform(new String[] {"mo", "di", "mi"}).toString();
		assertEquals(expected, actual);
	}

}