package ub007_InterfacesStringTransform.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ub007_InterfacesStringTransform.A2;
import ub007_InterfacesStringTransform.A3;

class A3Test {

	@Test
	void testTransform() {
		String expected = "[mo., di., mi.]";
		A3 a3 = new A3();
		String actual = a3.transform(new String[] {"mo", "di", "mi"}).toString();
		assertEquals(expected, actual);
	}

}
