package ub007_InterfacesStringTransform.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ub007_InterfacesStringTransform.A2;

class A2Test {

	@Test
	void testTransform() {
		String expected = "[mi, di, mo]";
		A2 a2 = new A2();
		String actual = a2.transform(new String[] {"mo", "di", "mi"}).toString();
		assertEquals(expected, actual);
	}

}
