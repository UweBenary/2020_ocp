package ub007_InterfacesStringTransform.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.function.UnaryOperator;

import org.junit.jupiter.api.Test;

import ub007_InterfacesStringTransform.A5;

class A5Test {

	@Test
	void testTransform() {
		String expected = "[mo(2), di(2), mi(2)]";
		String[] argument1 = {"mo", "di", "mi"};
		UnaryOperator<String> argument2 = s -> s + "(2)";
		String actual = A5.transform(argument1, argument2 ).toString();
		assertEquals(expected, actual);
	}

}
