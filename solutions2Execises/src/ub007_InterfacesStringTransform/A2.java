package ub007_InterfacesStringTransform;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class A2 
	implements StringTransformable {

	@Override
	public ArrayList<String> transform(String[] stringArray) {
		ArrayList<String> results = new ArrayList<String>();
		for (int i = stringArray.length - 1; i > -1 ; i--) {
				results.add(stringArray[i]);
		}
		return results;
	}

}
