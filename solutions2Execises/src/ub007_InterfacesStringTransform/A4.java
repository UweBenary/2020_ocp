package ub007_InterfacesStringTransform;

import java.util.ArrayList;

public class A4 
	implements StringTransformable {

	@Override
	public ArrayList<String> transform(String[] stringArray) {
		ArrayList<String> result = new ArrayList<String>();
		for (String string : stringArray) {
			result.add( string + "(" + string.length() + ")" ); 
		}
		return result;
	}

}
