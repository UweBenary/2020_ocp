package ub007_InterfacesStringTransform;

import java.util.ArrayList;
import java.util.List;

public class A1 {

	public static ArrayList<String> transform(String[] strings) {
		ArrayList<String> result = new ArrayList<String>();
		for (String string : strings) {
			result.add( string.toUpperCase() );
		}
		return result;
	}

}
