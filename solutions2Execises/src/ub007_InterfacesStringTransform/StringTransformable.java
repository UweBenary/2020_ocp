package ub007_InterfacesStringTransform;

import java.util.ArrayList;

public interface StringTransformable {
	ArrayList<String> transform(String[] stringArray);
}
