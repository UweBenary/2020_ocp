package ub007_InterfacesStringTransform;

import java.util.ArrayList;
import java.util.List;
import java.util.function.UnaryOperator;

public class A5 {

	public static List<String> transform(String[] arr, UnaryOperator<String> logic) {
		List<String> result = new ArrayList<>();
		for (String string : arr) {
			result.add( logic.apply(string) );
		}
		return result;		
	}
}
