package ub001_QuizzTypesWithBuilder;

import ub001_QuizTypes.Answer;
import ub001_QuizTypes.ValidText;

public final class Question {
	
	private final String questionText;
	private final String[] topics; 
	private final Answer[] answers; 
	private final String[] comments;
	
	public Question(String questionText, Answer[] answers, String[] topics, String[] comments) {
		if (questionText == null || topics == null || answers == null) {
			throw new IllegalArgumentException("Question contructor: null is no valid argument.");
		}
		this.questionText = questionText;
		this.topics = topics;
		this.answers = answers;
		this.comments = comments;
	}

}
