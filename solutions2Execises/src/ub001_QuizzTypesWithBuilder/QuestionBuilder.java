package ub001_QuizzTypesWithBuilder;

import java.util.ArrayList;
import java.util.List;

import ub001_QuizTypes.Answer;
import ub001_QuizTypes.ValidText;

public class QuestionBuilder {
	
	private ValidText question = null;
	private final List<Answer> answers = new ArrayList<>(); 
	private final List<ValidText> topics = new ArrayList<>();
	private final List<ValidText> comments = new ArrayList<>();
	
	public boolean setQuestionText (ValidText questionText) {
		if ( question == null ) {
			question = questionText;
			return true;
		}
		return false;
	}
	
	public boolean addAnswer(Answer answer) {
		if ( ! answers.contains(answer) ) {
			answers.add(answer);
			return true;
		}
		return false;
	}
		
	public boolean addTopic(ValidText topic) {
		if ( ! topics.contains(topic) ) {
			topics.add(topic);
			return true;
		}
		return false;
	}
	
	public boolean addComment(ValidText comment) {
		if ( ! comments.contains(comment) ) {
			comments.add(comment);
			return true;
		}
		return false;
	}
	

	public Object getQuestion() {
		if ( question == null ) {
			return null;
		}
		if (answers.size() < 2) {
			return null;
		}
		if( ! hasCorrectAnswer() ) {
			return null;
		}
		if (topics.size() < 1) {
			return null;
		}
		String questionText = question.toString();
		Answer[] answerArray = answers.toArray( new Answer[] {} );
		String[] topicArray = topics.toArray(new String[] {});
		String[] commentArray = {"None."};
		if ( comments.size() > 0 ) {
			commentArray = comments.toArray( new String[] {} );
		}
		return new Question(questionText, answerArray, topicArray, commentArray);
	}
	
	private boolean hasCorrectAnswer ( ) {
		if (answers.size() == 0) {
			return false;
		}
		for (Answer answer : answers) {
			if (answer.isCorrect()) {
				return true;
			}
		}
		return false;
	}
}
