package ub001_QuizzTypesWithBuilder.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ub001_QuizTypes.Answer;
import ub001_QuizTypes.ValidText;
import ub001_QuizzTypesWithBuilder.QuestionBuilder;

class QuestionBuilderTest {

	@Test
	final void testGetQuestion() {
		ValidText question = new ValidText("?");
		Answer answer = new Answer(new ValidText("answer1"), true);
		Answer answer2 = new Answer(new ValidText("answer2"), false);
		ValidText topic = new ValidText("topic");
		
		 
		QuestionBuilder qBuilder = new QuestionBuilder();
		qBuilder.setQuestionText(question);
		qBuilder.addAnswer(answer);
		qBuilder.addAnswer(answer2);
		qBuilder.addTopic(topic);		
		assertNotNull( qBuilder.getQuestion() );
	}


}
