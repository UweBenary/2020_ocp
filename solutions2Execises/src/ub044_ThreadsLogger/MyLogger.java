package ub044_ThreadsLogger;

public class MyLogger {
	
	private StringBuilder sb = new StringBuilder();
//	private StringBuffer sb = new StringBuffer(); // reicht nicht
	
	
	public void addMessage(String caller, String message) {
		synchronized (this) { // sb ginge auch
			sb.append(caller)
			  .append(" - ")
			  .append(message)
			  .append("\n");
		}
	}
	
	public String getLog() {
		return sb.toString();
	}
	
}
