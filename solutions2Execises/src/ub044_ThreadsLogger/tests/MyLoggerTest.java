package ub044_ThreadsLogger.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import ub044_ThreadsLogger.MyLogger;

class MyLoggerTest {

	@Test
	void testGetLog() {
		MyLogger logger = new MyLogger();
		String caller = "Wolk";
		String message = "Nu pogodi!";
		logger.addMessage(caller, message);
		
		String expected = caller + " - " + message + "\n";
		String actual = logger.getLog();
		
		assertEquals(expected, actual);
	}
	
	@Test
	void testGetLogInThreads() {
		MyLogger logger = new MyLogger();
		
		Runnable writeMessage1 = () -> {
			for (int i = 0; i < 100; i++) {
				logger.addMessage("Wolk", "Nu pogodi!");
			}
		};
		
		Runnable writeMessage2 = () -> {
			for (int i = 0; i < 100; i++) {
				logger.addMessage("Sajaz", "..." );	
			}		
		};
		
		List<Thread> list = new ArrayList<Thread>();
		
		for (int i = 0; i < 10; i++) {
			list.add(new Thread(writeMessage1));
			list.add(new Thread(writeMessage2));
		}
		
		/*
		 * wenn die 2 folgenden Schleifen in eine zusammengefügt werden, kommt es nicht (verlässlich) zur RaceCondition
		 */
		for (Thread thread : list) {
			thread.start();
		}
		
		for (Thread thread : list) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.getStackTrace();
			}
		}
		
		assertTrue(isValid(logger.getLog()));
	}
	
	@Test
	void testGetLogInThreads2() {
		
		class MyMessageWriter extends Thread {
			MyLogger logger;
			String caller;
			String message;
			
			public MyMessageWriter(MyLogger logger, String caller, String message) {
				this.logger = logger;
				this.caller = caller;
				this.message = message;
			}
			
			@Override
			public void run() {
				for (int i = 0; i < 100; i++) {
					logger.addMessage(caller, message);
				}
			}
		}

		MyLogger logger = new MyLogger();
		
		List<Thread> list = new ArrayList<Thread>();
		for (int i = 0; i < 10; i++) {
			list.add(new MyMessageWriter(logger,"Wolk", "Nu pogodi!"));
			list.add(new MyMessageWriter(logger, "Sajaz", "..."));
		}
		
		for (Thread thread : list) {
			thread.start();
		}

		for (Thread thread : list) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.getStackTrace();
			}
		}
		
		assertTrue(isValid(logger.getLog()));
	}

	@Test
	void testIsValid() {
		assertTrue(isValid("Wolk - Nu pogodi!\nSajaz - ...\n"));
	}
	
	private boolean isValid(String log) {
		String[] messages = log.split("\n");
		for (String string : messages) {
			if ( ! ( string.equals("Wolk - Nu pogodi!") || string.equals("Sajaz - ...") ) ) 
				return false;
		}
		return true;
	}

}
