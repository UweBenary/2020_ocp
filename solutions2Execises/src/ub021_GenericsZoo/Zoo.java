package ub021_GenericsZoo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Zoo {
	
	private Collection<Tier> animals = new ArrayList<Tier>();
	
	public void addAnimal(Tier animal) {
		animals.add(animal);
	}
	
	public Collection<Tier> getTiere() {
		return animals;
	}

}
