package ub021_GenericsZoo.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ub021_GenericsZoo.Mensch;

class MenschTest {

	@Test
	final void testSetGesund() {

		Mensch m = new Mensch();
		m.setGesund(true);
		assertTrue( m.isGesund() );
	}

}
