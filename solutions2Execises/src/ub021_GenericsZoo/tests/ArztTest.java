package ub021_GenericsZoo.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ub021_GenericsZoo.Affe;
import ub021_GenericsZoo.Arzt;
import ub021_GenericsZoo.KannBehandeltWerden;
import ub021_GenericsZoo.Mensch;
import ub021_GenericsZoo.Tier;
import ub021_GenericsZoo.Zebra;

class ArztTest {

	@Test
	final void testBehandeltNurAffen() {
		Arzt<Affe> affenArzt = new Arzt<>();
		
		Affe affe = new Affe();
		Zebra zebra = new Zebra();
		
		Tier affenTier = new Affe();
		
		Mensch arzt = new Arzt<KannBehandeltWerden>();
		
		affenArzt.behandelt(affe);
//		affenArzt.behandelt(zebra);
//		affenArzt.behandelt(affenTier);
//		affenArzt.behandelt(arzt);
//		affenArzt.behandelt(affenArzt);		
	}
	
	@Test
	final void testBehandeltNurTiere() {
		Arzt<Tier> tierArzt = new Arzt<>();
		
		Affe affe = new Affe();
		Zebra zebra = new Zebra();
		
		Tier affenTier = new Affe();
		
		Mensch arzt = new Arzt<KannBehandeltWerden>();
		
		tierArzt.behandelt(affe);
		tierArzt.behandelt(zebra);
		tierArzt.behandelt(affenTier);
//		tierArzt.behandelt(arzt);
//		tierArzt.behandelt(tierArzt);		
	}
	
	@Test
	final void testBehandeltAlle() {
		Arzt<KannBehandeltWerden> omnipotentArzt = new Arzt<>();
		
		Affe affe = new Affe();
		Zebra zebra = new Zebra();
		
		Tier affenTier = new Affe();
		
		Mensch arzt = new Arzt<KannBehandeltWerden>();
		
		omnipotentArzt.behandelt(affe);
		omnipotentArzt.behandelt(zebra);
		omnipotentArzt.behandelt(affenTier);
		omnipotentArzt.behandelt(arzt);
		omnipotentArzt.behandelt(omnipotentArzt);		
	}

}
