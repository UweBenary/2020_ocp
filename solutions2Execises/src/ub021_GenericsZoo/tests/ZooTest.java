package ub021_GenericsZoo.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.Test;

import ub021_GenericsZoo.Affe;
import ub021_GenericsZoo.Tier;
import ub021_GenericsZoo.Zebra;
import ub021_GenericsZoo.Zoo;

class ZooTest {

	@Test
	final void test() {
		Zoo zoo = new Zoo();
		
		Tier affe = new Affe();
		zoo.addAnimal(affe);
		
		Tier zebra = new Zebra();
		zoo.addAnimal(zebra);
		
		final Collection<Tier> expected = new ArrayList<Tier>();
		expected.add(affe);
		expected.add(zebra);
		
		final Collection<Tier> actual = zoo.getTiere();
		
		assertEquals(expected, actual);
	}

}
