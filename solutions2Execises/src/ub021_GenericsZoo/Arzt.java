package ub021_GenericsZoo;

public class Arzt <T extends KannBehandeltWerden> 
	extends Mensch {
	
	public void behandelt(T t) {
		if ( ! t.isGesund() ) {
			t.setGesund(true);
		}
	}

}
