package ub023_GenericsGenerischeMethodenFunctional.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

import org.junit.jupiter.api.Test;

import ub023_GenericsGenerischeMethodenFunctional.A3;

class A3Test {

	@Test
	final void testBuildWithStringArrayList() {
		String[] values = {"", null, "Hallo", "3"};
		Supplier<List<String>> sup = () -> new ArrayList<String>( values .length);
		
		List<String> expected = new ArrayList<>( Arrays.asList( values ) );
		List<String> actual = A3.build(sup, values);
		
		assertIterableEquals(expected, actual);
	}

}
