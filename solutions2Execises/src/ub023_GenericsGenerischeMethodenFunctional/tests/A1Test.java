package ub023_GenericsGenerischeMethodenFunctional.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import org.junit.Ignore;
import org.junit.jupiter.api.Test;

import ub023_GenericsGenerischeMethodenFunctional.A1;

class A1Test {

	@Test
	final void testCountWithIntegerList() {
		
		List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
		Predicate<Integer> pred = i -> i % 2 == 0;
		
		final int expected = 2;
		final int actual = A1.count(list, pred);  
		
		assertEquals(expected, actual);
	}
	
	@Test
	final void testCountWithNumberList() {		
		
		List<Number> list = Arrays.asList(1, 1.2, 3, 3.4);
		Predicate<Number> pred = b -> b.intValue() % 2 != 0;
		
		final int expected = 4;
		final int actual = A1.count(list, pred);  
		
		assertEquals(expected, actual);
	}
	
	@Test
	final void testCountWithStringList() {		
		
		List<String> list = Arrays.asList("a", "", "b", "", "c");
		Predicate<String> pred = String::isEmpty;
		
		final int expected = 2;
		final int actual = A1.count(list, pred);  
		
		assertEquals(expected, actual);
	}

}
