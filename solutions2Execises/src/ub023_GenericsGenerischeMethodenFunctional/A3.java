package ub023_GenericsGenerischeMethodenFunctional;

import java.util.Collection;
import java.util.function.Supplier;

public class A3 {

	/**
	 * Function takes values of type A and a collection of type A provided by the supplier. 
	 * The collection is then populated with the values.  
	 * The populated collection is returned.
	 */
	@SafeVarargs
	public static <A, T extends Collection<A>> T build( Supplier<T> s, A... values ) {
						
		T c = s.get();
		for (A a : values) {
			c.add(a);
		}
		return c;
	}
	
}
