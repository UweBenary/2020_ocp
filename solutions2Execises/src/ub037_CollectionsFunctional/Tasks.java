package ub037_CollectionsFunctional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Tasks {

	public static void main(String[] args) {

		int count = 0;
		System.out.println("*** A" + ++count);
		a1();
		System.out.println("*** A" + ++count);
		a2();
		System.out.println("*** A" + ++count);
		a3();
		System.out.println("*** A" + ++count);
		a4i();
		a4ii();
		a4iii();
		a4iv();
		a4v();
		System.out.println("*** A" + ++count);
		a5();
		System.out.println("*** A" + ++count);
		a6(); // ????
	}
	
	private static void a6() {
		Collection<Integer> coll = new ArrayList<Integer>() ;
		coll.stream();		
	}

	private static void a5() {
		List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));
		int result = list.stream().mapToInt(Integer::intValue).sum();
		System.out.println(result);
		
		list.replaceAll(x->x==3?null:x);
		result = list.stream().filter(x->x!=null).reduce(0, (x,y) -> x+y );
		System.out.println(result);
	}

	private static void a4v() {
		List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));
		
		
		Comparator<Integer> r2 = new Comparator<Integer> () {
			@Override
			public int compare(Integer o1, Integer o2) {
				return o2-o1;
			}			
		};
		list.sort( r2::compare );
		
		System.out.println(list);		
		
	}
	
	private static void a4iv() {
		List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));
		
		class RevOrder2 implements Comparator<Integer> {
			@Override
			public int compare(Integer o1, Integer o2) {
				return o2-o1;
			}
		}
		RevOrder2 r2 = new RevOrder2();
		list.sort( r2::compare );
		
		System.out.println(list);		
		
	}
	
	public static class RevOrder implements Comparator<Integer> {
		@Override
		public int compare(Integer o1, Integer o2) {
			return o2-o1;
		}
	}
	
	private static void a4iii() {
		List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));
		
		RevOrder r = new Tasks.RevOrder();
		list.sort( r::compare );
		
		System.out.println(list);		
	}
	
	private static void a4ii() {
		List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));
		list.sort( (x,y) -> y-x);
		System.out.println(list);		
		
	}

	private static void a4i() {
		List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));
		list.sort( new Comparator<Integer> () {

			@Override
			public int compare(Integer o1, Integer o2) {
				return o2-o1;
			}
			
		});
		System.out.println(list);		
		
	}

	private static void a3() {
		List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));
		list.replaceAll( x -> x%2==1 ? 0 : x);
		System.out.println(list);		
	}

	private static void a2() {
//		List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));
		List<Integer> list = Stream.iterate(1, x->x+1).limit(8).collect(Collectors.toCollection(ArrayList::new));
		list.removeIf(x-> x%2==1);
		System.out.println(list);
	}

	private static void a1() {
		Collection<Integer> coll = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
		coll.forEach( System.out::println );
		
	}

}
