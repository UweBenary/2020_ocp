package ub036_StreamCollectCollectorsAutos;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;


public class Main {

	public static void main(String[] args) {
		
		List<Auto> autos = Arrays.asList(
				new Auto("VW", "Golf"),
				new Auto("VW", "Polo"),
				new Auto("Opel", "Corsa"),
				new Auto("Opel", "Astra")
			);
		
		/*
		 * A1
		 */
		Set<String> set = a1(autos);
		System.out.println(set); // mögliche Ausgabe: [VW, Opel]
		
		/*
		 * A2
		 */
		Map<String, List<Auto>> map =  a2(autos);
		System.out.println(map); 
		// mögliche Ausgabe: {VW=[VW/Golf, VW/Polo], Opel=[Opel/Corsa, Opel/Astra]}

		/*
		 * A3
		 */
		Map<String, List<String>> map3 = a3(autos);
		System.out.println(map3); 
		// mögliche Ausgabe: {VW=[Golf, Polo], Opel=[Corsa, Astra]}
		
		/*
		 * A4
		 */
		Map<String, List<Auto>> map4 = a4(autos);
		System.out.println(map4); 
		// Ausgabe: {Opel=[Opel/Corsa, Opel/Astra], VW=[VW/Golf, VW/Polo]}
		
		/*
		 * A5
		 */
		Map<Boolean, List<Auto>> map5 = a5(autos);
		System.out.println(map5); 
		// Ausgabe: {Opel=[Opel/Corsa, Opel/Astra], VW=[VW/Golf, VW/Polo]}
	}

	public static Map<Boolean, List<Auto>> a5(List<Auto> autos) {
		Predicate<Auto> predicate = auto -> auto.getModell().contains("o");
		
//		Collector<Auto, ?, List<Auto>> downstream = Collectors.toList();
		
//		Collector<Auto, ?, Map<Boolean, List<Auto>>> collector = Collectors.partitioningBy(predicate, downstream);
		Collector<Auto, ?, Map<Boolean, List<Auto>>> collector = Collectors.partitioningBy(predicate);
		return autos.stream().collect(collector);
	}

	public static Map<String, List<Auto>> a4(List<Auto> autos) {
		Function<Auto, String> classifier = Auto::getHersteller;
		Supplier<TreeMap<String, List<Auto>>> mapFactory = TreeMap::new;

		Collector<Auto, ?, List<Auto>> downstream = Collectors.toList();

		Collector<Auto, ?, TreeMap<String, List<Auto>>> collector = Collectors.groupingBy(classifier, mapFactory, downstream);
		return autos.stream().collect(collector);
	}

	public static Map<String, List<String>> a3(List<Auto> autos) {
		Function<Auto, String> classifier = Auto::getHersteller;
		
		Function<Auto, String> mapper = Auto::getModell;
		
		Collector<Auto, ?, List<String>> downstream = Collectors.mapping(mapper, Collectors.toList());
		
		Collector<Auto, ?, Map<String, List<String>>> collector = Collectors.groupingBy(classifier, downstream);
		return autos.stream().collect(collector);
	}

	public static Map<String, List<Auto>> a2(List<Auto> autos) {
		Function<Auto, String> classifier = Auto::getHersteller;
		Collector<Auto, ?, Map<String, List<Auto>>> collector = Collectors.groupingBy(classifier);
		return autos.stream().collect(collector);
	}

	public static Set<String> a1(List<Auto> autos) {
		Collector<String, ?, Set<String>> downstream = Collectors.toSet();
		
		Function<Auto, String> mapper = Auto::getHersteller;
		Collector<Auto, ?, Set<String>> collector 
				= Collectors.mapping(mapper, downstream);

		return autos.stream().collect(collector);
	}

}
