package ub036_StreamCollectCollectorsAutos.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.jupiter.api.Test;

import ub036_StreamCollectCollectorsAutos.Auto;
import ub036_StreamCollectCollectorsAutos.Main;

class MainTest {

	private final 	List<Auto> autos = Arrays.asList(
			new Auto("VW", "Golf"),
			new Auto("VW", "Polo"),
			new Auto("Opel", "Corsa"),
			new Auto("Opel", "Astra")
		);
	
	@Test
	final void testA5falsePart() {
		final String expectedAutoOfFalse = "[Opel/Astra]";
		
		Map<Boolean, List<Auto>> map = Main.a5(autos);
		String actualAutoOfFalse = map.get(false).toString();
		
		assertEquals(expectedAutoOfFalse, actualAutoOfFalse);
	}
	
	@Test
	final void testA5truePart() {
		final int expectedSizeOfTrue = 3;
		
		Map<Boolean, List<Auto>> map = Main.a5(autos);
		int actualSizeOfTrue = map.get(true).size();
		
		assertEquals(expectedSizeOfTrue, actualSizeOfTrue);
	}
	
	@Test
	final void testA4() {
		final String expected = "{Opel=[Opel/Corsa, Opel/Astra], VW=[VW/Golf, VW/Polo]}";
		
		Map<String, List<Auto>> map = Main.a4(autos);
		String actual = map.toString();
		
		assertEquals(expected, actual);
	}

	@Test
	final void testA3() {
		final String expected = "{VW=[Golf, Polo], Opel=[Corsa, Astra]}";
		
		Map<String, List<String>> map = Main.a3(autos);
		String actual = map.toString();
		
		assertEquals(expected, actual);
	}

	@Test
	final void testA2() {
		final String expected = "{VW=[VW/Golf, VW/Polo], Opel=[Opel/Corsa, Opel/Astra]}";
		
		Map<String, List<Auto>> map = Main.a2(autos);
		String actual = map.toString();
		
		assertEquals(expected, actual);
	}

	@Test
	final void testA1() {
		final String expected = "[VW, Opel]";
		
		Set<String> set = Main.a1(autos);
		String actual = set.toString();
		
		assertEquals(expected, actual);
	}

}
