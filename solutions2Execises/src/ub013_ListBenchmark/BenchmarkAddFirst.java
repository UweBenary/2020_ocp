package ub013_ListBenchmark;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class BenchmarkAddFirst {

	public static void main(String[] args) {

		int n = 50_000;
				
		String[] array = getStringArray(n);		
		
		int m = 100;

		long[] warmup;
		List<String> list;
		
		
		list = new LinkedList<>();
		warmup = callTestAddFirst(m, array, list);	
		System.out.println(warmup.length);
		long[] runTimesLinkedList = callTestAddFirst(m, array, list);		
		
		
		list = new ArrayList<>();
		warmup = callTestAddFirst(m, array, list);	
		System.out.println(warmup.length);
		long[] runTimesArrayList = callTestAddFirst(m, array, list);	

		System.out.println("********  ArrayList");
		printStats(runTimesArrayList);
		System.out.println("********  LinkedList");
		printStats(runTimesLinkedList);
	}

	private static void printStats(long[] times) {
		String[] titles = {
				"Number of envokations:", 
				"Total run time (ms):", 
				"Average run time (ms):",
				"Median run time (ms):",				 
				"Maximal run time (ms):", 
				"Minimal run time (ms):"};
		double[] stats = {
				times.length,
				sum(times),
				sum(times)/times.length,
				median(times),
				max(times),
				min(times),
		};

		for (int i = 0; i < titles.length; i++) {
			System.out.format("%-25s %5.1e %n", titles[i], stats[i]);
		}
		
	}

	private static double median(long[] times) {
		Arrays.sort(times);
		return (double) times[Math.floorDiv(times.length-1, 2)];
	}
	
	private static double max(long[] times) {
		Arrays.sort(times);
		return (double) times[times.length-1];
	}
	
	private static double min(long[] times) {
		Arrays.sort(times);
		return (double) times[0];
	}

	private static double sum(long[] times) {
		double sum = 0;
		for (int i = 0; i < times.length; i++) {
			sum += times[i];
		}
		return sum;
	}

	private static long[] callTestAddFirst(int m, String[] array, List<String> list) {
		long[] runTime = new long[m];
		long start, end;
		for (int i = 0; i < m; i++) {
			start = System.currentTimeMillis();
			testAddFirst(array, list);
			end = System.currentTimeMillis();
			runTime[i] = end - start;
		}
		return runTime;
	}

	private static String[] getStringArray(int n) {
		List<String> list = new ArrayList<>();
		for (int i = 0; i < n; i++) {
			list.add("Test");
		}
		return list.toArray(new String[0]);
	}
	
	public static void testAddFirst(String[] array, List<String> list) {
		for (String element : array) {
			list.add(0, element);
		}
		System.out.println(list.get(new Random().nextInt(list.size())) );
		list.clear();
	}
}
