package ub001_QuizTypes;

import java.util.ArrayList;
import java.util.List;

public final class Question {
	
	private final ValidText text;
	
	private final List<ValidText> topics = new ArrayList<>(); 
	private final List<Answer> answers = new ArrayList<>(); 
	private final List<ValidText> comments = new ArrayList<>();
	
	
	public Question(ValidText questionText, Answer correctAnswer, Answer incorrectAnswer) {
		if (questionText == null) {
			throw new IllegalArgumentException("answerText = null is no valid value of Question(questionText, Answer...)");
		}

		if (correctAnswer == null) {
			throw new IllegalArgumentException("correctAnswer = null is no valid value of Question(questionText, Answer...)");
		}
		
		if ( ! correctAnswer.isCorrect() ) {
			throw new IllegalArgumentException("correctAnswer is no correct answer!");
		}
		
		if (incorrectAnswer == null) {
			throw new IllegalArgumentException("incorrectAnswer = null is no valid value of Question(questionText, Answer...)");
		}
		
		if ( correctAnswer.equals(incorrectAnswer) ) {
			throw new IllegalArgumentException("correctAnswer and incorrectAnswer are identical in Question(questionText, Answer...)");
		}

		
		this.text = questionText;
		addAnswer(correctAnswer);
		addAnswer(incorrectAnswer);
	}

	public boolean addAnswer(Answer answer) {
		if ( ! answers.contains(answer) ) {
			answers.add(answer);
			return true;
		}
		return false;
	}
	
	public boolean removeAnswer(Answer answer) {
		if (answers.size() < 3) {
			return false;
		}
		if ( !(answer.isCorrect()) ) {
			return answers.remove(answer);
		}		
		if ( hasAtLeast2correctAnswers() ) {
			return answers.remove(answer);
		}
		return false;
	}
	
	private boolean hasAtLeast2correctAnswers ( ) {
		int count = 0;
		for (Answer answer : answers) {
			if (answer.isCorrect()) {
				count++;
			}
		}
		return count >= 2;
	}
	
	public boolean addTopic(ValidText topic) {
		if ( ! topics.contains(topic) ) {
			topics.add(topic);
			return true;
		}
		return false;
	}
	
	public boolean removeTopic(ValidText topic) {
		return topics.remove(topic);
	}
	
	public boolean addComment(ValidText comment) {
		if ( ! comments.contains(comment) ) {
			comments.add(comment);
			return true;
		}
		return false;
	}
	
	public boolean removeComment(ValidText comment) {
		return comments.remove(comment);
		}
}
