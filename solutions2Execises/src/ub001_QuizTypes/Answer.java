package ub001_QuizTypes;

public final class Answer {

	private final ValidText text;
	private final boolean isCorrect;
	
	public Answer(ValidText answerText, boolean answerIsCorrect) {
		if (answerText == null) {
			throw new IllegalArgumentException("answerText = null is no valid value of Answer(answerText, answerIsCorrect)");
		}

		this.text = answerText;
		this.isCorrect = answerIsCorrect;
	}

	public boolean isCorrect() {
		return isCorrect;
	}
	
	@Override
	public String toString() {
		return text.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if ( ! (obj instanceof Answer) ) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		
		Answer other = (Answer) obj;
		if( this.isCorrect == other.isCorrect() && (this.text).equals(other.text) ) {
			return true;
		}
		
		return false;
	}
}
