package ub001_QuizTypes;

public class ValidText {
	
	private final String text;
	
	public ValidText(String text) {
		if (text == null) {
			throw new IllegalArgumentException("ValidText contructor: null is no valid argument.");
		}
		if (text.isEmpty()) {
			throw new IllegalArgumentException("ValidText contructor: empty string is no valid argument.");
		}		
		if (text.isBlank()) {
			throw new IllegalArgumentException("ValidText contructor: string of blanks is no valid argument.");
		}
		this.text = text;
	}

	@Override
	public String toString() {
		return text;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		
		if ( obj instanceof ValidText)  {
			ValidText other = (ValidText) obj;
			if( (this.text).contentEquals( other.toString() ) ) {
				return true;
			}
		}
		
		if ( obj instanceof String)  {
			String other = (String) obj;
			if( (this.text).contentEquals( other ) ) {
				return true;
			}
		}
		
		return false;
	}
	
}
