package ub001_QuizTypes.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import ub001_QuizTypes.Answer;
import ub001_QuizTypes.ValidText;

class ValidTextTest {

	private final String expectedValidText = "Test text";
	private final String expectedValidText2 = "		Test text";	
	
	private final String expectedInvalidNullText = null;
	private final String expectedInvalidEmptyText = "";	
	private final String expectedInvalidBlankText = "	";
	private final String expectedInvalidBlanksText = " 	 	";
	


	
	@Test
	void testInstantiateValidText() {
	    try {
	    	new ValidText(expectedValidText);
		    } catch (Exception e) {
		      fail(e.getMessage());
		    }
	    try {
	    	new ValidText(expectedValidText2);
		    } catch (Exception e) {
		      fail(e.getMessage());
		    }
	}				
	
	@ParameterizedTest(name = "Test #{index}. Expected: {1} ")
	@CsvSource( {
		expectedInvalidEmptyText  + ", false",
		expectedInvalidBlankText  + ", false",
		expectedInvalidBlanksText + ", false",
	} )
	void parameterizedTestInstantiateValidTextWithIllegalArgument(String argument, boolean expected) {
	    try {
	    	new ValidText(argument);
	    	fail("Exception was expected for illegal input argument: " + argument);
	    } catch (IllegalArgumentException e) {          
	    }
	}
	
	@Test
	void testInstantiateValidTextWithIllegalArgumentNull() {
	    try {
	    	new ValidText(expectedInvalidNullText);
	    	fail("Exception was expected for illegal input argument: null");
	    } catch (IllegalArgumentException e) {          
	    }
	}
	
	
	@Test
	void testToString() {
		String actual = new ValidText(expectedValidText).toString();
		assertEquals(expectedValidText, actual);
	}
	
	@Test
	void testEqualsWithIdentity() {
		ValidText validText = new ValidText(expectedValidText);
		assertTrue( validText.equals(validText) );
	}
		
	@Test
	void testEqualsWithNewValidTextRef() {
		ValidText validText = new ValidText(expectedValidText);
		ValidText validText2 = new ValidText(expectedValidText);
		assertTrue( validText.equals(validText2) );
	}		
		
	@Test
	void testEqualsWithSameString() {
		ValidText validText = new ValidText(expectedValidText);
		assertTrue( validText.equals(expectedValidText) );
	}
}
