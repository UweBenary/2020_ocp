package ub001_QuizTypes.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import ub001_QuizTypes.Answer;
import ub001_QuizTypes.ValidText;

class AnswerTest {

	private final String expectedText = "Test text of answer";
	private final ValidText expectedValidText = new ValidText(expectedText);
	private final boolean expectedBoolean = true;
	
	@Test
	void testInstantiateAnswer() {
	    try {
	    	new Answer(expectedValidText, expectedBoolean);
		    } catch (Exception e) {
		      fail(e.getMessage());
		    }
	}				
	
	@Test
	void testInstantiateAnswerWithIllegalFirstArgumentNull() {
	    try {
	    	new Answer(null, expectedBoolean);
	    	fail("Exception was expected for null input argument");
	    } catch (IllegalArgumentException e) {          
	    }
	}
	
	    
	@Test
	void testIsCorrect() {
		Answer answer = new Answer(expectedValidText, expectedBoolean);
		boolean actual = answer.isCorrect();
		assertEquals(expectedBoolean, actual);
	}

	@Test
	void testToString() {
		Answer answer = new Answer(expectedValidText, expectedBoolean);
		String actual = answer.toString();
		assertEquals(expectedValidText, actual);
	}

	@Test
	void testEqualsWithIdentity() {
		Answer answer = new Answer(expectedValidText, expectedBoolean);
		assertTrue( answer.equals(answer) );
	}
	
	@ParameterizedTest(name = "Test #{index}. Expected: {2} ")
	@CsvSource( {
		expectedText + ", " + expectedBoolean + ", true",
		expectedText + ", " + (!expectedBoolean) + ", false",
		"unexpectedText, " + expectedBoolean + ", false",
		"unexpectedText, " + (!expectedBoolean) + ", false",
	} )
	void parameterizedTestEquals(String argument1, boolean argument2, boolean expected) {
		Answer answer = new Answer( expectedValidText, expectedBoolean);
		Answer answer2 = new Answer( new ValidText(argument1) , argument2);
		boolean actual =answer.equals(answer2);
		assertEquals(expected, actual);
	}
}
