package ub001_QuizTypes.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ub001_QuizTypes.Answer;
import ub001_QuizTypes.Question;
import ub001_QuizTypes.ValidText;

class QuestionTest {

	Answer correctAnswer1 = new Answer( new ValidText("Correkt 1"), true);
	Answer correctAnswer2 = new Answer(new  ValidText("Correkt 2"), true);
	Answer incorrectAnswer1 = new Answer(new  ValidText("Incorrekt 1"), false);
	Answer incorrectAnswer2 = new Answer(new  ValidText("Incorrekt 2"), false);
	
	String expectedText =  "   What if?";
	final ValidText expectedValidText = new ValidText(expectedText);
	
	
	/*
	 * Constructor
	 */
	
	@Test
	void testInstantiateQuestion() {
		try {
			new Question(expectedValidText, correctAnswer1, incorrectAnswer1);
    	} catch (Exception e) {
		    fail(e.getMessage());
	    }
		
		try {
			new Question(expectedValidText, correctAnswer1, correctAnswer2);
    	} catch (Exception e) {
		    fail(e.getMessage());
	    }
	}
	
	@Test
	void testInstantiateQuestionWithIllegalArgumentNull() {
		try {
			new Question(null, correctAnswer1, incorrectAnswer1);
	    	fail("Exception was expected for input argument: null");
	    } catch (IllegalArgumentException e) {          
	    }
		
		try {
			new Question(expectedValidText, null, incorrectAnswer1);
	    	fail("Exception was expected for input argument: null");
	    } catch (IllegalArgumentException e) {          
	    }
		
		try {
			new Question(expectedValidText, correctAnswer1, null);
	    	fail("Exception was expected for input argument: null");
	    } catch (IllegalArgumentException e) {          
	    }
	}


	@Test
	void testInstantiateQuestionWithIllegalArgumentAnswer() {
		try {
			new Question(expectedValidText, incorrectAnswer2, incorrectAnswer1);
	    	fail("Exception was expected for incorrect Answer as 2nd input argument");
	    } catch (IllegalArgumentException e) {          
	    }

		try {
			new Question(expectedValidText, incorrectAnswer1, correctAnswer1);
	    	fail("Exception was expected for incorrect Answer as 2nd input argument");
	    } catch (IllegalArgumentException e) {          
	    }

		try {
			new Question(expectedValidText, correctAnswer1, correctAnswer1);
	    	fail("Exception was expected for identical Answer as input arguments");
	    } catch (IllegalArgumentException e) {          
	    }
		
		Answer correctAnswer3 = new Answer( new ValidText("Correkt 1"), true);
		try {
			new Question(expectedValidText, correctAnswer1, correctAnswer3);
	    	fail("Exception was expected for identical Answer as input arguments");
	    } catch (IllegalArgumentException e) {          
	    }
	}
		
	@Test
	void testInstantiateQuestionWith2correctAnswers() {
		try {
			new Question(expectedValidText, correctAnswer1, correctAnswer2);
    	} catch (Exception e) {
		    fail(e.getMessage());
	    }
	}
	
	/*
	 * addAnswer()
	 */

	@Test
	void testAddAnswer() {
		Question question = new Question(expectedValidText, correctAnswer1, incorrectAnswer1);
		assertTrue(question.addAnswer(correctAnswer2));
		assertFalse(question.addAnswer(correctAnswer1));
	}

	@Test
	void testRemoveAnswer() {
		Question question = new Question(expectedValidText, correctAnswer1, incorrectAnswer1);
		question.addAnswer(correctAnswer2);
		question.addAnswer(incorrectAnswer2);
		assertTrue(question.removeAnswer(correctAnswer2));
		assertTrue(question.removeAnswer(incorrectAnswer1));
	}
	
	@Test
	void testRemoveAnswerUponRemovalOf2ndAnswer() {
		Question question = new Question(expectedValidText, correctAnswer1, incorrectAnswer1);
		assertFalse(question.removeAnswer(correctAnswer1));
		assertFalse(question.removeAnswer(incorrectAnswer1));		
	}
	
	@Test
	void testRemoveAnswerUponRemovalOflastCorrectAnswer() {
		Question question = new Question(expectedValidText, correctAnswer1, incorrectAnswer1);
		question.addAnswer(incorrectAnswer2);
		assertFalse(question.removeAnswer(correctAnswer1));	
	}
	
	
	/*
	 * addTopic()
	 */
	
	
	@Test
	void testAddTopic() {
		Question question = new Question(expectedValidText, correctAnswer1, incorrectAnswer1);		
		assertTrue(question.addTopic( new ValidText("topic") ));	
	}

	@Test
	void testRemoveTopic() {		
		Question question = new Question(expectedValidText, correctAnswer1, incorrectAnswer1);
		ValidText topic = new ValidText("topic");
		assertFalse( question.removeTopic(topic) );	
		
		question.addTopic(topic);
		assertTrue(question.removeTopic(topic));	
		
		question.addTopic(topic);
		assertTrue( question.removeTopic(new ValidText("topic")) );
	}

	
	
	/*
	 * addComment()
	 */
	

	@Test
	void testAddComment() {
		Question question = new Question(expectedValidText, correctAnswer1, incorrectAnswer1);		
		assertTrue(question.addComment( new ValidText("Comment") ));	

	}

	@Test
	void testRemoveComment() {		
		Question question = new Question(expectedValidText, correctAnswer1, incorrectAnswer1);
		ValidText comment = new ValidText("Comment");
		assertFalse( question.removeComment(comment) );	
		
		question.addComment(comment);
		assertTrue(question.removeComment(comment));	
		
		question.addComment(comment);
		assertTrue( question.removeComment(new ValidText("Comment")) );
	}

}
