package ub025_BiFunctionMethodReference;

import java.util.ArrayList;
import java.util.List;

public class Owner implements Cloneable {
	
	static Owner of(Car car, Integer numberOfCars) {
		return new Owner(car, numberOfCars);
	}
	
	List<Car> cars = new ArrayList<Car>();
	
	public Owner(Car car, Integer numberOfCars) {
		setCars(car, numberOfCars);
	}

	private void setCars(Car car, Integer numberOfCars) {
		for (int i = 0; i < numberOfCars; i++) {
			cars.add(car);
		}
	}
	
	public Owner createFromPrototype(Car car, Integer numberOfCars) {
		Owner newOwner = this.clone();
		setCars(car, numberOfCars);
		return newOwner;
	}
	
	@Override
	public String toString() {
		return "Owner of " + cars;
	}
	
	@Override
	protected Owner clone() {
		try {
			return (Owner) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new UnsupportedOperationException("Cannot clone owner. " + e.getMessage() );
		}
	}

}
