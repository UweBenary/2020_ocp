package ub025_BiFunctionMethodReference;

import java.util.function.BiFunction;

public class Main {

	public static void main(String[] args) {

		/*
		 * A1
		 */
		BiFunction<Car, Integer, Owner> f1 = new BiFunction<Car, Integer, Owner>() {
			@Override
			public Owner apply(Car c, Integer i) {
				return new Owner(c, i);
			}
		};
		
		Car car1 = new Car("Opel");
		int carCount = 1;
		Owner result = f1.apply(car1, carCount);
		
		System.out.println("A1: " + result);
		
		/*
		 * A2
		 */
		BiFunction<Car, Integer, Owner> f2 = (car, i) -> new Owner(car, i);
		
		result = f2.apply(car1, carCount);
		
		System.out.println("A2: " + result);
		
		/*
		 * A3
		 */
		BiFunction<Car, Integer, Owner> f3 = Owner::of;
		
		result = f3.apply(car1, carCount);
		
		System.out.println("A3: " + result);
		
		/*
		 * A4
		 */
		BiFunction<Car, Integer, Owner> f4 = Owner::new;
		
		result = f4.apply(car1, carCount);
		
		System.out.println("A4: " + result);
		
		/*
		 * A5
		 */		
		Owner owner = result;
		
		BiFunction<Car, Integer, Owner> f5 = owner::createFromPrototype;
		
		Car car5 = new Car("VW");
		carCount = 2;
		result = f5.apply(car5, carCount);
		
		System.out.println("A5: " + result);
		
		/*
		 * A6
		 */
		
		BiFunction<Car, Integer, Owner> f6 = Car::sellToOwner;
		
		Car car6 = new Car("BMW");
		carCount = 1;
		result = f6.apply(car6, carCount);
		
		System.out.println("A6: " + result);
		
		
	}

}
