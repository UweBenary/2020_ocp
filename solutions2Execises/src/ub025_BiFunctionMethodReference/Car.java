package ub025_BiFunctionMethodReference;

public class Car {
	
	private	String carName;
	
	public Car(String carName) {
		this.carName = carName;
	}
	
	public Owner sellToOwner(int numberOfCars) {
		return new Owner(this, numberOfCars);
	}

	@Override
	public String toString() {
		return carName;
	}
}
