package ub016_MapSimple;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Main {

	public static void main(String[] args) {

		/*
		 * Erzeugen Sie bitte eine `Map<String, LocalDate>`
		 */
		Map<String, LocalDate> timeMap = new HashMap<>();
		
		/*
		 * Speichern Sie bitte in der Map den Schlüßel "heute", ordnen Sie dem Schlüßel dabei den Wert, den Sie mit `LocalDate.now()`ermitteln
		 */
		timeMap.put("heute", LocalDate.now() );
		
		/*
		 * Ordnen Sie bitte in der Map den weiteren Schlüßel "gestern" und "morgen" die entsprechende Werte zu.
		 */
		timeMap.put("gestern", LocalDate.now().minusDays(1) );
		timeMap.put("morgen", LocalDate.now().plusDays(1) );
		
		/*
		 * Rufen Sie bitte alle Schlüßel aus der Map ab und iterieren Sie durch die Schlüßel. 
		 * Ermitteln Sie für jeden Schlüßel seinen Wert und geben Sie beide aus.
		 */
		Set<String> timeMapKeys = timeMap.keySet();
		for (String key : timeMapKeys) {
			LocalDate date = timeMap.get(key);
			System.out.println( key + " = " + date.format(DateTimeFormatter.ISO_LOCAL_DATE) );
		}
		
		System.out.println();
		/*
		 * Verwenden Sie die Methode `entrySet` um alle Einträge aus der Map zu erhalten. 
		 * Geben Sie bitte die Einträge einzeln untereinander aus.
		 */
		Set< Map.Entry<String, LocalDate> > timeMapEntries = timeMap.entrySet();
		for (Entry<String, LocalDate> entry : timeMapEntries) {
			System.out.println(entry);
		}
	}

}
