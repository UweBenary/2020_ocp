package ub03_smallTaskAbfahrtszeiten;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class Task {
	
	public static void main(String[] args) {
		
		List<String> abfahrtszeiten = getAbfahrtszeiten(6, 24);
		
		System.out.println( abfahrtszeiten );
		
		
		abfahrtszeiten = getAbfahrtszeiten(LocalTime.of(6, 12), LocalTime.of(23, 59), 20); //better List<LocalTime>
		
		System.out.println( abfahrtszeiten );
		
		TreeSet<String> abfahrtszeitenTreeset = new TreeSet<>(abfahrtszeiten);
		
		System.out.println( "1. " + abfahrtszeitenTreeset.higher("12:03") );
		System.out.println( "2. " + abfahrtszeitenTreeset.lower("12:03") );
		System.out.println( "3. " + abfahrtszeitenTreeset.ceiling("17:12") );
		System.out.println( "4. " + abfahrtszeitenTreeset.higher("17:12") );
		System.out.println( "5. " + abfahrtszeitenTreeset.subSet("12:00", "13:00") );
		System.out.println( "6. " + abfahrtszeitenTreeset.subSet("11:52", false, "13:12", true) );
		System.out.println( "7. " + abfahrtszeitenTreeset.first() );
		System.out.println( "8. " + abfahrtszeitenTreeset.last() );
		
	}

	private static List<String> getAbfahrtszeiten(int start, int end) {		
		List<String> result = new ArrayList<>();
		for (int index = start; index < end; index++) {
			result.add( String.format("%02d", index) );
		}
		return result;
	}
	
	private static List<String> getAbfahrtszeiten(LocalTime start, LocalTime end, int takt) {			
		List<String> result = new ArrayList<>();
		DateTimeFormatter fmt = DateTimeFormatter.ofPattern("HH:mm");
		
		LocalDate date = LocalDate.now();
		LocalDateTime startIndex = LocalDateTime.of(date, start);
		LocalDateTime endIndex = LocalDateTime.of(date, end);
				
		for (LocalDateTime index = startIndex; index.isBefore(endIndex); index = index.plusMinutes(takt)) {
			result.add( index.format(fmt) );
		}
		return result;
	}

}
