package ub09_smallTaskStreams;


import java.util.stream.Stream;

public class Task {

	public static void main(String[] args) {

		String[] a1 = { "mo", "di" };
		String[] a2 = { "mi", "do", "fr" };

		Stream.of(a1, a2)
			.forEach(arr -> {
				for (String string : arr) {
					System.out.print(string + " ");
				}
				System.out.println();
			});
		
	}

}
