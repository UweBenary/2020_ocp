package ub18_smallTaskGroupingBy;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Task {

	public static void main(String[] args) {

		Integer[] array = { 12, -22, 0, 77, 0, -5, -5, 33, 6 };
		
		Function<Integer, String> classifier = i -> i < 0 ? "negative" : i > 0 ? "positive" : "zero"; 
		Map<String, List<Integer>> map = Arrays.stream(array)
												.collect(Collectors.groupingBy(classifier));
		System.out.println(map);
		
		
		Collector<Integer, ?, HashSet<Integer>> downstream = Collectors.toCollection(HashSet::new);
		Map<String, HashSet<Integer>> map2 = Arrays.stream(array)
												.collect(Collectors.groupingBy(classifier, downstream));
		System.out.println(map2);
		
		
		Supplier<TreeMap<String, TreeSet<Integer>>> mapFactory = TreeMap::new;
		Collector<Integer, ?, TreeSet<Integer>> downstream3 = Collectors.toCollection(TreeSet::new);
		Map<String, TreeSet<Integer>> map3 = Arrays.stream(array)
												.collect(Collectors.groupingBy(classifier, mapFactory, downstream3));
		System.out.println(map3);
	}

}
