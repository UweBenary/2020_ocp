package ub04_smallTaskDeque;

import java.util.ArrayDeque;
import java.util.Deque;

public class Mirror {
	
	private Deque<Character> charDeque = new ArrayDeque<Character>();
	
	public Mirror() {
		charDeque.add('|');
		}

	public void add(char ch) {
		charDeque.addFirst(ch);
		charDeque.addLast(ch);
	}

	public boolean isEmpty() {
		return charDeque.size() == 1 ;
	}

	public void remove() {
		charDeque.removeFirst();
		charDeque.removeLast();
	}
	
	@Override
	public String toString() {
		String result = "";
		for (Character character : charDeque) {
			result += character;
		}
		return result;
	}

}
