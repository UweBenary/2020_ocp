package ub04_smallTaskDeque.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ub04_smallTaskDeque.Mirror;

class MirrorTest {

	Mirror m = new Mirror();
	
	@Test
	final void testAdd() {
		Mirror m = new Mirror();
		m.add('a');
		m.add('b');
		final String expected = "ba|ab";
		final String actual = m.toString();
		assertEquals(expected, actual);
	}

	@Test
	final void testIsEmpty() {
		Mirror m = new Mirror();
		assertTrue( m.isEmpty() );	
		}

	@Test
	final void testRemove() {
		Mirror m = new Mirror();
		m.add('a');
		m.add('b');
		m.remove();
		final String expected = "a|a";
		final String actual = m.toString();
		assertEquals(expected, actual);
	}
	
	@Test
	final void testToString() {
		Mirror m = new Mirror();		
		final String expected = "|";
		final String actual = m.toString();
		assertEquals(expected, actual);
	}

}
