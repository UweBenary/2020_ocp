package ub10_smallTaskFilterStream;

import java.util.Arrays;

public class Task {
	
	public static void main(String[] args) {
		
		String[] arr = {"mo", "di", "mi", "do", "fr" };
		
		Arrays.stream(arr)
			.filter( s->s.contains("m"))
			.forEach(System.out::println);
	}

}
