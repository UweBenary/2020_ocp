package ub24_smallTaskDiningPhilosophersProblem;

public class Fork {

	private String name;
	private Philosopher user;
	
	public Fork(String forkName) {
		name = forkName;
		user = null;
	}
	
	public void usedBy(Philosopher philosopher) {
		if (user != null) {
			throw new IllegalStateException(philosopher + " cannot use " + toString());
		}
		user = philosopher;
	}
	
	public void returned() {
		user = null;
	}

	@Override
	public String toString() {
		return name + " used by " + (user==null? "nobody" : user.toString());
	}
}
