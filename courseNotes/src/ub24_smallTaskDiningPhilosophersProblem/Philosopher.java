package ub24_smallTaskDiningPhilosophersProblem;

public class Philosopher implements Runnable {

	private Fork rightFork;
	private Fork leftFork;
	private String name;
	
	
	public Philosopher(String name, Fork rightFork, Fork leftFork) {
		this.name = name;
		this.rightFork = rightFork;
		this.leftFork = leftFork;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	public String showForks() {
		return " [" + rightFork.toString() +  ", " + leftFork.toString() +  "]";
	}

	@Override
	public void run() {
		while (true) {
			try {
				thinking();
				startDining();
			} catch (InterruptedException e) {
				leftFork.returned();
				rightFork.returned();
				break;
			}			
		}	
	}

	private void startDining() throws InterruptedException {
		System.out.println(toString() + " is waiting for leftFork " + leftFork.toString() + " ...");
		
		synchronized (leftFork) {
			leftFork.usedBy(this);
			System.out.println(toString() + " is waiting for rightFork " + rightFork.toString() + " ...");
			
			synchronized (rightFork) {
				rightFork.usedBy(this);
				
				haveSpaghetti();
				
				rightFork.returned();
			}
			
			leftFork.returned();	
		} 	
		
	}


	private void haveSpaghetti() throws InterruptedException {
		System.out.println(toString() + " is having spaghetti...");
		Thread.sleep(3000);//0 -> DeadLock		
	}

	private void thinking() throws InterruptedException {
		System.out.println(toString() + " is thinking...");
		Thread.sleep(2000);//0 -> DeadLock	
	}


}
