package ub24_smallTaskDiningPhilosophersProblem;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;

public class Main {

	public static void main(String[] args) {

		final int numberOfPhilosophers = 5;
		
		List<Fork> forks = new ArrayList<Fork>();
		for (int i = 0; i < numberOfPhilosophers; i++) {
			forks.add( new Fork("Fork " + (i+1) ) );
		}

		List<Philosopher> philosophers = new ArrayList<Philosopher>();
		for (int i = 0; i < numberOfPhilosophers; i++) {
			philosophers.add( new Philosopher("Philosopher " + (i+1), forks.get(i), forks.get((i+1)%numberOfPhilosophers) ) );
		}
		
		for (Philosopher philosopher : philosophers) {
			System.out.println(philosopher + philosopher.showForks());
		}
		
		for (Fork fork : forks) {
			System.out.println(fork);
		}
		
		List<Thread> philosopherThreads = new ArrayList<Thread>();
		for (int i = 0; i < numberOfPhilosophers; i++) {
			philosopherThreads.add( new Thread( philosophers.get(i)) );
		}
		
		for (Thread philosopherThread : philosopherThreads) {
			philosopherThread.start();
		}
		
		Thread terminator = new Thread( () -> {
			try {
				Thread.sleep(120_000);
				System.out.println("Terminator terminates dinner.");
				for (Thread philosopherThread : philosopherThreads) {
					philosopherThread.interrupt();
				}
			} catch (InterruptedException e) {
				e.getStackTrace();
			}
		});
		terminator.start();
		
	}

}
