package ub06_smallTaskRekursiveTypeBound;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RecTypeBound {
	
	public static void main(String[] args) {
		
		Integer x1 = getMax1(12, 55);
		System.out.println("x1 = " + x1);
		
		Double x2 = getMax1(22.0, 3.5);
		System.out.println("x2 = " + x2);

//		getMax1("a", "b"); //Compilerfehler
		
		x1 = getMax2(12, 55);
		System.out.println("x1 = " + x1);
		
		x2 = getMax2(22.0, 3.5);
		System.out.println("x2 = " + x2);
		
		String x3 = getMax2("a", "b");
		System.out.println("x3 = " + x3);
		
//		getMax2("a", 12); //Compilerfehler
		
		
	}
	
	private static <T extends Comparable<T>> T getMax2(T a, T b) {
//		List<T> arr = new ArrayList<T>();
//		arr.add(a);
//		arr.add(b);	
//		
//		arr.sort(Collections.reverseOrder());
//		return arr.get(0);
		
		if (a.compareTo(b) > 0) {
			return a;
		}
		
		return b;		
	}

	private static <T extends Number> T getMax1(T a, T b) {
		if (a.doubleValue() > b.doubleValue()) {
			return a;
		}
		
		return b;		
	}
}
