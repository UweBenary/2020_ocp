package ub27_smallTaskJoinFork;

import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class IntegerIntervalFinder extends RecursiveAction{
	
	private List<Integer> list;
	private static final int intervalSize = 3;

	
	public IntegerIntervalFinder(List<Integer> list) {
		this.list = list;
	}

	public static void main(String[] args) {
		
		int a = 1;
		int b = 9;
		
		List<Integer> list = Stream.iterate(1, x->x+1).limit(b-a+1).collect(Collectors.toList());
		System.out.println(list);
		ForkJoinPool pool = ForkJoinPool.commonPool();
		RecursiveAction task = new IntegerIntervalFinder(list);
		pool.invoke(task);

	}

	@Override
	protected void compute() {

		if (list.size() <= intervalSize) {
			System.out.println("Sublist: " + list);
		} else {
			RecursiveAction task1 = new IntegerIntervalFinder(list.subList(0, intervalSize));
			RecursiveAction task2 = new IntegerIntervalFinder(list.subList(intervalSize, list.size()));
			
			invokeAll(task1, task2);			
		}
		
	}

}
