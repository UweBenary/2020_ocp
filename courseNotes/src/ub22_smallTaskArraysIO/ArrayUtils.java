package ub22_smallTaskArraysIO;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class ArrayUtils {

	public static int[] createRandomIntArray(int len, int min, int max) {
		Random rand = new Random();
		
		int[] arr = new int[len];
		
		for (int i = 0; i < arr.length; i++) {
			arr[i] = rand.nextInt(max+1-min) + min;
		}
		return arr;
	}

	public static void saveToFile(int[] arr1, String fileName) {
		
	try( FileWriter out = new FileWriter(fileName/*, false*/) ) {
			for (int i : arr1) {
				out.write(i);
				out.write(" ");
			}
						
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public static int[] loadFromFile(String fileName) {
		List<Integer> list = new ArrayList<>();

		try( FileReader in = new FileReader(fileName) ) {
			
			int i = 0;
			while ( ( i = in.read()) != -1 )  {
				if (i!=32) { //'space'
					list.add( i );
				}

			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		int[] result = new int[list.size()];
		for (int i = 0; i < list.size(); i++) {
			result[i] = list.get(i);
		}

		return  result;
	}

}
