package ub22_smallTaskArraysIO;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class SmallTask {
	
	public static void main(String[] args) {
		
		int len = 10;
		int min = 0;
		int max = 15;	
		
		int[] arr1 = ArrayUtils.createRandomIntArray(len, min, max);
		
		System.out.println(Arrays.toString(arr1));
		
		Path file = Paths.get("src", "ub22_smallTaskArraysIO", "array.txt");
		
//		try {
//			if ( ! Files.exists(file) ) {
//				Files.createFile(file);
//			}
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		
//		try {
//			if ( Files.exists(file) ) {
//				Files.delete(file);
//			}
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		}

		ArrayUtils.saveToFile(arr1, file.toString() );
		
		int[] arr2 = ArrayUtils.loadFromFile( file.toString() );
		
		System.out.println("References equal? " + (arr1 == arr2)); //false
		System.out.println("Content equal? " + Arrays.equals(arr1, arr2)); //true
		
		
	}

}
