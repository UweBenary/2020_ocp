package ub16_smallTaskCollect;

import java.util.Arrays;
import java.util.Comparator;
import java.util.TreeSet;
import java.util.function.BiConsumer;
import java.util.function.Supplier;
import java.util.stream.Stream;


public class Task {

	static class Kreis {
		private int radius;

		public Kreis(int radius) {
			this.radius = radius;
		}
		
		
		public int getRadius() {
			return radius;
		}


		@Override
		public String toString() {
			return "Kreis R=" + radius;
		}
	}
	public static void main(String[] args) {


		a1();
		a2();
	}

	private static void a2() {
		Integer[] radien = { 2, 4, 1, 5 };
		
		
		Supplier<TreeSet<Kreis>> supplier = 
				() -> new TreeSet<>( Comparator.comparingInt(Kreis::getRadius)); //comparingInt() saves autoboxing wrt comparing()
		BiConsumer<TreeSet<Kreis>, Kreis> accumulator = TreeSet::add;
		BiConsumer<TreeSet<Kreis>, TreeSet<Kreis>> combiner = TreeSet::addAll;
		TreeSet<Kreis> result = Arrays.stream(radien)
									  .map(Kreis::new)
									  .collect(supplier , accumulator , combiner );

		System.out.println(result);
	}

	private static void a1() {
		Supplier<StringBuilder> supplier = StringBuilder::new;
		BiConsumer<StringBuilder, String> accumulator = StringBuilder::append; //(sb, str) -> sb.append(str);
		BiConsumer<StringBuilder, StringBuilder> combiner = StringBuilder::append; //(sb1, sb2) -> sb1.append(sb2);
		
		StringBuilder result = Stream.of("a", "bb", "ccc")
									 .collect(supplier, accumulator , combiner );
		System.out.println(result);
	}

}
