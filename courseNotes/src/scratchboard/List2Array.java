package scratchboard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class List2Array {

	public static void main(String[] args) {

		List<String> list = Arrays.asList( new String[] {"ene", "mene", "muh", "und", "raus", "bist", "du", "!"} );
		System.out.println(list.size());

		int nColumns = 5;
		
		String[][] array = stringListTo2dArray(list, nColumns);
		System.out.println(Arrays.deepToString(array));
		
		array = collTo2dArray(list, nColumns);
		System.out.println(Arrays.deepToString(array));
		
	}

	private static String[][] stringListTo2dArray(List<String> list, int nColumns) {
		AtomicInteger streamElementIndex = new AtomicInteger(0);
		AtomicInteger subgroupIndex = new AtomicInteger(0);
		
		
		Map<Integer, List<String>> groups = list.stream().limit(7).collect(Collectors.groupingBy( s -> {
			if ( streamElementIndex.getAndIncrement() % nColumns == 0) {
				subgroupIndex.getAndIncrement();
			}
			return subgroupIndex.get();
		}));

		System.out.println(groups);
		
		 List<List<String>> subSets = new ArrayList<List<String>>(groups.values());
		 	 
		 System.out.println(subSets);
		
		 String[][] array = subSets.stream().map(l -> l.stream().toArray(String[]::new)).toArray(String[][]::new);
	
		 return array;
	
	}

	private static <T> T[][] collTo2dArray(Collection<T> coll, int arrayLength) {
			AtomicInteger streamElementIndex = new AtomicInteger(0);
			AtomicInteger subgroupIndex = new AtomicInteger(0);
					
			Function<T, Integer> classifier = t -> {
						if ( streamElementIndex.getAndIncrement() % arrayLength == 0) {
							subgroupIndex.getAndIncrement();
						}
						return subgroupIndex.get();
					};

			Map<Integer, List<T>> groups = coll.stream().collect(Collectors.groupingBy(classifier));
					
			List<List<T>> subSets = new ArrayList<List<T>>(groups.values()); 	 
			
			Object[][] array = subSets.stream().map(l -> l.stream().toArray(Object[]::new)).toArray(Object[][]::new);
			
			
			
			return (T[][]) null;
		}
}
