package scratchboard;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.RecursiveTask;

public class Threads {

	public static void main(String[] args) {
		
		ForkJoinPool pool = new ForkJoinPool();
		
		RecursiveAction task = null;
		pool.invoke(task);
		
		RecursiveTask<Integer> task2 = null;
		task2.fork();

	}

}
