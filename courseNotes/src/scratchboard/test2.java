package scratchboard;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.UnaryOperator;

public class test2 {

	public static void main(String[] args) {
		UnaryOperator<String> trafo1 =  s -> s.toUpperCase() ;
		UnaryOperator<String> trafo2 =  s -> s + "!"  ;

		String expected = "[" + trafo1.toString() + ", " + trafo2.toString() + "]";
		System.out.println(expected);

		List<Function> list = new ArrayList<>();
		list.add(trafo1);
		list.add(trafo2);
		System.out.println(list);
		
	}

}
