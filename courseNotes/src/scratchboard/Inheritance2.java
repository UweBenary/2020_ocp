package scratchboard;

class Supa {}

public class Inheritance2 extends Supa {

	Supa m1() {
		return new Inheritance2();
	}
	
	
	public static void main(String[] args) {
		
		Supa in = new Inheritance2().m1();
		
		System.out.println( in instanceof Inheritance2);
	}
}
