package scratchboard;

import java.nio.file.Path;
import java.nio.file.Paths;

public class IOTest {

	public static void main(String[] args) {
		System.out.println("* mit einem relativen Pfad");
		Path path = Paths.get("C:/a/b", "c/d");
		
		System.out.println("path: " + path);
		
		int count = path.getNameCount();
		System.out.println("getNameCount(): " + count);
		
		for(int i=-1; i<count; i++) {
			Path namePath = path.getName(i);
			System.out.println("i: " + i + ", getName(i): " + namePath); // 0 -> a, 1 -> b ...
		}
	}

}
