package scratchboard;

import java.util.OptionalInt;
import java.util.stream.Stream;

public class IntStream {

	public static void main(String[] args) {
		
		String[][] data = {{},{""}};
		
		OptionalInt max;
		
		try {
			max = Stream.of(data)
				.flatMap(array -> Stream.of(array))
				.mapToInt(String::length)
				.max();
			
		} catch (NullPointerException e) {
			throw new NullPointerException("Data must not have elemnts of 'null'. ");
		}
		
		System.out.println(max);
	}
}
