package scratchboard;

import java.util.Optional;

class Value <V> {
	private Optional<V> value;
	
	public Value(V input) {
		value = Optional.of(input);
	}

	public V getValue() {
		return value.get();
	}
}

public class Generics {

	public static void main(String[] args) {
		
		Value<String> valString = new Value<>("Test");
		System.out.println(valString.getValue()); //Test
		
//		valString =  new Value<>(2);
//		valString =  new Value<Integer>(2);
		valString =  new Value(2);
//		System.out.println(valString.getValue()); //ClassCastEcx as expected
		
		
		/*
		 * How to set generic type at runtime? 
		 */
		
		Object input = getInput();
		
		Value<input.getClass()> val = new Value<>(input);
		
	}

	private static Object getInput() {
		return null;
	}

}
