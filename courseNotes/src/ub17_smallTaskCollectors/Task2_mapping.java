package ub17_smallTaskCollectors;

import java.util.Arrays;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Task2_mapping {

	public static void main(String[] args) {

		Integer[] zahlen = { 1, 22, 333, 4444 };
		
		Function<Integer, String> mapper = String::valueOf;
		Collector<String, ?, TreeSet<String>> downstream = Collectors.toCollection(TreeSet::new);
		TreeSet<String> set = Arrays.stream(zahlen)
			  .collect( Collectors.mapping(mapper, downstream));
		System.out.println(set);
	}

}
