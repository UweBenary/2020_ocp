package ub17_smallTaskCollectors;

import java.util.LinkedList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Wochentag {
	
	private int zahl;
	private String name;
	
	public Wochentag(int zahl, String name) {
		super();
		this.zahl = zahl;
		this.name = name;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return name + " (" + zahl + ")";
	}
	
}

public class Task1_Collectors_toXXX {

	public static void main(String[] args) {

		Integer[] zahlen = { 1, 2, 3, 4, 5, 6, 7 };
		String[] namen = { "mo", "di", "mi", "do", "fr", "sa", "so" };
		
		int length = Math.min(zahlen.length, namen.length);
		
		LinkedList<Wochentag> wochentage = Stream.iterate(0, i->i+1)
												 .limit(length)
												 .map( i -> new Wochentag(zahlen[i], namen[i]) )
												 .collect( Collectors.toCollection(LinkedList::new) );
		
		System.out.println(wochentage);
	}

}
