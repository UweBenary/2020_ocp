package ub08_smallTaskMethodReference;

import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

import javax.sound.midi.Soundbank;

public class Task1 {

	public static void main(String[] args) {
		
		
		BiFunction<String, String, String> f1 = (x, y) -> x.concat(y);
		
		BiFunction<String, String, String> f2 = String::concat;
		
		System.out.println(f1.apply("a", "b"));
		System.out.println(f2.apply("a", "b"));
		
		BinaryOperator<String>  f3 = (x, y) -> x.concat(y);
		System.out.println(f3.apply("a", "b"));
		
	}
}
