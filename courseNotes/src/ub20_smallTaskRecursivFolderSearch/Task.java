package ub20_smallTaskRecursivFolderSearch;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hamcrest.core.Is;

public class Task {

	public static void main(String[] args) {
		
		File root = new File("src");
		root = new File(".");
		
		List<File> files = getFiles(root);
		
		System.out.println(files.size());
		System.out.println(files);
	}

	private static List<File> getFiles(File root) {

		List<File> rootFileList = Arrays.asList(root.listFiles(f->f.isDirectory())); // w/o filter NPE
		
		List<File> fileList = new ArrayList<File>();
		
		for (File file : rootFileList) {
			fileList.addAll(getFiles(file));
		}
		
		fileList.addAll(rootFileList);
		
		return fileList;
	}
}
