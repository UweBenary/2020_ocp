package ub14_smallTaskMatch;

import java.util.stream.Stream;

public class EmptyStreamMatch {
	
	public static void main(String[] args) {
		
		System.out.println("allMatch:");
		System.out.println( Stream.<Integer>empty().allMatch( i->true ) );
		System.out.println( Stream.<Integer>empty().allMatch( i->false ) );
		System.out.println();
		
		System.out.println("anyMatch:");
		System.out.println( Stream.<Integer>empty().anyMatch( i->true ) );
		System.out.println( Stream.<Integer>empty().anyMatch( i->false ) );
		System.out.println();
		
		System.out.println("noneMatch:");
		System.out.println( Stream.<Integer>empty().noneMatch( i->true ) );
		System.out.println( Stream.<Integer>empty().noneMatch( i->false ) );
		
	}

}
