package ub25_permutation;

import java.util.Arrays;

public class HeapsAlgorithm {
	
	
	public static void main(String[] args) {
		
		char[] arr = { 'a',  'b', 'c'};
	
		permutate(arr, arr.length);
	
		
	}

	/*
	 * https://en.wikipedia.org/wiki/Heap%27s_algorithm
	 */
	public static void permutate(char[] arr, int pointer) {
	    if(pointer==1) {
	        System.out.printf("%s %n", Arrays.toString(arr));
	        return;
	    }
	    
		for (int i = 0; i < pointer-1; i++) {
		   permutate(arr, pointer-1);
		    
			if(pointer%2==0) {
			    char tmp = arr[pointer-1];
			    arr[pointer-1] = arr[i];
			    arr[i] = tmp;
			} else {
			    char tmp = arr[pointer-1];
			    arr[pointer-1] = arr[0];
			    arr[0] = tmp;
			}
			
		}
		
		permutate(arr, pointer-1);
	}
	
	
}
