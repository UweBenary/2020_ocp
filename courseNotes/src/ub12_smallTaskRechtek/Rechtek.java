package ub12_smallTaskRechtek;

import java.util.Arrays;
import java.util.Comparator;

/*
		 * Erstellen Sie bitte eine Klasse Rechtek (mit Attributen 'breite' und 'hoehe'). 
		 * Dann ein Array mit 4-5 Rechteck-Referenzen. 
		 * Dann bitte das Array als Datenquelle für eine Pipeline einsetzen. 
		 * Dann mit der Pipeline beide 'sorted' Operations testen.
		 */
public class Rechtek implements Comparable<Rechtek> {

	public static void main(String[] args) {
		
		Rechtek[] arr = {
				new Rechtek(2, 7),
				new Rechtek(1, 7),
				new Rechtek(2, 10),
				new Rechtek(3, 7),
				new Rechtek(2, 2),
		};

		Arrays.stream(arr)
			.sorted()
			.forEach(System.out::println);
		
		System.out.println("---");
		Comparator<Rechtek> comparator = Comparator.reverseOrder();
		Arrays.stream(arr)
			.sorted(comparator)
			.forEach(System.out::println);		
		

	}
	
	
	
	private int breite;
	private int hoehe;
	
	
	public Rechtek(int breite, int hoehe) {
		if (breite < 1) {
			throw new IllegalArgumentException("breite < 1");
		}
		
		if (hoehe < 1) {
			throw new IllegalArgumentException("hoehe < 1");
		}
		
		this.breite = breite;
		this.hoehe = hoehe;
	}

	public int getArea() {
		return breite*hoehe;
	}
	
	@Override
		public String toString() {
			return "Rechteck (" + breite + "," + hoehe + ")";
		}

	@Override
	public int compareTo(Rechtek o) {
		int result = breite - o.breite;
		if (result == 0) {
			result = hoehe - o.hoehe; 
		}
		return result;
	}


}
