package ub21_smallTaskCreateDelete;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Task {

	public static void main(String[] args) throws IOException {

		Path dir = Paths.get("a/b/c");
		Files.createDirectories(dir);

		System.out.println(Paths.get("a").getParent());
		
		int count = 0;
		do {
			count++;
			Files.delete(dir);
			dir = dir.getParent();
		} while (  dir != null  && count < 3 );

	}

}
