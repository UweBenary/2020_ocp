package ub13_smallTaskPerson;

import java.util.stream.Stream;

public class task {
	
	static class Person {
		String name;
		
		Person(String name) {
			this.name = name;
		}
		
		@Override
		public String toString() {
			return "Person " + name;
		}
	}
	
	public static void main(String[] args) {
		
		String[] arr = { "Peter", "Paul", "Mary" };
		
		Stream.of(arr)
			.map( s -> new Person(s) )
			.forEach(System.out::println);
		
	}
	

}
