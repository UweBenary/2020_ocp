package ub26_smallTaskFuture;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SmallTask {

	public static void main(String[] args) {
		
		Random random = new Random();
		
		List<Callable<Integer>> callables = new ArrayList<Callable<Integer>>();
		for (int i = 0; i < 100; i++) {
			callables.add( () -> random.nextInt(5) );
		}
		
		ExecutorService service = Executors.newCachedThreadPool();
		
		Integer sum = 0;
		for (Callable<Integer> task : callables) {
			Future<Integer> result = service.submit(task);
			try {
				sum += result.get();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		System.out.println(sum);	
		
//		System.out.println(
//				Stream.generate(  () -> random.nextInt(5) )
//					.limit(100)
//					.map( i -> service.submit( () -> i))
//					.map( t -> {
//						try {
//							return t.get();
//						} catch (Exception e) {
//							e.printStackTrace();
//						}
//						return null; 
//					})
//					.mapToInt(Integer::intValue)
//					.sum()
//					);

		
		List<Runnable> notExecuted = service.shutdownNow();
		System.out.println(notExecuted.size());
	}
}
