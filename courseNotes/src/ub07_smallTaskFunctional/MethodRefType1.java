package ub07_smallTaskFunctional;

import java.util.Comparator;
import java.util.Random;

class MyUtils1 {
	
	static int random(int i1, int i2) {
		return new Random().nextInt();
	}
}


public class MethodRefType1 {

	public static void main(String[] args) {

		Comparator<Integer> c0 = (x, y) -> MyUtils1.random(x, y); 
		Comparator<Integer> c1 = MyUtils1::random; 
		
		System.out.println(c1.compare(1, 1));

	}

}
