package ub07_smallTaskFunctional;

import java.util.function.Function;
import java.util.function.UnaryOperator;

public class Task {
	
	public static void main(String[] args) {
		
	Function<String, UnaryOperator<String>>	v5 = s -> x -> x.concat(s);
	
	System.out.println( v5.apply("Bla").apply("blib") ); // x.concat(s)

	
	}

}
