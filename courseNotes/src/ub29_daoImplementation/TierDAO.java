package ub29_daoImplementation;

import java.util.List;


public interface TierDAO {
	void create(Tier t);
	void delete(int id);
	void update(Tier t);
	Tier findByID(int id);
	List<Tier> getAllTiere();
}
