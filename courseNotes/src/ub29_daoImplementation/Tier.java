package ub29_daoImplementation;

public class Tier {
	private int id;
	private String name;
	private int alter;
	
	public Tier(int id, String name, int alter) {
		this.id = id;
		this.name = name;
		this.alter = alter;
	}
	
	
	
	public int getId() {
		return id;
	}



	public String getName() {
		return name;
	}



	public int getAlter() {
		return alter;
	}



	@Override
	public String toString() {
		return name + " id: " + id + ", alter: " + alter;
	}
}
