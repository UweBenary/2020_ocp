package ub29_daoImplementation;

import java.util.List;

public class Client {
	public static void main(String[] args) {

		TierDAO dao = Tiers.getDefaultDAO();
		
		Tier t = new Tier(4, "Rex", 2);
		
		dao.create( t );
		
		dao.delete( 2 );

		dao.update( new Tier(1, "Tomas", 9) );
		
		System.out.println( dao.findByID(1) );

		List<Tier> alleTiere = dao.getAllTiere();
		
		System.out.println("*** alle Tiere: ");
		alleTiere.forEach(System.out::println);
	}
}
