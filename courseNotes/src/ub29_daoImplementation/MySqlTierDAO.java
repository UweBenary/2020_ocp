package ub29_daoImplementation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ub28_mysql.MySqlUtils;

public class MySqlTierDAO implements TierDAO {
	
	public MySqlTierDAO() {
		
		try( Connection connection = MySqlUtils.getConnection() ) {
			MySqlUtils.removeTabelleTiere(connection);
			MySqlUtils.buildTabelleTiere(connection);
			MySqlUtils.tiereHinzufuegen(connection);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void create(Tier t) {
		try( Connection connection = MySqlUtils.getConnection();
				Statement stm = connection.createStatement(); ) {
				
				String sql = String.format("INSERT INTO `tiere` (`id`, `name`, `alter`) VALUES ('%d', '%s', '%d')",
						t.getId(), t.getName(), t.getAlter() );
				
				stm.executeUpdate(sql) ;
				System.out.println("Tier " + t + " kreiert.");
		} catch (SQLException e) {
			throw new RuntimeException("Tier kann nicht kreiert werden" , e);
		}
	}

	@Override
	public void delete(int id) {
		try( Connection connection = MySqlUtils.getConnection();
				Statement stm = connection.createStatement(); ) {
									
			Tier t = findByID(id);	
			
			String sql = "delete from `tiere` where id='" + id + "'";
			stm.executeUpdate(sql);

			System.out.println("Tier " + t + " gelöscht.");
		} catch (SQLException e) {
			throw new RuntimeException("Tier kann nicht gelöscht werden" , e);
		}
	}

	@Override
	public void update(Tier t) {
		Tier tOld = null;
		try( Connection connection = MySqlUtils.getConnection();
				Statement stm = connection.createStatement(); ) {
			
			tOld = findByID(t.getId());	
			
			String sql = String.format("update `tiere` set `name` = '%s' where id='%d'", 
					t.getName(), t.getId());
			stm.executeUpdate(sql);
			
			sql = String.format("update `tiere` set `alter` = '%d' where id='%d'", 
					t.getAlter(), t.getId());
			stm.executeUpdate(sql);

			System.out.println("Tier " + tOld + " aktualisiert zu " + t );
		} catch (SQLException e) {
			throw new RuntimeException("Tier " + tOld + " kann nicht aktualisiert werden" , e);
		}
	}

	@Override
	public Tier findByID(int id) {
		Tier t;
		String sql = "SELECT * FROM `tiere` where id='" + id + "'";
		
		try( Connection connection = MySqlUtils.getConnection();
				Statement stm = connection.createStatement(); 
				ResultSet res = stm.executeQuery(sql) ) {
			
			if ( ! res.next() ) {
				throw new NullPointerException();
			};	
			
			int idRes = res.getInt("id");
			String nameRes = res.getString("name");
			int alterRes = res.getInt("alter"); 
			
			t = new Tier(idRes, nameRes, alterRes);
			
			System.out.println("Tier mit id=" + id + " ist: " + t);
		} catch (SQLException | NullPointerException e) {
			throw new RuntimeException("Tier mit id=" + id + "kann nicht gefunden werden" , e);
		}
		
		return t;
	}

	@Override
	public List<Tier> getAllTiere() {
		List<Tier> all = new ArrayList<Tier>();
		
		try( Connection connection = MySqlUtils.getConnection();
				Statement stm = connection.createStatement(); 
				ResultSet res = stm.executeQuery("SELECT * FROM `tiere`") ) {
			
			while (res.next()) {
				int idRes = res.getInt("id");
				String nameRes = res.getString("name");
				int alterRes = res.getInt("alter"); 
				
				all.add( new Tier(idRes, nameRes, alterRes) );
			}	
					
			System.out.println(all.size() + " Tiere gefunden.");
		} catch (SQLException e) {
			throw new RuntimeException("Keine Tiere gefunden" , e);
		}
		
		return all;
	}

}
