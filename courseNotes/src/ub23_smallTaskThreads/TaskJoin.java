package ub23_smallTaskThreads;

public class TaskJoin {

	volatile static int value;
	
	public static void main(String[] args) {
//
//		class myThread extends Thread {
//			volatile int value = 0;
//			@Override
//			public void run() {
//				for (int i = 0; i < 1_000_000; i++) {
//					value++;
//				}
//			}
//		}
//		
//		myThread th = new myThread();
//		
//		th.start();
//		
//		try {
//			th.join();
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//
//		System.out.println(th.value);
		
		
		
		Thread th1 = new Thread (
				()-> { for (int i = 0; i < 1_000_000; i++) {
					value++;
				}});
		th1.start();
		try {
			th1.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(value);
	}

}
