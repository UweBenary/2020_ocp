package ub23_smallTaskThreads;

import java.util.Random;
import java.util.stream.Stream;

public class PriorityTask {

	public static void main(String[] args) {
		
		
		Thread th1 = new Thread( () -> {
			System.out.println("th1: " +
					Stream.iterate(0, x -> x+1)
					.limit(10_000_000)
					.mapToInt( x -> new Random().nextInt(10))
					.sum()
					);					
		}) ;
		Thread th2 = new Thread( () -> {
			System.out.println(Thread.currentThread().getName() + ": " +
					Stream.iterate(0, x -> x+1)
					.limit(10_000_000)
					.mapToInt( x -> new Random().nextInt(10))
					.sum()
					);					
		}, "Tread th2") ;
		
		th1.setPriority(Thread.MAX_PRIORITY);
		th2.setPriority(Thread.MIN_PRIORITY);
		
//		th1.setPriority(Thread.NORM_PRIORITY);
//		th2.setPriority(Thread.NORM_PRIORITY);
		
		th2.start();
		th1.start();

	}

}
