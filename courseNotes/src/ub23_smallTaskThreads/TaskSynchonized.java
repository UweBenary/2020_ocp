package ub23_smallTaskThreads;

import java.util.function.BiConsumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Printer extends Thread{
	char c;
	int lineLength; 
	int lineCount;
	String text;
	
	public Printer(char c, int lineLength, int lineCount) {
		super();
		this.c = c;
		this.lineLength = lineLength;
		this.lineCount = lineCount;
		
		text = Stream.generate( () -> String.valueOf(c))
				.limit(lineLength)
				.collect(Collectors.joining());
	}
	
	@Override
	public void run() {

		for (int i = 0; i < lineCount; i++) {			
//			System.out.println(
//				Stream.generate( () -> c )
//					.limit(lineLength)
//					.collect(StringBuilder::new, StringBuilder::append, StringBuilder::append));
			
//			Stream.generate( () -> c )
//					.limit(lineLength)
//					.forEach(System.out::print);
//			System.out.println();
			
//			synchronized (System.out) { // println synchronisiert selbst schon auf System.out
//				for (int j = 0; j < lineLength; j++) {
//					System.out.print(c); 
//				}
//				System.out.println();
//			}

			System.out.println(text); // println synchronisiert selbst schon auf System.out
	

		}

	}
}


public class TaskSynchonized {

	public static void main(String[] args) {

		Printer p1 = new Printer('a', 10, 20);
		p1.start();
	
		Printer p2 = new Printer('*', 17, 30);
		p2.start();
		
	}

}
