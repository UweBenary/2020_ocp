package ub15_smallTaskReduce;

import java.util.Arrays;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

import ub12_smallTaskRechtek.Rechtek;

public class smallTaskReduceTypechange {

	public static void main(String[] args) {

		Rechtek[] arr = {
				new Rechtek(2, 1),
				new Rechtek(2, 3),
				new Rechtek(4, 5),
				new Rechtek(5, 1),
				new Rechtek(1, 3),
					};
		
		int sum = Arrays.stream(arr)
			  .mapToInt(Rechtek::getArea)
			  .sum();
		System.out.println(sum);
		
		Integer identity = 0;
		
		BiFunction<Integer, Rechtek, Integer> accumulator = (i, rechteck) -> i + rechteck.getArea();
		
		BinaryOperator<Integer> combiner = Integer::sum;
		Integer sum2 = Arrays.stream(arr)
			  .reduce(identity , accumulator , combiner );
		
		System.out.println(sum2);
	}

	

}
