package ub15_smallTaskReduce;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class SmallTask {

	public static void main(String[] args) {

		Integer[] numbers = { 1, 2, 3, 4 };
		
		BinaryOperator<Integer> accumulator = (x,y) -> x*y;

		Optional<Integer> result = Stream.<Integer>of(numbers)
			  .reduce(accumulator );
		System.out.println(result.get());
		
		Arrays.stream(numbers)
			  .reduce(accumulator)
			  .ifPresent(System.out::println);
		
		
		Integer identity = 1;
		BiFunction<Integer,Integer,Integer> accumulator2 = (x,y) -> x*y;
		BinaryOperator<Integer> combiner = (x,y) -> x*y;
		
		Integer resultInteger = Arrays.stream(numbers)
		  .reduce(identity , accumulator, accumulator );
		System.out.println(resultInteger);
		
		resultInteger = Arrays.stream(numbers)
				  .reduce(identity , accumulator2, combiner );
		System.out.println(resultInteger);
		
	}

}
