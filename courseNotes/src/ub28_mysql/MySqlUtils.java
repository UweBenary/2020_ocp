package ub28_mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MySqlUtils {
	
	public static Connection getConnection() throws SQLException {
		String url = "jdbc:mysql://localhost/java_test_db?serverTimezone=UTC";
		String user = "root";
		String password = "root";
	
		return DriverManager.getConnection(url, user, password );
	}
	
	public static void main(String[] args) {
		
		try( Connection connection = getConnection() ) {
			System.out.println("Connection steht...");
			
			buildTabelleTiere(connection);
			
			tiereHinzufuegen(connection);
						
			printTabelleTiere(connection);
			
			removeTabelleTiere(connection);

			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public static void printTabelleTiere(Connection connection) throws SQLException {
		try( Statement stm = connection.createStatement() ) {
			
			String sql = "SELECT * FROM tiere";
			
			try( ResultSet res = stm.executeQuery(sql) ) {
				
				System.out.printf("| %2s | %7s | %s | %n", "id", "name", "alter"); 
						
				while( res.next() ) {
			
					System.out.printf("| %2d | %7s | %5d | %n", 
							res.getInt("id"),
							res.getString("name"),
							res.getInt("alter") );
					
				}				
			}
		}
		
	}

	public static void removeTabelleTiere(Connection connection) throws SQLException {
		try( Statement stm = connection.createStatement() ) {
			
			String sql = "	DROP TABLE IF EXISTS `java_test_db`.`tiere`";
			
			stm.executeUpdate(sql) ;
			System.out.println("Tabelle Tiere gelöscht");
		}
		
	}

	public static void tiereHinzufuegen(Connection connection) throws SQLException {
		
		try( Statement stm = connection.createStatement() ) {
			
			String sql = "INSERT INTO `tiere` (`id`, `name`, `alter`)  VALUES  ('1', 'Tom', '3')";
			
			stm.executeUpdate(sql) ;
			
			sql = "INSERT INTO `java_test_db`.`tiere` (`id`, `name`, `alter`)  VALUES  ('2', 'Jerry', '5')";
			
			stm.executeUpdate(sql) ;
			
			sql = "INSERT INTO `java_test_db`.`tiere` (`id`, `name`, `alter`)  VALUES  ('3', 'Lassie', '4')";
			
			stm.executeUpdate(sql) ;
			
			System.out.println("Tiere erzeugt");
		}
		
	}

	public static void buildTabelleTiere(Connection connection) throws SQLException {
		
		try( Statement stm = connection.createStatement() ) {
			
			String sql = 
					"	CREATE TABLE IF NOT EXISTS `java_test_db`.`tiere` (\r\n" + 
					"	  `id` INT NOT NULL ,\r\n" + 
					"	  `name` VARCHAR(45) NULL,\r\n" + 
					"	  `alter` INT NULL,\r\n" + 
					"	  PRIMARY KEY (`id`),\r\n" + 
					"	  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE);";
			
			stm.executeUpdate(sql) ;
			System.out.println("Neue Tabelle Tiere erzeugt");
		}
	}

}
