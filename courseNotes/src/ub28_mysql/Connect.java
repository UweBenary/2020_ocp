package ub28_mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class Connect {

	public static void main(String[] args) {

		String url = "jdbc:mysql://localhost?serverTimezone=UTC";
		String user = "root";
		String password = "root";
		try( Connection connection = DriverManager.getConnection(url, user, password ) ) {
			System.out.println("Verbindung steht");
			
			try( Statement stm = connection.createStatement() ) {
				
				String sql = "INSERT INTO `java_test_db`.`personen` (`vorname`, `nachname`, `geburtsjahr`) VALUES ('Georg', 'H.', '1458')";
				
				stm.executeUpdate(sql) ;
				System.out.println("Neuer Eintrag vorgenommen");
			
				
								
				String sql2 = "SELECT * FROM java_test_db.personen";
				
				try( ResultSet res = stm.executeQuery(sql2) ) {
					
					while (res.next()) {
						
						int id = res.getInt(1);
						id = res.getInt("id");
						
						System.out.printf("id: %d, vorname %7s, nachname %12s, geburtsjahr %d \n", 
								id, 
								res.getString("vorname"), 
								res.getString("nachname"), 
								res.getInt("geburtsjahr"));
					}
				};
			} 
			
			System.out.println("Verbindung wird geschlossen");
			
		} catch (SQLException e) {
			System.out.println("Verbindung konnte nicht geöffnet werden.");
			e.printStackTrace();
		};

	}

}
