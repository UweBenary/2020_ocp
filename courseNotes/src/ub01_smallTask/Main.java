package ub01_smallTask;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


public class Main {

	public static void main(String[] args) {
		/*
		 * Collection to Array
		 */
		
		Collection<Integer> coll = new ArrayList<>();
		Integer[] arr = coll.toArray(new Integer[0]);
		
		Collection<Integer> coll2 = new ArrayList<>();
		coll2.add(1);
		coll2.add(3);
		Integer[] arr2 = coll2.toArray(new Integer[0]);
		System.out.println(Arrays.toString(arr2));
		
		/*
		 * Array to List
		 */
		
		String[] arr3 = {"Tom", "Jerry"};
		List<String> coll3 = Arrays.asList(arr3);
		System.out.println( coll3 );
	}
	
}
