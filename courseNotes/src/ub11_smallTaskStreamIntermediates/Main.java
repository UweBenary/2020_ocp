package ub11_smallTaskStreamIntermediates;


import java.util.Comparator;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {

	/*
	 * selbständig testen: 
			◦map 
			◦sorted (beide Varianten)
			◦flatMap (wenn es geht)
	 */
		
		Stream.of(1,2,3,4)
			.map( x->2*x )
			.forEach(System.out::println);
		
		System.out.println("---");
		Stream.of(4,1,3,2,4)
		.sorted( )
		.forEach(System.out::println);
		
		System.out.println("---");
		Stream.of(4,1,3,2,4)
		.sorted( Comparator.reverseOrder() )
		.forEach(System.out::println);
		
		System.out.println("---");
		Stream.of(4,1,3,2)
		.flatMap(x->Stream.of(x+"2","3"))
		.forEach(System.out::println);
		
		System.out.println("---");
		Stream.of(4,1,3,2)
			.sorted()
			.flatMap(x->Stream.of( "/a/b/c/d".subSequence(0, 2*x)))
			.forEach(System.out::println);
		
		System.out.println("---");
		Stream.of(4,1,3,2)
			.sorted()
			.map(x->"/a/b/c/d".subSequence(0, 2*x))
			.forEach(System.out::println);
	}

}
