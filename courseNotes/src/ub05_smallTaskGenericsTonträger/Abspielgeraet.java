package ub05_smallTaskGenericsTonträger;

public class Abspielgeraet <T extends Tontraeger> {

	void abspielen(T tontreager) {
		System.out.println("Abspielgerät spielt " + tontreager + " ab...");
	}
}
