package ub05_smallTaskGenericsTonträger;

public class Main {

	public static void main(String[] args) {

		Schallplatte schallplatte = new Schallplatte();
		Abspielgeraet<Schallplatte> plattenspieler = new Abspielgeraet<>();
		plattenspieler.abspielen(schallplatte);

		
		CD cd = new CD();
		Abspielgeraet<CD> cdspieler = new Abspielgeraet<CD>();
		cdspieler.abspielen(cd);
		
//		Abspielgeraet<String> 
//		plattenspieler.abspielen(cd);
		
	}

}
