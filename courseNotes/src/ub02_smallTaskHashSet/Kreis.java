package ub02_smallTaskHashSet;

public class Kreis {

	private int radius;

	public Kreis(int radius) {
		super();
		this.radius = radius;
	}
	
	@Override
	public String toString() {
		return "Kreis (" + radius + ")";
	}
	
	@Override
	public int hashCode() {
		return radius;
	}
	
	@Override
	public boolean equals(Object obj) {
		if ( ! (obj instanceof Kreis) ) {
			return false;
		}
		Kreis other = (Kreis) obj;
		return this.radius == other.radius;
	}
}
