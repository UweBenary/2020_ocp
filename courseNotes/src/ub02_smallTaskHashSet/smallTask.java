package ub02_smallTaskHashSet;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

public class smallTask {

	public static void main(String[] args) {

		Collection<Kreis> set = new HashSet<>();
		
		set.add( new Kreis(3) );
		set.add( new Kreis(2) );
		set.add( new Kreis(3) );

		System.out.println( set );
	}

}
