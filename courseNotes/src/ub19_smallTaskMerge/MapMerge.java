package ub19_smallTaskMerge;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class MapMerge {

	public static void main(String[] args) {

		String text = "Heute ist Freitag";
		Map<Character, Integer> countMap = new TreeMap<Character, Integer>();
		
		BiFunction<Integer, Integer, Integer> remappingFunction = (oldCount, value) -> oldCount + value;
		
		for (Character ch : text.toCharArray() ) {
			countMap.merge(ch, 1, remappingFunction);
		}

		System.out.println(countMap);
		
		Function<Character, Character> classifier = c -> c ;
		Supplier<TreeMap<Character, Long>> supplier = TreeMap::new;
		Collector<Character, ?, Long> downstream = Collectors.counting();
			
		Collector<Character, ?, TreeMap<Character, Long>> collector = Collectors.groupingBy(classifier, supplier , downstream );
		
		Map<Character, Long> result2 = text.chars().mapToObj(c -> (char) c)
				   .collect( collector  );
		
		System.out.println(result2);
		
	}

}
