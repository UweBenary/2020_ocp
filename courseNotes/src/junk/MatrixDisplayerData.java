package junk;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class MatrixDisplayerData {
	
	/**
	 * depricated
	 * adopt as adapter to produce Matrix
	 */
	
	
	public static final String MISSING_VALUE_STRING = "missing v@lue";

	public String missingValueString = MISSING_VALUE_STRING; //must not be null
	public String[][] data;

	
	/*
	 * Constructors 
	 */
	/**
	 * data={{s1,s2,s3},{s4,s5,null}} : columns=3 : rows=2
	 * If data.rows not of equal length
	 * throws IllegalArgumentException
	 */
	public MatrixDisplayerData(Object[][] data) {
		if (data.length == 0 || data[0].length == 0) 
			throw new IllegalArgumentException("Number of columns <= 0.");
		
		if ( ! rowLengthsMatch(data) ) 
			throw new IllegalArgumentException("Length of data rows mismatch.");
		
		
		this.data = arrayTo2dStringArray(data, data.length, data[0].length);
	}
	
	private boolean rowLengthsMatch(Object[][] data) {
		int rowLength = data[0].length;
		for (int i = 1; i < data.length; i++) {
			if ( rowLength != data[i].length) 
				return false;
		}
		return true;
	}

	private String[][] arrayTo2dStringArray(Object[][] data, int nRows, int nColumns) {		
		String[][] result = new String[nRows][nColumns] ;
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[i].length; j++) {
				try {
					result[i][j] = data[i][j].toString();
					
				} catch (ArrayIndexOutOfBoundsException e) {
					result[i][j] = missingValueString;
			
				} catch (NullPointerException e) {
					result[i][j] = "null";
				}
			}
		}
		return result;
	}
	
	/**
	 * data={{s1,null},{s3,MISSING_VALUE_STRING},{null, MISSING_VALUE_STRING}} : columns=2 : rows=3
	 * If dataWithoutNulls.rows not of equal length
	 * fill in missingValueString in each row shorter than maxLength
	 * If missingValueString == null -> fill in MISSING_VALUE_STRING
	 */
	public MatrixDisplayerData(Object[][] data, String missingValueString) {	
		if (data.length == 0 || data[0].length == 0) 
			throw new IllegalArgumentException("Number of columns <= 0.");
		if (missingValueString != null) {
			this.missingValueString = missingValueString;
		}
		this.data = arrayTo2dStringArray(data, data.length,  maxRowLength(data) );
		
	}
		
	private int maxRowLength(Object[][] data) {
		int maxLength = data[0].length;
		for (int i = 1; i < data.length; i++) {
			if ( maxLength <  data[i].length) 
				maxLength = data[i].length;
		}
		return maxLength;
	}

	/**
	 * If dataWithoutNulls.size() % nColumns != 0
	 * throws IllegalArgumentException
	 */
	public MatrixDisplayerData(Collection<?> dataWithoutNulls, int nColumns) {	
		if (nColumns < 1) 
			throw new IllegalArgumentException("Number of columns <= 0.");
		
		if (dataWithoutNulls.size() % nColumns != 0) 
			throw new IllegalArgumentException("Size of data and nColumns mismatch.");
		
		data = collTo2dStringArray(dataWithoutNulls, nColumns);
	}

	private static String[][] collTo2dStringArray(Collection<?> collectionWithoutNulls, int arrayLengthLarger0) {
		AtomicInteger streamElementIndex = new AtomicInteger(0);
		AtomicInteger subgroupIndex = new AtomicInteger(0);
		
		Map<Integer, List<String>> groups = collectionWithoutNulls.stream().map(Object::toString).collect(Collectors.groupingBy( s -> {
			if ( streamElementIndex.getAndIncrement() % arrayLengthLarger0 == 0) {
				subgroupIndex.getAndIncrement();
			}
			return subgroupIndex.get();
		}));
		
		 List<List<String>> subSets = new ArrayList<List<String>>(groups.values());
	
		 return subSets.stream().map(l -> l.stream().toArray(String[]::new)).toArray(String[][]::new);
	}
	
	/**
	 * If dataWithoutNulls.size() % nColumns != 0
	 * fill in MISSING_VALUE_STRING in last row
	 * If missingValueString == null -> fill in MISSING_VALUE_STRING
	 */
	public MatrixDisplayerData(Collection<?> dataWithoutNulls, int nColumns, String missingValueString) {	
		if (nColumns < 1) 
			throw new IllegalArgumentException("Number of columns <= 0.");
		
		if (missingValueString != null) {
			this.missingValueString = missingValueString;
		}
		
		List<String> stringList = addMissingValueStrings(dataWithoutNulls, nColumns);
		data = collTo2dStringArray(stringList, nColumns);
	}	
	
	private List<String> addMissingValueStrings(Collection<?> data, int nColumns) {
		List<String> list = data.stream().map(Object::toString).collect(Collectors.toList());
		while ( list.size() % nColumns != 0) {
			list.add(missingValueString);
		}
		return list;
	}
	
	/*
	 * Public Instance Methods
	 */
	
	public String[][] getDataAsStringArray() {
		return data;
	}
	
	public int nRows() {
		return data.length;
	}
	
	public int nColumns() {
		return data[0].length;
	}
}