package junk;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

class MatrixDisplayerDataTest {

	private static final Integer[][] arrayRegular = new Integer[][] { {1,2,3},{4,5,6} } ;
	private static final int nColumnsRegular = 3;
	private static final List<Double> list = Stream.iterate( 1., i -> i + 1 )
			.limit(2*nColumnsRegular)
			.collect(Collectors.toCollection(ArrayList::new));
	
	private static final int nColumnsIrregular = nColumnsRegular+1;
	
	@Test
	final void testInstantiationForRegularArray() {
		try {
			new MatrixDisplayerData(arrayRegular);
			new MatrixDisplayerData(arrayRegular, null);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Fail upon exception");
		}	
	}
		
	@Test
	final void testInstantiationFailsForIrregularArray() {
		Integer[][] arrayIrregular = new Integer[][] { {1,2},{4},{7, 8, 9},{10} } ;
		assertThrows(IllegalArgumentException.class, () -> new MatrixDisplayerData(arrayIrregular));
	}
	
	@Test
	final void testInstantiationForIrregularArrayAndMissingValueFillIn() {
		try {
			Integer[][] arrayIrregular = new Integer[][] { {1,2},{4},{7, 8, 9},{10} } ;
			new MatrixDisplayerData(arrayIrregular, null);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Fail upon exception");
		}	
	}
	
	@Test
	final void testInstantiationFailsForEmptyArray() {
		assertThrows(IllegalArgumentException.class, () -> new MatrixDisplayerData( new Integer[][] {} ));
		assertThrows(IllegalArgumentException.class, () -> new MatrixDisplayerData( new Integer[][] {{ }} ));
		
		assertThrows(IllegalArgumentException.class, () -> new MatrixDisplayerData( new Integer[][] {}, null) );
		assertThrows(IllegalArgumentException.class, () -> new MatrixDisplayerData( new Integer[][] {{ }}, null) );

	}
	
	@Test
	final void testInstantiationForListWithRegularColumnArgument() {
		try {
			new MatrixDisplayerData(list, nColumnsRegular);
			new MatrixDisplayerData(list, nColumnsRegular, null);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Fail upon exception");
		}	
	}
	
	@Test
	final void testInstantiationFailsForListWithIrregularColumnArgument() {		
		assertThrows(IllegalArgumentException.class, () -> new MatrixDisplayerData(list,nColumnsIrregular));
	}
	
	@Test
	final void testInstantiationForListWithIrregularColumnArgumentAndMissingValueFillIn() {
		try {
			new MatrixDisplayerData(list, nColumnsIrregular, null);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Fail upon exception");
		}	
	}
	
	@Test
	final void testInstantiationFailsForListWithNull() {
		List<Double> listWithNull = new ArrayList<>(list);
		listWithNull.set(1, null);
		
		assertThrows(NullPointerException.class, () -> new MatrixDisplayerData(listWithNull,nColumnsRegular));
		assertThrows(NullPointerException.class, () -> new MatrixDisplayerData(listWithNull,nColumnsRegular, null));
	}
	
	@Test
	final void testInstantiationFailsForNull() {
		assertThrows(NullPointerException.class, () -> new MatrixDisplayerData(null,nColumnsRegular));
		assertThrows(NullPointerException.class, () -> new MatrixDisplayerData(null));
	}
	
	@Test
	final void testInstantiationFailsForNonPositiveColumnsArgument() {
		assertThrows(IllegalArgumentException.class, () -> new MatrixDisplayerData(list,0));
		assertThrows(IllegalArgumentException.class, () -> new MatrixDisplayerData(list,0, null));
	}

	@Test
	final void testGetData() {
		final String[][] expected = new String[][] { {"1","2","3"},{"4","5","6"} };
		String[][] actual = new MatrixDisplayerData(arrayRegular).getDataAsStringArray();
		assertArrayEquals(expected, actual);
	}
	
	@Test
	final void testGetDataWithMissingValueFillIn() {
		String mvString = MatrixDisplayerData.MISSING_VALUE_STRING;
		final String[][] expected = new String[][] { {"1.0","2.0","3.0","4.0"},{"5.0","6.0",mvString,mvString} };
		String[][] actual = new MatrixDisplayerData(list, nColumnsIrregular, null).getDataAsStringArray();
		assertArrayEquals(expected, actual);
	}

	@Test
	final void testGetRows() {
		final int expected = 2;
		int actual = new MatrixDisplayerData(arrayRegular).nRows();
		assertEquals(expected, actual);
	}
	
	@Test
	final void testGetColumns() {
		final int expected = 3;
		int actual = new MatrixDisplayerData(arrayRegular).nColumns();
		assertEquals(expected, actual);
	}
}
